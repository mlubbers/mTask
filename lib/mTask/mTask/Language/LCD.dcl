definition module mTask.Language.LCD

import mTask.Language

:: LCD = LCDid Int

instance toString LCD
instance toString Button
derive class iTask LCD, Button
derive gDefault Button
instance == Button
instance basicType Button

:: Button = RightButton | UpButton | DownButton | LeftButton | SelectButton | NoButton

rightButton     :== lit RightButton
upButton        :== lit UpButton
downButton      :== lit DownButton
leftButton      :== lit LeftButton
selectButton    :== lit SelectButton
noButton        :== lit NoButton

class lcd v where
    LCD         :: Int Int [DPin] ((v LCD)->Main (v b)) -> Main (v b) | type b
    print       :: (v LCD) (v t) -> MTask v Int  | type t       // returns bytes written
    setCursor   :: (v LCD) (v Int) (v Int) -> MTask v ()
    scrollLeft  :: (v LCD) -> MTask v ()
    scrollRight :: (v LCD) -> MTask v ()
    pressed     :: (v Button) -> MTask v Bool

class buttonPressed v :: MTask v Button

printAt lcd x y z :== setCursor lcd x y >>|. print lcd z
