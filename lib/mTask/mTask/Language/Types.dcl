definition module mTask.Language.Types

import mTask.Language

from mTask.Interpret.ByteCodeEncoding import generic toByteCode, class toByteWidth, generic fromByteCode, :: FBC

from iTasks.WF.Definition import class iTask, :: Stability, :: TaskValue(..)
from iTasks.UI.Editor.Generic import generic gEditor, :: Editor, :: EditorPurpose
from iTasks.Internal.Generic.Visualization import generic gText, :: TextFormat
from Data.GenDefault import generic gDefault
from Data.GenEq import generic gEq
from Text.GenJSON import generic JSONEncode, generic JSONDecode, :: JSONNode
from Data.Tuple import instance toString ()

:: In a b = In infix 0 a b
:: Main m = {main :: m}

main :: a -> Main a
unmain :: (Main a) -> a

class type t | toString, iTask, toByteWidth, gDefault{|*|}, toByteCode{|*|}, fromByteCode{|*|} t

class basicType t | type t where basicType :: t
instance basicType Int, Bool, Real, Char, ()

class int v a :: (v a) -> v Int
class real v a :: (v a) -> v Real

type2string :: a -> String | TC a

:: I2CAddr =: I2C UInt8
i2c x :== I2C (fromInt x)

:: TimingInterval v
  = Default
  | BeforeMs (v Int)
  | BeforeSec (v Int)
  | ExactMs (v Int)
  | ExactSec (v Int)
  | RangeMs (v Int) (v Int)
  | RangeSec (v Int) (v Int)

instance toInt I2CAddr
instance toString I2CAddr
derive class iTask \ gEditor, gText I2CAddr
derive gText I2CAddr
derive gEditor I2CAddr
