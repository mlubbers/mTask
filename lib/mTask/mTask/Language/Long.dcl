definition module mTask.Language.Long

import mTask.Language

from StdOverloaded import class +, class -, class zero, class one, class *, class /, class <, class toInt, class ~

from iTasks.WF.Definition import class iTask
from iTasks.UI.Editor.Generic import generic gEditor, :: Editor, :: EditorPurpose
from iTasks.Internal.Generic.Visualization import generic gText, :: TextFormat
from Data.GenDefault import generic gDefault
from Data.GenEq import generic gEq
from Text.GenJSON import generic JSONEncode, generic JSONDecode, :: JSONNode

:: Long = Long Int

LONG_MAX :== Long  0x7fffffff
LONG_MIN :== Long -0x7fffffff

instance zero Long
instance one Long
instance ~ Long
instance + Long
instance - Long
instance * Long
instance / Long
instance < Long
instance == Long
instance toString Long
instance toInt Long
instance toReal Long
derive class iTask Long
derive gDefault Long
instance basicType Long

class long v a :: (v a) -> v Long
