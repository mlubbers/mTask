implementation module mTask.Language.Long

import iTasks
import mTask.Language

instance zero Long where zero = Long 0
instance one Long where one = Long 1
instance ~ Long where ~ (Long x) = Long (~ x)
instance + Long where (+) (Long x) (Long y) = Long (x + y)
instance - Long where (-) (Long x) (Long y) = Long (x - y)
instance * Long where (*) (Long x) (Long y) = Long (x * y)
instance / Long where (/) (Long x) (Long y) = Long (x / y)
instance < Long where (<) (Long x) (Long y) = x < y
instance == Long where (==) (Long x) (Long y) = x == y
instance toString Long where toString (Long l) = "L" +++ toString l
instance toInt Long where toInt (Long i) = i
instance toReal Long where toReal (Long i) = toReal i
derive class iTask Long
instance basicType Long where basicType = zero
derive gDefault Long
