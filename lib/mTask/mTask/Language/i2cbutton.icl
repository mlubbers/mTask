implementation module mTask.Language.i2cbutton

import iTasks
import mTask.Language

derive class iTask ButtonStatus, I2CButton
instance toString ButtonStatus where toString a = toSingleLineText a

instance == ButtonStatus where (==) a b = a === b
