definition module mTask.Language.AirQuality

import mTask.Language

:: AirQualitySensor = AirQualitySensor Int

derive class iTask AirQualitySensor

class AirQualitySensor v where
	airqualitysensor :: I2CAddr ((v AirQualitySensor) -> Main (v a)) -> Main (v a) | type a

	setEnvironmentalData :: (v AirQualitySensor) (v Real) (v Real) -> MTask v ()

	tvoc` :: (TimingInterval v) (v AirQualitySensor) -> MTask v Int
	tvoc :: (v AirQualitySensor) -> MTask v Int
	tvoc s = tvoc` Default s

	co2` :: (TimingInterval v) (v AirQualitySensor) -> MTask v Int
	co2 :: (v AirQualitySensor) -> MTask v Int
	co2 s = co2` Default s

	setEnvFromDHT :: (v AirQualitySensor) (v DHT) -> MTask v () | tupl, .&&., dht, step, expr v
	setEnvFromDHT aqs dht :== temperature dht .&&. humidity dht
		>>~. \x->setEnvironmentalData aqs (first x) (second x)
