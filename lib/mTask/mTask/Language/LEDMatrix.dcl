definition module mTask.Language.LEDMatrix

import mTask.Language

:: LEDMatrix = LEDMatrix Int

:: LEDMatrixInfo =
	{ dataPin  :: Pin
	, clockPin :: Pin
	}

derive class iTask LEDMatrix, LEDMatrixInfo

class LEDMatrix v where
	ledmatrix   :: LEDMatrixInfo ((v LEDMatrix) -> Main (v b)) -> Main (v b) | type b
	LMDot       :: (v LEDMatrix) (v Int) (v Int) (v Bool) -> MTask v ()
	LMIntensity :: (v LEDMatrix) (v Int) -> MTask v ()
	LMClear     :: (v LEDMatrix) -> MTask v ()
	LMDisplay   :: (v LEDMatrix) -> MTask v ()
