definition module mTask.Language.Sound

import mTask.Language

:: SoundDetector :== (DPin, APin)

class SoundDetector v | tupl, expr, pinMode v & dio DPin v
where
	soundDetector :: DPin APin ((v SoundDetector) -> Main (v b)) -> Main (v b) | type b & expr, step, pinMode v
	soundDetector dp ap def :== {main=pinMode PMInput (lit dp) >>|. unmain (def (lit (dp, ap)))}

	soundPresence` :: (TimingInterval v) (v SoundDetector) -> MTask v Bool
	soundPresence` i p :== readD` i (first p)

	soundPresence :: (v SoundDetector) -> MTask v Bool | tupl v & dio DPin v
	soundPresence p :== readD (first p)

	soundLevel` :: (TimingInterval v) (v SoundDetector) -> MTask v Bool | aio, tupl v
	soundLevel` i p :== readA` i (second p)

	soundLevel :: (v SoundDetector) -> MTask v Bool | tupl, aio v
	soundLevel p :== readA (second p)
