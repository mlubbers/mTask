definition module mTask.Language.DHT

import mTask.Language
from Data.UInt import :: UInt8

:: DHT = Dht Int
:: DHTInfo
	= DHT_DHT Pin DHTtype
	| DHT_SHT I2CAddr
:: DHTtype = DHT11 | DHT21 | DHT22

instance toString DHTtype
derive class iTask DHTtype, DHT, DHTInfo

class dht v where
	DHT          :: DHTInfo ((v DHT)->Main (v b)) -> Main (v b) | type b

	temperature` :: (TimingInterval v) (v DHT) -> MTask v Real
	temperature  :: (v DHT) -> MTask v Real
	temperature  s = temperature` Default s

	humidity`    :: (TimingInterval v) (v DHT) -> MTask v Real
	humidity     :: (v DHT) -> MTask v Real
	humidity s = humidity` Default s
