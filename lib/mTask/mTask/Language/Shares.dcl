definition module mTask.Language.Shares

import iTasks.SDS.Definition
import mTask.Language

:: Sds a = Sds Int

class sds v where
	sds     :: ((v (Sds t))->In t (Main (MTask v u))) -> Main (MTask v u) | type t & type u
	setSds  :: (v (Sds t)) (v t) -> MTask v t | type t
	updSds  :: (v (Sds t)) ((v t) -> v t) -> MTask v t | type t
	getSds` :: (TimingInterval v) (v (Sds t)) -> MTask v t | type t
	getSds  :: (v (Sds t)) -> MTask v t | type t
	getSds sds = getSds` Default sds

class liftsds v where
	liftsds :: ((v (Sds t))->In (Shared sds t) (Main (MTask v u))) -> Main (MTask v u) | type t & type u & RWShared sds & TC (sds () t t)
