definition module mTask.Language.Expressions

import mTask.Language

from StdOverloaded import class +, class -, class zero, class one, class *, class /, class <
from StdClass import class Ord, class Eq

class expr v where
	lit :: t -> v t | type t
	(+.) infixl 6 :: (v t) (v t) -> v t | basicType, +, zero t
	(-.) infixl 6 :: (v t) (v t) -> v t | basicType, -, zero t
	(*.) infixl 7 :: (v t) (v t) -> v t | basicType, *, zero, one t
	(/.) infixl 7 :: (v t) (v t) -> v t | basicType, /, zero t
	(&.) infixr 3 :: (v Bool) (v Bool) -> v Bool
	(|.) infixr 2 :: (v Bool) (v Bool) -> v Bool
	Not           :: (v Bool) -> v Bool
	(==.) infix 4 :: (v a) (v a) -> v Bool | Eq, basicType a
	(!=.) infix 4 :: (v a) (v a) -> v Bool | Eq, basicType a
	(<.)  infix 4 :: (v a) (v a) -> v Bool | Ord, basicType a
	(>.)  infix 4 :: (v a) (v a) -> v Bool | Ord, basicType a
	(<=.) infix 4 :: (v a) (v a) -> v Bool | Ord, basicType a
	(>=.) infix 4 :: (v a) (v a) -> v Bool | Ord, basicType a
	If :: (v Bool) (v t) (v t) -> v t | type t

class fun a v :: ((a->v s)->In (a->v s) (Main (MTask v u))) -> Main (MTask v u) | type s & type u

class tupl v where
	first  :: (v (a, b)) -> v a | type a & type b
	second :: (v (a, b)) -> v b | type a & type b
	tupl :: (v a) (v b) -> v (a, b) | type a & type b

	/*
	 * Transforms a function on a v of tuple to a tuple of v's
	 * @param The function to transform
	 * @result The transformed function
	 * @type ((v a, v b) -> v c) -> ((v (a, b)) -> v c) | tupl v & type a & type b
	 */
	tupopen f :== \v->f (first v, second v)

true  :== lit True
false :== lit False
