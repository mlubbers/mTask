implementation module mTask.Language.MTask

import mTask.Language

instance toString (TaskValue a) | toString a where
	toString NoValue = "NoValue"
	toString (Value a s) = "Value " +++ toString a +++ " " +++ toString s

derive gDefault TaskValue
