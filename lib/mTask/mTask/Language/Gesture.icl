implementation module mTask.Language.Gesture

import iTasks
import mTask.Language

derive class iTask GestureSensor, Gesture
derive gDefault Gesture
instance toString Gesture where toString a = toSingleLineText a
instance == Gesture where (==) l r = l === r
instance basicType Gesture where basicType = GNone
