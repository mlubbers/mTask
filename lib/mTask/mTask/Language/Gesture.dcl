definition module mTask.Language.Gesture

import mTask.Language

:: GestureSensor = GestureSensor Int
:: Gesture = GNone | GRight | GLeft | GUp | GDown | GForward | GBackward | GClockwise | GCountClockwise

derive class iTask GestureSensor, Gesture
derive gDefault Gesture
instance toString Gesture
instance == Gesture
instance basicType Gesture

class GestureSensor v where
	gestureSensor :: !I2CAddr ((v GestureSensor) -> Main (v a)) -> Main (v a) | type a
	gesture`      :: (TimingInterval v) (v GestureSensor) -> MTask v Gesture
	gesture       :: (v GestureSensor) -> MTask v Gesture
	gesture s = gesture` Default s
