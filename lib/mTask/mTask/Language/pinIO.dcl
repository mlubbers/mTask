definition module mTask.Language.pinIO

import mTask.Language

class aio v where
	writeA :: (v APin) (v Int) -> MTask v Int
	readA` :: (TimingInterval v) (v APin) -> MTask v Int
	readA  :: (v APin) -> MTask v Int
	readA p = readA` Default p

class dio p v | pin p where
	writeD :: (v p) (v Bool) -> MTask v Bool
	readD` :: (TimingInterval v) (v p) -> MTask v Bool | pin p
	readD :: (v p) -> MTask v Bool | pin p
	readD p = readD` Default p

class pinMode v where
	pinMode :: PinMode (v p) -> MTask v () | pin p

	declarePin :: p PinMode ((v p) -> Main (v a)) -> Main (v a) | pin p & type a
	declarePin p pm def :== {main=pinMode pm (lit p) >>|. unmain (def (lit p))}

:: PinMode = PMInput | PMOutput | PMInputPullup
:: Pin = AnalogPin APin | DigitalPin DPin
class pin p :: p -> Pin | type p
instance pin APin, DPin

:: DPin = D0 | D1 | D2 | D3 | D4 | D5 | D6 | D7 | D8 | D9 | D10 | D11 | D12 | D13
:: APin = A0 | A1 | A2 | A3 | A4 | A5

instance toString DPin, APin, PinMode, Pin
derive class iTask APin, DPin, PinMode, Pin
derive gDefault APin, DPin, PinMode, Pin

a0 :== lit A0
a1 :== lit A1
a2 :== lit A2
a3 :== lit A3
a4 :== lit A4
a5 :== lit A5
d0 :== lit D0
d1 :== lit D1
d2 :== lit D2
d3 :== lit D3
d4 :== lit D4
d5 :== lit D5
d6 :== lit D6
d7 :== lit D7
d8 :== lit D8
d9 :== lit D9
d10 :== lit D10
d11 :== lit D11
d12 :== lit D12
d13 :== lit D13
