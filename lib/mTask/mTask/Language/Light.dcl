definition module mTask.Language.Light

import mTask.Language

derive class iTask LightSensor

:: LightSensor = LightSensor Int

class LightSensor v where
	lightsensor :: I2CAddr ((v LightSensor) -> Main (v b)) -> Main (v b) | type b
	light` :: (TimingInterval v) (v LightSensor) -> MTask v Real
	light :: (v LightSensor) -> MTask v Real
	light s = light` Default s
