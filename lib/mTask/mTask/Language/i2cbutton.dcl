definition module mTask.Language.i2cbutton

import mTask.Language

instance toString ButtonStatus
derive class iTask ButtonStatus, I2CButton
instance == ButtonStatus

:: ButtonStatus = ButtonNone | ButtonPress | ButtonLong | ButtonDouble | ButtonHold
:: I2CButton =: I2CButton Int

class i2cbutton v where
	i2cbutton :: !I2CAddr ((v I2CButton) -> Main (v b)) -> Main (v b) | type b

	AButton` :: (TimingInterval v) (v I2CButton) -> MTask v ButtonStatus
	AButton :: (v I2CButton) -> MTask v ButtonStatus
	AButton b = AButton` Default b

	BButton` :: (TimingInterval v) (v I2CButton) -> MTask v ButtonStatus
	BButton :: (v I2CButton) -> MTask v ButtonStatus
	BButton b = BButton` Default b
