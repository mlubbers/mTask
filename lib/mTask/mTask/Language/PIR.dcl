definition module mTask.Language.PIR

import mTask.Language

:: PIR :== DPin

class PIR v | step, expr, pinMode v & dio DPin v where
	PIR :: DPin ((v PIR) -> Main (v b)) -> Main (v b) | type b & expr, step, pinMode v
	PIR dp def :== {main=pinMode PMInput (lit dp) >>|. unmain (def (lit dp))}

	motion` :: (TimingInterval v) (v PIR) -> MTask v Bool
	motion` i p :== readD` i p 

	motion :: (v PIR) -> MTask v Bool | dio DPin v
	motion p :== readD p
