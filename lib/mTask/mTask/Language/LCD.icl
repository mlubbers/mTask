implementation module mTask.Language.LCD

import iTasks
import mTask.Language

instance toString Button where toString b = toSingleLineText b
derive class iTask Button, LCD
derive gDefault Button
instance == Button where (==) x y = gEq{|*|} x y
instance basicType Button where basicType = NoButton

instance toString LCD where toString lcd = "LCD"
