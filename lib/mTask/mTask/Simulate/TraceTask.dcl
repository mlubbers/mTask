definition module mTask.Simulate.TraceTask

import mTask.Language

from iTasks.WF.Definition import class iTask, :: Stability, :: TaskValue(..), :: Task
from iTasks.UI.Editor.Generic import generic gEditor, :: Editor, :: EditorPurpose
from iTasks.Internal.Generic.Visualization import generic gText, :: TextFormat
from Data.GenDefault import generic gDefault
from Data.GenEq import generic gEq
from Text.GenJSON import generic JSONEncode, generic JSONDecode, :: JSONNode

:: TraceTask a =
	{ trace :: [String]
	, tskVl :: a
	, nTask :: TT a
	}

derive class iTask TraceTask

:: TT a =: TT (Task (TraceTask a))

simulate :: (Main (TT a)) -> Task (TraceTask a) | type a

instance LEDMatrix TT
instance LightSensor TT
instance AirQualitySensor TT
instance GestureSensor TT
instance aio TT
instance buttonPressed TT
instance delay TT
instance dht TT
instance dio APin TT
instance dio DPin TT
instance pinMode TT
instance expr TT
instance lcd TT
instance liftsds TT
instance rpeat TT
instance rtrn TT
instance sds TT
instance step TT
instance tupl TT
instance unstable TT
instance .&&. TT
instance .||. TT
instance fun () TT
instance fun (TT a) TT | type a
instance fun (TT a, TT b) TT | type a & type b
instance fun (TT a, TT b, TT c) TT | type a & type b & type c

instance int TT Int
instance int TT Real
instance int TT Long

instance real TT Int
instance real TT Real
instance real TT Long

instance long TT Int
instance long TT Real
instance long TT Long
