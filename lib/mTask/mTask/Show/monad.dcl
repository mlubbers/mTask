definition module mTask.Show.monad

/*
	Pieter Koopman
	Radboud University NIjmegen, The Netherlands
	pieter@cs.ru.nl
*/

import mTask.Show.basic

from Control.Monad import class Monad
from Control.Applicative import class Applicative, class pure, class <*>
from Data.Functor import class Functor

nl :: Show a
indent :: Show a
unIndent :: Show a
instance Functor Show
instance pure Show
instance <*> Show
instance Monad Show
(>>>|) infixl 1 :: (Show a) (Show b) -> Show b
par :: (Show a) -> Show a
censor :: (Show a) -> Show (a, [String])
