definition module mTask.Show.DSL

/*
	Pieter Koopman
	Radboud University NIjmegen, The Netherlands
	pieter@cs.ru.nl
*/

import mTask.Show.basic
import mTask.Language

instance LEDMatrix Show
instance LightSensor Show
instance AirQualitySensor Show
instance GestureSensor Show
instance aio Show
instance delay Show
instance dht Show
instance dio APin Show
instance dio DPin Show
instance pinMode Show
instance expr Show
instance i2cbutton Show
instance lcd Show
instance liftsds Show
instance rpeat Show
instance rtrn Show
instance sds Show
instance step Show
instance tupl Show
instance unstable Show
instance fun () Show
instance fun (Show a) Show | type a
instance fun (Show a, Show b) Show | type a & type b
instance fun (Show a, Show b, Show c) Show | type a & type b & type c
instance .||. Show
instance .&&. Show

instance int Show Int
instance int Show Real
instance int Show Long

instance real Show Int
instance real Show Real
instance real Show Long

instance long Show Int
instance long Show Real
instance long Show Long
