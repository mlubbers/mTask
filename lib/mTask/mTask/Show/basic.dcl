definition module mTask.Show.basic

/*
	Pieter Koopman
	Radboud University NIjmegen, The Netherlands
	pieter@cs.ru.nl
*/

import mTask.Language

:: Show a = Show (ShowState -> (a, ShowState))

:: ShowState =
	{ show 	 :: [String]
	, indent :: Int
	, ids    :: [String]
	}

show :: String -> Show a
show2String :: a -> Show b | toString a
showMain :: (Main (Show a)) -> [String] | type a
showIt :: (Show a) -> [String]

class freshId s :: s String
instance freshId Show
