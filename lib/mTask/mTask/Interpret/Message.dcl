definition module mTask.Interpret.Message

from StdOverloaded import class toString
from GenType.CSerialise import generic gCSerialise, generic gCDeserialise, :: CDeserialiseError
from Data.Either import :: Either
from iTasks.WF.Definition import class iTask
from iTasks.UI.Editor.Generic import generic gEditor, :: Editor, :: EditorPurpose
from iTasks.Internal.Generic.Visualization import generic gText, :: TextFormat
from Data.GenEq import generic gEq
from Text.GenJSON import generic JSONEncode, generic JSONDecode, :: JSONNode

from Data.UInt import :: UInt8, :: UInt16, :: UInt32
from iTasks.WF.Definition import :: TaskValue
from mTask.Interpret.Instructions import :: BCInstr, :: BCInstrs
from mTask.Interpret.Specification import :: MTDeviceSpec
from mTask.Interpret.Peripheral import :: BCPeripheral
from mTask.Interpret.Compile import :: BCShareSpec
from mTask.Interpret.String255 import :: String255
from mTask.Interpret.VoidPointer import :: VoidPointer

:: MTMessageTo
	//* taskid returnwidth peripherals shares instructions
	= MTTTask MTask
	//* taskid (task prep for devices that have small comm buffers)
	| MTTTaskPrep UInt8
	//* taskid
	| MTTTaskDel UInt8
	//*
	| MTTSpecRequest
	//*
	| MTTShutdown
	//* taskid sdsid sds value
	| MTTSdsUpdate UInt8 UInt8 String255

:: MTask =
	{ taskid        :: UInt8
	, tree          :: VoidPointer
	, stability     :: MTaskValueState
	, value         :: String255
	, shares        :: {BCShareSpec}
	, peripherals   :: {BCPeripheral}
	, instructions  :: BCInstrs
	, status        :: MTaskEvalStatus
	, execution_min :: UInt32
	, execution_max :: UInt32
	, lastrun       :: UInt32
	}
:: MTaskValueState = MTNoValue | MTUnstable | MTStable | MTRemoved

// MTEvaluated: task tree is evaluated and the previously calculated execution interval is still correct
// MTPurged: task tree is evaluated, but the execution interval needs to be recalculated
// MTUnevaluated: task tree contains unevaluated parts, the execution interval is [0,0]
:: MTaskEvalStatus = MTEvaluated | MTPurged | MTUnevaluated

:: MTMessageFro
	//* taskid
	= MTFTaskAck UInt8
	//* taskid (task prep ack for devices that have small comm buffers)
	| MTFTaskPrepAck UInt8
	//* taskid
	| MTFTaskDelAck UInt8
	//* taskid returnvalue
	| MTFTaskReturn UInt8 (TaskValue String255)
	//* specification
	| MTFSpec MTDeviceSpec
	//* taskid sdsid sds value
	| MTFSdsUpdate UInt8 UInt8 String255
	//* exception taskid type
	| MTFException MTException
	//* debug message
	| MTFDebug String255
	//* ping
	| MTFPing

:: MTException
	= MTEOutOfMemory UInt8
	| MTEHeapUnderflow UInt8
	| MTEStackOverflow UInt8
	| MTESdsUnknown UInt8 UInt8
	| MTEPeripheralUnknown UInt8 UInt8
	| MTEUnsupportedPeripheral UInt8 UInt8
	| MTEFPException UInt8
	| MTESyncException String255
	| MTERTSError
	| MTEUnexpectedDisconnect

derive class iTask MTMessageFro, MTMessageTo, MTException, MTaskValueState, MTaskEvalStatus
derive gCSerialise MTMessageFro, MTMessageTo, MTException, TaskValue, MTaskValueState, MTaskEvalStatus
derive gCDeserialise MTMessageFro, MTMessageTo, MTException, TaskValue, MTaskValueState, MTaskEvalStatus

instance toString MTException
