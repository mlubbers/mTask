definition module mTask.Interpret.Specification

from iTasks.WF.Definition import class iTask, :: Stability, :: TaskValue(..), :: Task
from iTasks.UI.Editor.Generic import generic gEditor, :: Editor, :: EditorPurpose
from iTasks.Internal.Generic.Visualization import generic gText, :: TextFormat
from Data.GenDefault import generic gDefault
from Data.GenEq import generic gEq
from Data.Either import :: Either
from Text.GenJSON import generic JSONEncode, generic JSONDecode, :: JSONNode
from StdClass import class zero

from GenType.CSerialise import generic gCSerialise, generic gCDeserialise, :: CDeserialiseError
from mTask.Interpret.DSL import :: UInt8, :: UInt16

derive class iTask MTDeviceSpec
instance zero MTDeviceSpec

:: MTDeviceSpec =
	{ memory  :: UInt16
	, aPins   :: UInt8
	, dPins   :: UInt8
	, haveDHT :: Bool
	, haveLM  :: Bool
	, haveI2B :: Bool
	, haveLS  :: Bool
	, haveAQS :: Bool
	, haveGes :: Bool
	}

derive gCSerialise MTDeviceSpec
derive gCDeserialise MTDeviceSpec
