definition module mTask.Interpret.Device

from Control.Monad.State import :: StateT
from Control.Monad.Writer import :: WriterT
from Data.Either import :: Either
from Data.Functor.Identity import :: Identity
from Data.GenDefault import generic gDefault
from Data.GenEq import generic gEq
from Text.GenJSON import generic JSONEncode, generic JSONDecode, :: JSONNode
from iTasks.Internal.Generic.Visualization import generic gText, :: TextFormat
from iTasks.SDS.Definition import class RWShared, class Readable, class Writeable, class Modifiable, class Registrable, class Identifiable
from iTasks.UI.Editor.Generic import generic gEditor, :: Editor, :: EditorPurpose
from iTasks.WF.Definition import :: Task, :: TaskValue
from iTasks.WF.Definition import class iTask

from mTask.Interpret.ByteCodeEncoding import generic fromByteCode, generic toByteCode, class toByteWidth, :: FBC
from mTask.Interpret.Compile import :: CompileOpts
from mTask.Interpret.DSL import :: BCInterpret, :: BCState
from mTask.Interpret.Device.Serial import :: TTYSettings
from mTask.Interpret.Device.TCP import :: TCPSettings
from mTask.Interpret.Device.MQTT import :: MQTTSettings

from mTask.Interpret.Instructions import :: BCInstr
from mTask.Interpret.Message import :: MTMessageFro, :: MTMessageTo, :: MTException
from mTask.Interpret.Specification import :: MTDeviceSpec
from mTask.Language import :: Main, class type

:: Channels :== ([MTMessageFro], [MTMessageTo], Bool)
:: MTDevice

class channelSync a :: a (sds () Channels Channels) -> Task () | RWShared sds

/**
 * Connect to a device
 *
 * @param connection specification
 * @param task using the device
 */
withDevice :: a (MTDevice -> Task b) -> Task b | iTask b & channelSync, iTask a

/**
 * Lift the mTask task to an itasks task, i.e. run it on the given device
 *
 * @param mTask task
 * @param device
 */
liftmTask :: (Main (BCInterpret (TaskValue u))) MTDevice -> Task u | type u

/**
 * Lift the mTask task to an itasks task given the options, i.e. run it on the given device
 *
 * @param compilation options
 * @param mTask task
 * @param device
 */
liftmTaskWithOptions :: CompileOpts (Main (BCInterpret (TaskValue u))) MTDevice -> Task u | type u

/**
 * Wraps a task and catches mTask exceptions
 *
 * @param task
 */
mTaskSafe :: (Task u) -> Task (Either MTException u) | type u

/**
 * Retrieves the device specification from a device handle
 */
deviceSpecification :: MTDevice -> Task MTDeviceSpec

//Handy stuff to enter a device
:: MTD = TCP TCPSettings | Serial TTYSettings | MQTT MQTTSettings
derive class iTask MTD
instance channelSync MTD
//Editor for a device with some presets
enterDevice :: Task MTD
