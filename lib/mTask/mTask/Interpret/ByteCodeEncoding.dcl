definition module mTask.Interpret.ByteCodeEncoding

import StdGeneric
from Control.Applicative import class Applicative, class pure, class <*>, class <*, class *>, class Alternative
from Control.Monad import class Monad
from Data.Either import :: Either
from Data.Error import :: MaybeError
from Data.Functor import class Functor
from Data.GenCons import generic conses
from Data.GenDefault import generic gDefault
from Data.GenEq import generic gEq
from GenType.CSerialise import generic gCSerialise, generic gCDeserialise, :: CDeserialiseError
from StdOverloaded import class toString
from Text.GenJSON import generic JSONEncode, generic JSONDecode, :: JSONNode
from iTasks.Internal.Generic.Visualization import generic gText, :: TextFormat
from iTasks.UI.Editor.Generic import generic gEditor, :: Editor, :: EditorPurpose
from iTasks.WF.Definition import :: TaskException, :: TaskValue, class iTask

from mTask.Language import :: Gesture, :: Pin, :: PinMode, :: APin, :: DPin, :: Button, :: ButtonStatus, :: Long, class type
from Data.UInt import :: UInt8, :: UInt16, :: UInt32, :: Int8, :: Int16, :: Int32
from mTask.Interpret.String255 import :: String255
from mTask.Interpret.ByteCodeEncoding._FBC import :: FBC

generic toByteCode a :: a -> String

derive toByteCode OBJECT, CONS of {gcd_index}, PAIR, EITHER, UNIT, RECORD, FIELD
derive toByteCode (), (,), (,,), TaskValue, Int, Bool, Char, Real, Long, Button, ButtonStatus, APin, DPin, Pin, UInt8, UInt16, UInt32, Int8, Int16, Int32, PinMode, Gesture

generic fromByteCode a *! :: FBC a

// Successful: (Right (Just a), String)
// Not enough bytes: (Right Nothing, String)
// Error: (Left String, String) (E.g. not existing constructor number)
runFBC :: (FBC a) -> ([Char] -> (Either String (? a), [Char]))
instance Functor FBC
instance pure FBC
instance <*> FBC
instance *> FBC
instance <* FBC
instance Alternative FBC
instance Monad FBC

pure2 :: (Either String (? a), [Char]) -> FBC a

class toByteWidth a :: a -> Int
instance toByteWidth (a,b) | toByteWidth a & toByteWidth b
instance toByteWidth (a,b,c) | toByteWidth a & toByteWidth b & toByteWidth c
instance toByteWidth (TaskValue a) | toByteWidth a
instance toByteWidth Int, Bool, Char, (), Real, Long, Button, ButtonStatus, APin, DPin, Pin, UInt8, UInt16, Gesture

derive fromByteCode OBJECT, CONS of {gcd_index,gcd_name}, PAIR, EITHER, UNIT, RECORD, FIELD
derive fromByteCode (), (,), (,,), TaskValue, Int, Bool, Char, Real, Long, Button, ButtonStatus, APin, DPin, Pin, UInt8, UInt16, UInt32, Int8, Int16, Int32, PinMode, Gesture

convert_real_to_float_in_int :: !Real -> Int
convert_float_in_int_to_real :: !Int -> Real

fromBCADT :: FBC a | conses{|*|} a
fail :: String -> (FBC a)
peek :: FBC (? Char)
skipNL :: FBC ()
top :: FBC Char
ntop :: Int -> FBC String

unpack :: (m a) -> a

iTasksDecode :: String -> MaybeError TaskException a | type a

fromCSerialiseToToBytecode :: a -> String | gCSerialise{|*|} a
fromCDeserialiseToFromBytecode :: FBC a | gCDeserialise{|*|} a
