definition module mTask.Interpret.Instructions

from iTasks.WF.Definition import class iTask
from iTasks.UI.Editor.Generic import generic gEditor, :: Editor, :: EditorPurpose
from iTasks.Internal.Generic.Visualization import generic gText, :: TextFormat
from Data.GenDefault import generic gDefault
from Data.GenEq import generic gEq
from Text.GenJSON import generic JSONEncode, generic JSONDecode, :: JSONNode
from Text.HTML import :: HtmlTag

from Data.Either import :: Either

from StdClass import class <
from Data.UInt import :: UInt8, :: UInt16
from Data.Map import :: Map
from mTask.Interpret.String255 import :: String255
from mTask.Language.pinIO import :: PinMode

from GenType.CSerialise import generic gCSerialise, generic gCDeserialise, :: CDeserialiseError

derive class iTask BCInstrs, BCInstr, BCTaskType, JumpLabel

derive gCSerialise BCInstrs, BCInstr, BCTaskType, JumpLabel, PinMode
derive gCDeserialise BCInstrs, BCInstr, BCTaskType, JumpLabel, PinMode

bytewidth :: BCInstr -> Int
debugInstructions :: [BCInstr] -> [(String, String, [String])]
formatDebugInstructions :: [(String, String, [String])] -> HtmlTag
printInstr :: BCInstr -> (String, [String])

//Special type to encode instructions compacter
:: BCInstrs =: BCIs {BCInstr}
bcinstrsgType :: (String, [String], String -> [String], String -> [String])

:: ArgWidth    :== UInt8
:: ReturnWidth :== UInt8
:: Depth       :== UInt8
:: Num         :== UInt8
:: SdsId       :== UInt8
:: JumpLabel   =: JL UInt16
instance < JumpLabel
instance toString JumpLabel

:: BCInstr
	//Return instructions
	= BCReturn1_0      | BCReturn1_1      | BCReturn1_2     | BCReturn1 ArgWidth
	| BCReturn2_0      | BCReturn2_1      | BCReturn2_2     | BCReturn2 ArgWidth
	| BCReturn3_0      | BCReturn3_1      | BCReturn3_2     | BCReturn3 ArgWidth
	| BCReturn_0 ArgWidth | BCReturn_1 ReturnWidth | BCReturn_2 ReturnWidth | BCReturn ReturnWidth ArgWidth
	//Jumping and function calls
	| BCJumpF JumpLabel | BCJump JumpLabel | BCLabel JumpLabel
	| BCJumpSR ArgWidth JumpLabel
	// Tailcall: first is argwidth of the jumper, second the jumped to
	| BCTailcall ArgWidth ArgWidth JumpLabel
	//Arguments
	| BCArg0 | BCArg1 | BCArg2 | BCArg3 | BCArg4 | BCArg ArgWidth //Single args
	| BCArg10 | BCArg21 | BCArg32 | BCArg43                       //Double args
	| BCArgs ArgWidth ArgWidth //Args from to where from > to
	| BCStepArg UInt16 UInt8
	//Task node creation
	| BCMkTask BCTaskType
	//Task node refinement
	| BCTuneRateMs | BCTuneRateSec
	//Task value ops
	| BCIsStable | BCIsUnstable | BCIsNoValue | BCIsValue
	//Stack ops
	| BCPush1 UInt8 | BCPush2 UInt8 UInt8 | BCPush3 UInt8 UInt8 UInt8 | BCPush4 UInt8 UInt8 UInt8 UInt8 | BCPush String255 | BCPushNull
	| BCPop1 | BCPop2 | BCPop3 | BCPop4 | BCPop Num
	| BCRot Depth Num | BCDup | BCPushPtrs
	//Casting
	| BCItoR | BCItoL | BCRtoI | BCRtoL | BCLtoI | BCLtoR
	//Int arith
	| BCAddI | BCSubI | BCMultI | BCDivI
	//Long arith
	| BCAddL | BCSubL | BCMultL | BCDivL
	//Real arith
	| BCAddR | BCSubR | BCMultR | BCDivR
	//Bool arith
	| BCAnd | BCOr | BCNot
	//Equality
	| BCEqI | BCNeqI | BCEqL | BCNeqL
	//Int comparison
	| BCLeI | BCGeI | BCLeqI | BCGeqI
	//Long comparison
	| BCLeL | BCGeL | BCLeqL | BCGeqL
	//Real comparison
	| BCLeR | BCGeR | BCLeqR | BCGeqR

:: BCTaskType
	= BCStable0 | BCStable1 | BCStable2 | BCStable3 | BCStable4 | BCStableNode ArgWidth
	| BCUnstable0 | BCUnstable1 | BCUnstable2 | BCUnstable3 | BCUnstable4 | BCUnstableNode ArgWidth
	// Pin io
	| BCReadD | BCWriteD | BCReadA | BCWriteA | BCPinMode PinMode
	// Repeat
	| BCRepeat
	// Delay
	| BCDelay | BCDelayUntil /* only for internal use */
	// Parallel
	| BCTAnd | BCTOr
	//Step
	| BCStep ArgWidth JumpLabel
	| BCStepStable ArgWidth JumpLabel | BCStepUnstable ArgWidth JumpLabel
	| BCSeqStable ArgWidth | BCSeqUnstable ArgWidth
	//Sds ops
	| BCSdsGet SdsId | BCSdsSet SdsId | BCSdsUpd SdsId JumpLabel
	// Rate limiter
	| BCRateLimit
	////Peripherals
	//DHT
	| BCDHTTemp UInt8 | BCDHTHumid UInt8
	//LEDMatrix
	| BCLEDMatrixDisplay UInt8 | BCLEDMatrixIntensity UInt8 | BCLEDMatrixDot UInt8 | BCLEDMatrixClear UInt8
	//I2CButton
	| BCAButton UInt8 | BCBButton UInt8
	//LightSensor
	| BCGetLight UInt8
	//AirQualitySensor
	| BCSetEnvironmentalData UInt8 | BCTVOC UInt8 | BCCO2 UInt8
	//Gesture
	| BCGesture UInt8
