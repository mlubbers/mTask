definition module mTask.Interpret.Peripherals.DHT

from Data.UInt import :: UInt8
import mTask.Interpret.DSL
import mTask.Language

instance dht (StateT BCState (WriterT [BCInstr] Identity))
