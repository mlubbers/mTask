implementation module mTask.Interpret.Peripherals.DHT

import StdEnv

import Data.Func
import Data.List
import Data.Functor
import Data.Functor.Identity
import Data.Monoid
import Control.Monad
import Control.Monad.State
import Control.Monad.Writer
import Control.Applicative

import mTask.Interpret.DSL
import Data.UInt
import mTask.Language

instance dht (StateT BCState (WriterT [BCInstr] Identity))
where
	DHT pin type def = {main
		=   gets nextDHT
		<*  modify (\s->{s & bcs_hardware=s.bcs_hardware ++ [BCDHT pin type]})
		>>= unmain o def o pure
		}
	temperature` ti dht
		= dht
		>>= \(Dht i)->tell` [BCMkTask $ BCDHTTemp $ fromInt i]
		>>| setRate True ti
	humidity` ti dht
		= dht
		>>= \(Dht i)->tell` [BCMkTask $ BCDHTTemp $ fromInt i]
		>>| setRate True ti

nextDHT :: BCState -> DHT
nextDHT st=:{bcs_hardware=p} = Dht $ fromInt $ length [()\\(BCDHT _)<-p]
