implementation module mTask.Interpret.Device.Serial

import StdEnv
import mTask.Interpret => qualified :: MTask
import iTasks
import CleanSerial.iTasks
import System.Time
import Data.Either
import Data.Maybe
import mTask.Interpret.ByteCodeEncoding
import mTask.Interpret.Message
import GenType.CSerialise

instance channelSync TTYSettings where
	 channelSync settings channels
		= syncSerialChannel
			{tv_sec=0,tv_nsec=100*1000000}
			settings
			(\a->{#toChar c\\c<-gCSerialise{|*|} a []})
			write
			channels

write s = case deserialiseMultiple listTop [] [toInt s\\s<-:s] of
	Left e = (Left (toString e), s)
	Right (a, c) = (Right a, toString c)
