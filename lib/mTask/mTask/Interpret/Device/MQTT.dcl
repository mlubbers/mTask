definition module mTask.Interpret.Device.MQTT

from mTask.Interpret.Device import class channelSync
from iTasks.WF.Definition import class iTask, :: Task
from iTasks.UI.Editor.Generic import generic gEditor, :: Editor, :: EditorPurpose
from iTasks.Internal.Generic.Visualization import generic gText, :: TextFormat
from Data.GenEq import generic gEq
from Text.GenJSON import generic JSONEncode, generic JSONDecode, :: JSONNode
from MQTT import :: MQTTAuth

:: MQTTSettings = 
  { host  :: String
  , port  :: Int
  , mcuId :: String
  , serverId :: String
  , auth :: MQTTAuth
  }
derive class iTask MQTTSettings

generateServerId :: Task String

instance channelSync MQTTSettings