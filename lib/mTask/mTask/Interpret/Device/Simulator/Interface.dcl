definition module mTask.Interpret.Device.Simulator.Interface

import mTask.Interpret.Device.Simulator.State
import Data.UInt

/**
 * @result The amount of milliseconds since unix epoch
 */
getMillies :: !*World -> (!Int, !*World)

/**
 * Read Digital Pin value
 */
readDPin :: !UInt8 !BCSimState -> (!Bool, !BCSimState)

/**
 * Write new Digital Pin value
 * @param ID of the pin
 * @param Value to write
 * @param Simulator State
 */
writeDPin :: !UInt8 !Bool !BCSimState -> BCSimState

/**
 * Read Analogue Pin value
 */
readAPin :: !UInt8 !BCSimState -> (!UInt8, !BCSimState)

/**
 * Write new Analogue Pin value
 * @param ID of the pin
 * @param Value to write
 * @param Simulator State
 */
writeAPin :: !UInt8 !UInt8 !BCSimState -> BCSimState

/**
 * Read the temperature from Pin
 */
getDhtTemp :: !BCSimState !UInt8 -> UInt16

/**
 * Read the humidity from Pin
 */
getDhtHumid :: !BCSimState !UInt8 -> UInt16
