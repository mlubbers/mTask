definition module mTask.Interpret.Device.Simulator.Task

from mTask.Interpret.Compile import :: BCShareSpec
from mTask.Interpret.Instructions import :: BCInstr, :: BCTaskType
from StdOverloaded import class zero
import mTask.Interpret.Device.Simulator
import mTask.Interpret.Device.Simulator.State
import Data.UInt
import iTasks

/**
 * The Task as defined in task.h
 */
:: MTTask =
	{ id :: !UInt8              //* Id of the Task (used in e.g. updating Shares)
	, stability :: !UInt16      //* Stability of the result
	, returnwidth :: !Int       //* Returnwidth of this task
	, returnvalue :: ![UInt16]  //* Return value of this task
	, bc :: ![(Bool, BCInstr)]  //* Known as program in reference implementation, includes location of breakpoints
	, sds :: ![BCShareSpec]     //* Shares
	, tree :: !UInt16           //* Offset to the Child Tree in heap
	}

/**
 * Task Tree as in task.h
 */
:: MTTaskTree =
	{ task_type :: !BCTaskType //* The type of the Tree TODO: Remove this field, information already in node
	, trash :: !Bool           //* Should the tree be garbagecollected?
	, ptr :: !UInt16           //* Reverse Pointer to its parent.
	, node :: !MTTaskTreeNode  //* The Node (union in task.h)
	, break :: !Bool           //* Should rewriting this Task be considered as a breakpoint
	}

:: MTTaskTreeNode
	= NodeBCStable0
	| NodeBCStable1 UInt16
	| NodeBCStable2 UInt16 UInt16
	| NodeBCStable3 UInt16 UInt16 UInt16
	| NodeBCStable4 UInt16 UInt16 UInt16 UInt16
	| NodeBCStableNode UInt16 UInt8 UInt16 UInt16 // Next Width Stab1 Stab2
	| NodeBCUnstable0
	| NodeBCUnstable1 UInt16
	| NodeBCUnstable2 UInt16 UInt16
	| NodeBCUnstable3 UInt16 UInt16 UInt16
	| NodeBCUnstable4 UInt16 UInt16 UInt16 UInt16
	| NodeBCUnstableNode UInt16 UInt8 UInt16 UInt16 // Next Width Unstab1 Unstab2
	| NodeBCReadD UInt8 // TargetPin
	| NodeBCWriteD UInt8 Bool // TargetPin Value
	| NodeBCReadA UInt8 // TargetPin
	| NodeBCWriteA UInt8 UInt8 // TargetPin Value
	| NodeBCRepeat UInt16 UInt16 // OldTreePtr TreePtr
	| NodeBCDelay UInt16 Int // WaitTime EndTime
	| NodeBCStep UInt16 UInt16 UInt8 // LeftPtr RightPC LeftSize
	| NodeBCTAnd UInt16 UInt16 // LeftPtr RightPtr
	| NodeBCTOr UInt16 UInt16 // LeftPtr RightPtr
	| NodeBCSdsGet UInt16 UInt8 // PtrToData SdsId
	| NodeBCSdsSet UInt16 UInt8 UInt16 // PtrSDS SdsId PtrToWriteData
	| NodeBCDHTTemp UInt8 // Pin
	| NodeBCDHTHumid UInt8 // Pin

derive class iTask MTTask, MTTaskTree, MTTaskTreeNode

/**
 * Look for an sds in the list sds
 *
 * @result The offset to the sds
 */
sdsGetAddr :: !UInt8 !BCSimState -> UInt16

/**
 * task_complete in task.c
 *
 * @param The result that might return
 * @param The task
 * @param The I/O in case the result needs to be send
 * @param The Simulator State
 *
 * @result The Simulator State
 */
taskComplete :: ![UInt16] !MTTask !(SimChannels sds) !BCSimState -> Task BCSimState | RWShared sds

/**
 * tasktree_clone in task.c
 *
 * @param The root node of the tree that needs to be clone
 * @param The Parent of the root node
 * @param The Simulator State
 *
 * @result The Pointer of the new root node
 */
taskTreeClone :: !UInt16 !UInt16 !BCSimState -> (!UInt16, !BCSimState)
