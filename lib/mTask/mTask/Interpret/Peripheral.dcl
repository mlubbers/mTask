definition module mTask.Interpret.Peripheral

from Data.UInt import :: UInt8
from StdOverloaded import class toString
from GenType.CSerialise import generic gCSerialise, generic gCDeserialise, :: CDeserialiseError
from Data.Either import :: Either
from iTasks.WF.Definition import class iTask
from iTasks.UI.Editor.Generic import generic gEditor, :: Editor
from iTasks.Internal.Generic.Visualization import generic gText, :: TextFormat
from Data.GenEq import generic gEq
from Text.GenJSON import generic JSONEncode, generic JSONDecode, :: JSONNode

from mTask.Interpret.VoidPointer import :: VoidPointer
import mTask.Interpret.DSL
import mTask.Language

:: BCPeripheral
	= BCDHT DHTInfo
	| BCLEDMatrix LEDMatrixInfo
	| BCI2CButton I2CAddr
	| BCLightSensor I2CAddr
	| BCAirQualitySensor I2CAddr
	| BCGestureSensor I2CAddr
	| BCInitialised VoidPointer
	//= E.p: BCPeripheral p & iTask p & fromByteCode p & toByteCode p

derive class iTask BCPeripheral
derive gCSerialise BCPeripheral
derive gCDeserialise BCPeripheral

instance dht (StateT BCState (WriterT [BCInstr] Identity))
instance LEDMatrix (StateT BCState (WriterT [BCInstr] Identity))
instance i2cbutton (StateT BCState (WriterT [BCInstr] Identity))
instance LightSensor (StateT BCState (WriterT [BCInstr] Identity))
instance AirQualitySensor (StateT BCState (WriterT [BCInstr] Identity))
instance GestureSensor (StateT BCState (WriterT [BCInstr] Identity))
