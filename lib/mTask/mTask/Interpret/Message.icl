implementation module mTask.Interpret.Message

import StdEnv
import iTasks
import GenType.CSerialise
import Data.UInt
import mTask.Interpret => qualified :: MTask
import mTask.Interpret.Instructions
import mTask.Interpret.Compile
import mTask.Interpret.VoidPointer

derive class iTask MTMessageFro, MTMessageTo, MTException, MTask, MTaskValueState, MTaskEvalStatus
derive gCSerialise MTMessageFro, MTMessageTo, MTException, MTask, TaskValue, MTaskValueState, MTaskEvalStatus
derive gCDeserialise MTMessageFro, MTMessageTo, MTException, MTask, TaskValue, MTaskValueState, MTaskEvalStatus
instance toString MTException where toString e = toSingleLineText e
