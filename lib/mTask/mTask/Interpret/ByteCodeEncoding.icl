implementation module mTask.Interpret.ByteCodeEncoding

import Data.GenCons
import StdEnv

import Control.Applicative
import Control.GenBimap
import Control.Monad
import Data.Either
import Data.Error
import Data.Func
import Data.Functor
import Data.Maybe
import Data.Tuple
import System._Pointer
import Text => qualified join

import mTask.Language
import mTask.Interpret.DSL
import mTask.Interpret
import mTask.Interpret.String255
import mTask.Interpret.ByteCodeEncoding._FBC
import iTasks.WF.Definition

import GenType.CSerialise

toByteCode{|Char|} c = toByteCode{|*|} (toInt c)
toByteCode{|Real|} c = toBytes 4 $ IF_INT_64_OR_32
	convert_real_to_float_in_int
	convert_real_to_float_in_int32 c
toByteCode{|String|} s = toByteCode{|*|} (UInt8 (size s)) +++ s
toByteCode{|Int|} n = toBytes 2 (to2comp 16 n)
toByteCode{|String255|} s = toByteCode{|*|} (toString s)
toByteCode{|Bool|} b = toByteCode{|*|} (if b 1 0)
toByteCode{|UInt8|} x = fromCSerialiseToToBytecode x
toByteCode{|UInt16|} x = fromCSerialiseToToBytecode x
toByteCode{|UInt32|} x = fromCSerialiseToToBytecode x
toByteCode{|Int8|} x = fromCSerialiseToToBytecode x
toByteCode{|Int16|} x = fromCSerialiseToToBytecode x
toByteCode{|Int32|} x = fromCSerialiseToToBytecode x
toByteCode{|Long|} (Long n) = toBytes 4 (to2comp 32 n)
toByteCode{|APin|} a = toByteCode{|*|} $ consIndex{|*|} a << 1
toByteCode{|DPin|} a = toByteCode{|*|} $ (consIndex{|*|} a << 1) + 1
toByteCode{|Pin|} (DigitalPin a) = toByteCode{|*|} a
toByteCode{|Pin|} (AnalogPin a) = toByteCode{|*|} a
toByteCode{|PinMode|} a = {toChar $ consIndex{|*|} a}
toByteCode{|Gesture|} a = toByteCode{|*|} (consIndex{|*|} a)
toByteCode{|Button|} a = toByteCode{|*|} (consIndex{|*|} a)
toByteCode{|ButtonStatus|} a = toByteCode{|*|} (consIndex{|*|} a)

toBytes :: Int Int -> String
toBytes bytes i = {toChar $ i >> (b*8) bitand 0xff\\b<-reverse [0..bytes-1]}
to2comp :: Int Int -> Int
to2comp bits i = if (i < 0) (2 ^ bits + i) i

convert_real_to_float_in_int :: !Real -> Int
convert_real_to_float_in_int r = code {
|	cvtsd2ss %xmm0,%xmm0
	instruction 242
	instruction 15
	instruction 90
	instruction 192
}

convert_real_to_float_in_int32 :: !Real -> Int
convert_real_to_float_in_int32 r = snd (cvt r)
where
	cvt :: !Real -> (!Int, !Int)
	cvt r = code {
		|	cvtsd2ss %xmm0,%xmm0
			instruction 242
			instruction 15
			instruction 90
			instruction 192
		}

toByteCode{|()|} _ = ""
toByteCode{|(,)|} tol tor (l, r) = tol l +++ tor r
toByteCode{|(,,)|} tol tom tor (l, m, r) = tol l +++ tom m +++ tor r
toByteCode{|(,,,)|} tol tom ton tor (l, m, n, r) = tol l +++ tom m +++ ton n +++ tor r
toByteCode{|UNIT|} _ = ""
toByteCode{|OBJECT|} tob (OBJECT a) = tob a
toByteCode{|RECORD|} tob (RECORD a) = tob a
toByteCode{|FIELD|} tob (FIELD a) = tob a
toByteCode{|EITHER|} tob _ (LEFT a) = tob a
toByteCode{|EITHER|} _ tob (RIGHT a) = tob a
toByteCode{|CONS of {gcd_index}|} tob (CONS a) = {toChar $ gcd_index} +++ tob a
toByteCode{|PAIR|} tol tor (PAIR l r) = tol l +++ tor r

unpack :: (m a) -> a
unpack _ = undef

pure2 :: (Either String (? a), [Char]) -> FBC a
pure2 (v, str) = FBC \s->(v, str ++ s)

fromByteCode{|Bool|} = (==) 1 <$> fromByteCode{|*|}
fromByteCode{|Real|} = IF_INT_64_OR_32
	convert_float_in_int_to_real
	convert_float_in_int_to_real32 <$> fromBytes <$> ntop 4
fromByteCode{|Char|} = fromInt <$> fromByteCode{|*|}
fromByteCode{|Long|} = Long <$> fro2comp 32 <$> fromBytes <$> ntop 4
fromByteCode{|Int|} = fro2comp 16 <$> fromBytes <$> ntop 2
fromByteCode{|Button|} = fromByteCode{|*|} >>= fromCons
fromByteCode{|APin|} = fromByteCode{|*|} >>= \p->case p of
	AnalogPin a = pure a
	_ = fail "Expected an analog pin but got a digital"
fromByteCode{|DPin|} = fromByteCode{|*|} >>= \p->case p of
	DigitalPin a = pure a
	_ = fail "Expected an analog pin but got a digital"
fromByteCode{|Pin|} = topin <$> fromByteCode{|*|}
topin i
	| i bitand 1 > 0 = DigitalPin $ conses{|*|} !! (i >> 1)
	                 = AnalogPin $ conses{|*|} !! (i >> 1)
fromByteCode{|Gesture|} = fromByteCode{|*|} >>= fromCons
fromByteCode{|PinMode|} = top >>= fromCons o toInt
fromByteCode{|ButtonStatus|} = fromByteCode{|*|} >>= fromCons
fromByteCode{|UInt32|} = fromCDeserialiseToFromBytecode
fromByteCode{|UInt16|} = fromCDeserialiseToFromBytecode
fromByteCode{|UInt8|} = fromCDeserialiseToFromBytecode
fromByteCode{|Int32|} = fromCDeserialiseToFromBytecode
fromByteCode{|Int16|} = fromCDeserialiseToFromBytecode
fromByteCode{|Int8|} = fromCDeserialiseToFromBytecode

derive fromByteCode TaskValue
derive toByteCode TaskValue
fromByteCode{|()|} = pure ()
fromByteCode{|(,)|} a b = tuple <$> a <*> b
fromByteCode{|(,,)|} a b c = tuple3 <$> a <*> b <*> c
fromByteCode{|UNIT|} = pure UNIT
fromByteCode{|OBJECT|} a = (\x->OBJECT x) <$> a
fromByteCode{|CONS of {gcd_index,gcd_name}|} a
	= top >>= \i-> if (gcd_index == toInt i) (CONS <$> a)
		(fail ("gcd_index: " +++ toString gcd_index +++ " parsing: " +++ gcd_name))
fromByteCode{|EITHER|} l r = LEFT <$> l <|> RIGHT <$> r
fromByteCode{|PAIR|} l r = PAIR <$> l <*> r
fromByteCode{|RECORD|} a = (\x->RECORD x) <$> a
fromByteCode{|FIELD|} a = (\x->FIELD x) <$> a

instance toByteWidth Bool where toByteWidth _ = 1
instance toByteWidth UInt8 where toByteWidth _ = 1
instance toByteWidth UInt16 where toByteWidth _ = 1
instance toByteWidth Int where toByteWidth _ = 1
instance toByteWidth Long where toByteWidth _ = 2
instance toByteWidth Char where toByteWidth _ = 1
instance toByteWidth Real where toByteWidth _ = 2

convert_float_in_int_to_real :: !Int -> Real
convert_float_in_int_to_real r = code {
.d 0 1 r
	jmp	_convert_float_in_int_to_real_
.o 0 1 r
	:_convert_float_in_int_to_real_
|	cvtss2ds %xmm0,%xmm0
	instruction 243
	instruction 15
	instruction 90
	instruction 192
}

convert_float_in_int_to_real32 :: !Int -> Real
convert_float_in_int_to_real32 r = cvt (0, r)
where
	cvt :: !(!Int, !Int) -> Real
	cvt _ = code {
		.d 0 2 r
			jmp	_convert_float_in_int_to_real32_
		.o 0 2 r
			:_convert_float_in_int_to_real32_
		|	cvtss2ds %xmm0,%xmm0
			instruction 243
			instruction 15
			instruction 90
			instruction 192
		}

fro2comp :: Int Int -> Int
fro2comp bits i = let mask = 2 ^ dec bits in ~(i bitand mask) + (i bitand bitnot mask)

fromBytes :: String -> Int
fromBytes s = sum [toInt c << (b*8)\\c<-:s & b<-reverse [0..size s - 1]]

instance toByteWidth Button where toByteWidth _ = 1
instance toByteWidth ButtonStatus where toByteWidth _ = 1
instance toByteWidth APin where toByteWidth _ = 1
instance toByteWidth DPin where toByteWidth _ = 1
instance toByteWidth Pin where toByteWidth _ = 1
instance toByteWidth () where toByteWidth _ = 0
instance toByteWidth Gesture where toByteWidth _ = 1

instance toByteWidth (a,b) | toByteWidth a & toByteWidth b
where toByteWidth x = toByteWidth (fst x) + toByteWidth (snd x)

instance toByteWidth (a,b,c) | toByteWidth a & toByteWidth b & toByteWidth c
where toByteWidth x = toByteWidth (fst3 x) + toByteWidth (snd3 x) + toByteWidth (thd3 x)

instance toByteWidth (TaskValue a) | toByteWidth a where toByteWidth tv = one//toByteWidth (cast tv)

cast :: (v a) -> a
cast i = undef

top :: FBC Char
top = FBC \s->case s of
	[] = (Right ?None, s)
	[c:cs] = (Right (?Just c), cs)

ntop :: Int -> FBC String
ntop i = toString <$> sequence (repeatn i top)

peek :: FBC (? Char)
peek = FBC \s->case s of
	[] = (Right (?Just ?None), s)
	[a:_] = (Right (?Just (?Just a)), s)

skipNL :: FBC ()
skipNL = peek >>= \c->case c of
	?Just '\n' = top >>| skipNL
	_ = pure ()

fromBCADT :: FBC a | conses{|*|} a
fromBCADT = top >>= fromCons o toInt

fromCons :: Int -> FBC a | conses{|*|} a
fromCons c
	# cs = conses{|*|}
	| c >= length cs  = fail ("Unknown constructor: " +++ toString c)
	= pure (cs !! c)

runFBC :: (FBC a) -> ([Char] -> (Either String (? a), [Char]))
runFBC (FBC a) = a

fail :: String -> (FBC a)
fail e = FBC \s->(Left e, s)

instance Functor FBC where fmap f a = FBC $ appFst (fmap (fmap f)) o runFBC a
instance pure FBC where pure a = FBC \s->(Right (?Just a), s)
instance *> FBC
instance <* FBC
instance <*> FBC
where
	(<*>) a b = FBC \s->case runFBC a s of
		(Left e, _) = (Left e, s)
		(Right ?None, _) = (Right ?None, s)
		(Right (?Just ab), r) = runFBC (ab <$> b) r
instance Alternative FBC
where
	empty = FBC $ tuple $ Left "No matching alternative"
	(<|>) fa fb = FBC \s->case runFBC fa s of
		(Right (?Just a), s) = (Right (?Just a), s)
		(Right ?None, _) = (Right ?None, s)
//		(Right (Just a), s) = (Right (Just a), s)
//		(Right Nothing, s) = case runFBC fb s of
//			(Right (Just a), s) = (Right (Just a), s)
//			_ = (Right Nothing, s)
		_ = runFBC fb s
instance Monad FBC
where
	bind ma a2mb = FBC \s->case runFBC ma s of
		(Left e, _) = (Left e, s)
		(Right ?None, _) = (Right ?None, s)
		(Right (?Just a), r) = runFBC (a2mb a) r

iTasksDecode :: String -> MaybeError TaskException a | type a
iTasksDecode s = case runFBC fromByteCode{|*|} [c\\c<-:s] of
	(Left err, _) = Error $ exception $ "Decoding error: " +++ err
	(Right ?None, _) = Error $ exception $ "Not enough data to decode: " +++ 'Text'.join "," [toString (toInt c)\\c<-:s]
	(Right (?Just i), []) = Ok i
	(Right (?Just _), rest) = Error $ exception $ "Too much data to decode: " +++ 'Text'.join "," [toString (toInt c)\\c<-rest]

fromCSerialiseToToBytecode :: a -> String | gCSerialise{|*|} a
fromCSerialiseToToBytecode x = {toChar i\\i<-gCSerialise{|*|} x []}
fromCDeserialiseToFromBytecode :: FBC a | gCDeserialise{|*|} a
fromCDeserialiseToFromBytecode = FBC \i->case gCDeserialise{|*|} listTop (map toInt i) of
	Left CDInputExhausted = (Right ?None, i)
	Left e = (Left (toString e), i)
	Right (a, c) = (Right (?Just a), map toChar c)

derive class gCons Gesture, PinMode, Pin, APin, DPin, Button, ButtonStatus, String255
