definition module mTask.Interpret.VoidPointer

from Data.Either import :: Either
from Data.GenEq import generic gEq
from GenType import generic gType, :: Box, :: GType
from Text.GenJSON import generic JSONEncode, generic JSONDecode, :: JSONNode
from iTasks.Internal.Generic.Visualization import generic gText, :: TextFormat
from iTasks.UI.Editor.Generic import generic gEditor, :: Editor, :: EditorPurpose
from iTasks.WF.Definition import class iTask
from GenType import generic gType, :: Box, :: GType
from GenType.CSerialise import generic gCSerialise, generic gCDeserialise, :: CDeserialiseError
from Data.Either import :: Either

:: VoidPointer =: VoidPointer ()
vp :== VoidPointer ()
derive gType VoidPointer
derive gCSerialise VoidPointer
derive gCDeserialise VoidPointer
derive class iTask VoidPointer

voidPointergType :: (String, [String], String -> [String], String -> [String])
