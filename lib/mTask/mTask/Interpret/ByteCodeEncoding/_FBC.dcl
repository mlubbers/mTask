definition module mTask.Interpret.ByteCodeEncoding._FBC

from Data.Either import :: Either
from StdGeneric import generic binumap

derive binumap FBC
:: FBC a = FBC ([Char] -> (Either String (? a), [Char]))
