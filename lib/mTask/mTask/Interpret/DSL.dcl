definition module mTask.Interpret.DSL

from Gast import class Gast, generic ggen, generic genShow, generic gPrint, :: GenState, class PrintOutput, :: PrintState
from StdOverloaded import class zero
from Control.Monad.State import :: StateT
from Control.Monad.Writer import :: WriterT
from Data.Functor.Identity import :: Identity
from StdOverloaded import class ^(^), class -(-)
from Data.Map import :: Map
import iTasks.WF.Derives

import mTask.Interpret.Peripheral
import mTask.Interpret.ByteCodeEncoding
import mTask.Interpret.Instructions
import Data.UInt
import mTask.Language
from iTasks.SDS.Definition import :: SDSLens

MT_NULL     :== UInt16 ((2^16) - 1)
MT_REMOVE   :== UInt8  ((2^8)  - 1)

:: BCInterpret a :== StateT BCState (WriterT [BCInstr] Identity) a

tell` :: [BCInstr] -> BCInterpret a
setRate :: Bool (TimingInterval (StateT BCState (WriterT [BCInstr] Identity))) -> BCInterpret a

:: BCState =
	{ bcs_mainexpr     :: [BCInstr]
	, bcs_context      :: [BCInstr]
	, bcs_functions    :: Map JumpLabel BCFunction
	, bcs_freshlabel   :: JumpLabel
	, bcs_sdses        :: [Either String255 MTLens]
	, bcs_hardware     :: [BCPeripheral]
	}
:: MTLens :== SDSLens () String255 String255
:: BCFunction =
	{ bcf_instructions :: [BCInstr]
	, bcf_argwidth     :: UInt8
	, bcf_returnwidth  :: UInt8
	}

instance zero BCState

mainBC :: (Main (BCInterpret v)) BCState -> BCState

instance aio      (StateT BCState (WriterT [BCInstr] Identity))
instance expr     (StateT BCState (WriterT [BCInstr] Identity))
instance delay    (StateT BCState (WriterT [BCInstr] Identity))
instance dio p    (StateT BCState (WriterT [BCInstr] Identity))
instance pinMode  (StateT BCState (WriterT [BCInstr] Identity))
instance rpeat    (StateT BCState (WriterT [BCInstr] Identity))
//TODO instance lcd      (StateT BCState (WriterT [BCInstr] Identity))
instance rtrn     (StateT BCState (WriterT [BCInstr] Identity))
instance sds      (StateT BCState (WriterT [BCInstr] Identity))
instance liftsds  (StateT BCState (WriterT [BCInstr] Identity))
instance step     (StateT BCState (WriterT [BCInstr] Identity))
instance tupl     (StateT BCState (WriterT [BCInstr] Identity))
instance unstable (StateT BCState (WriterT [BCInstr] Identity))
instance .&&.     (StateT BCState (WriterT [BCInstr] Identity))
instance .||.     (StateT BCState (WriterT [BCInstr] Identity))
instance fun ()
                  (StateT BCState (WriterT [BCInstr] Identity))
instance fun (StateT BCState (WriterT [BCInstr] Identity) a)
                  (StateT BCState (WriterT [BCInstr] Identity)) | type a
instance fun (StateT BCState (WriterT [BCInstr] Identity) a, StateT BCState (WriterT [BCInstr] Identity) b)
                  (StateT BCState (WriterT [BCInstr] Identity)) | type a & type b
instance fun (StateT BCState (WriterT [BCInstr] Identity) a, StateT BCState (WriterT [BCInstr] Identity) b, StateT BCState (WriterT [BCInstr] Identity) c)
                  (StateT BCState (WriterT [BCInstr] Identity)) | type a & type b & type c

instance int  (StateT BCState (WriterT [BCInstr] Identity)) Int
instance int  (StateT BCState (WriterT [BCInstr] Identity)) Real
instance int  (StateT BCState (WriterT [BCInstr] Identity)) Long
instance real (StateT BCState (WriterT [BCInstr] Identity)) Int
instance real (StateT BCState (WriterT [BCInstr] Identity)) Real
instance real (StateT BCState (WriterT [BCInstr] Identity)) Long
instance long (StateT BCState (WriterT [BCInstr] Identity)) Int
instance long (StateT BCState (WriterT [BCInstr] Identity)) Real
instance long (StateT BCState (WriterT [BCInstr] Identity)) Long
