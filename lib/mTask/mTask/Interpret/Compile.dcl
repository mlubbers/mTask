definition module mTask.Interpret.Compile

from StdOverloaded import class zero
from GenType.CSerialise import generic gCSerialise, generic gCDeserialise, :: CDeserialiseError
from Data.Either import :: Either
from iTasks.WF.Definition import class iTask, :: TaskValue, :: Task
from iTasks.UI.Editor.Generic import generic gEditor, :: Editor, :: EditorPurpose
from iTasks.Internal.Generic.Visualization import generic gText, :: TextFormat
from Data.GenEq import generic gEq
from Text.GenJSON import generic JSONEncode, generic JSONDecode, :: JSONNode
from Data.GenDefault import generic gDefault

from mTask.Language import :: Main, class type
from mTask.Interpret.DSL import :: BCInterpret, :: MTLens, :: BCState, :: SDSLens, :: StateT, :: WriterT, :: Identity
from mTask.Interpret.Peripheral import :: BCPeripheral
from mTask.Interpret.ByteCodeEncoding import generic fromByteCode, generic toByteCode, class toByteWidth, :: FBC
from mTask.Interpret.Instructions import :: BCInstr

from mTask.Interpret.String255 import :: String255
from Data.UInt import :: UInt8

:: CompileOpts =
	{ tailcallopt  :: Bool
	, labelresolve :: Bool
	, shorthands   :: Bool
	}

instance zero CompileOpts

compileOpts :: CompileOpts (Main (BCInterpret (TaskValue v))) -> (String255, [(? MTLens, Task BCShareSpec)], {BCPeripheral}, [BCInstr]) | type v

:: BCShareSpec =
	{ bcs_ident  :: UInt8
	, bcs_itasks :: Bool
	, bcs_value  :: String255
	}

derive gCSerialise BCShareSpec
derive gCDeserialise BCShareSpec
derive class iTask BCShareSpec
