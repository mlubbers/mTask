definition module mTask.Interpret.String255

from Data.Either import :: Either
from Data.GenDefault import generic gDefault
from Data.GenEq import generic gEq
from GenType import generic gType, :: Box, :: GType
from GenType.CSerialise import generic gCSerialise, generic gCDeserialise, :: CDeserialiseError
from StdOverloaded import class +++, class %, class fromString, class ==
from Text import class Text
from Text.GenJSON import generic JSONEncode, generic JSONDecode, :: JSONNode
from iTasks.Internal.Generic.Visualization import generic gText, :: TextFormat
from iTasks.UI.Editor.Generic import generic gEditor, :: Editor, :: EditorPurpose
from iTasks.WF.Definition import class iTask

:: String255 =: String255 String

instance +++ String255
instance % String255
instance fromString String255
instance toString String255
instance == String255
instance Text String255

derive class iTask String255
derive gCDeserialise String255
derive gCSerialise String255
derive gType String255

safePrint :: String255 -> String

string255gType :: (String, [String], String -> [String], String -> [String])
