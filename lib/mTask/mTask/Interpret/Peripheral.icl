implementation module mTask.Interpret.Peripheral

import StdEnv

import Data.Func
import Data.List
import Data.Functor
import Data.Functor.Identity
import Data.Monoid
import Data.Either
import Control.Monad
import Control.Monad.State
import Control.Monad.Writer
import Control.Applicative

import Text => qualified join

import mTask.Interpret.DSL
import mTask.Interpret.VoidPointer
import Data.UInt
import mTask.Language

import GenType.CSerialise

derive class iTask BCPeripheral
derive gCSerialise BCPeripheral, DHTtype, DHTInfo, Pin, APin, DPin, LEDMatrixInfo, I2CAddr
derive gCDeserialise BCPeripheral, DHTtype, DHTInfo, Pin, APin, DPin, LEDMatrixInfo, I2CAddr

nextPeripheral st = length st.bcs_hardware

instance dht (StateT BCState (WriterT [BCInstr] Identity)) where
	DHT dhtinfo def = {main
		=   Dht <$> gets nextPeripheral
		<*  modify (\s->{s & bcs_hardware=s.bcs_hardware ++ [BCDHT dhtinfo]})
		>>= unmain o def o pure
		}
	temperature` ti dht
		= dht
		>>= \(Dht i)->tell` [BCMkTask $ BCDHTTemp $ fromInt i]
		>>| setRate True ti
	humidity` ti dht
		= dht
		>>= \(Dht i)->tell` [BCMkTask $ BCDHTHumid $ fromInt i]
		>>| setRate True ti

instance LEDMatrix (StateT BCState (WriterT [BCInstr] Identity)) where
	ledmatrix i def = {main
		=   LEDMatrix <$> gets nextPeripheral
		<*  modify (\s->{s & bcs_hardware=s.bcs_hardware ++ [BCLEDMatrix i]})
		>>= unmain o def o pure
		}
	LMDot m x y s = m >>= \(LEDMatrix i)->x >>| y >>| s >>| tell` [BCMkTask $ BCLEDMatrixDot $ fromInt i]
	LMIntensity m x = m >>= \(LEDMatrix i)->x >>| tell` [BCMkTask $ BCLEDMatrixIntensity $ fromInt i]
	LMClear m = m >>= \(LEDMatrix i)->tell` [BCMkTask $ BCLEDMatrixClear $ fromInt i]
	LMDisplay m = m >>= \(LEDMatrix i)->tell` [BCMkTask $ BCLEDMatrixDisplay $ fromInt i]

instance i2cbutton (StateT BCState (WriterT [BCInstr] Identity)) where
	i2cbutton addr def = {main
		=   (\x->I2CButton x) <$> gets nextPeripheral
		<*  modify (\s->{s & bcs_hardware=s.bcs_hardware ++ [BCI2CButton addr]})
		>>= unmain o def o pure
		}
	AButton` ti m
		= m
		>>= \(I2CButton i)->tell` [BCMkTask (BCAButton (fromInt i))]
		>>| setRate True ti
	BButton` ti m
		= m
		>>= \(I2CButton i)->tell` [BCMkTask (BCBButton (fromInt i))]
		>>| setRate True ti

instance LightSensor (StateT BCState (WriterT [BCInstr] Identity))
where
	lightsensor addr def = {main
		= LightSensor <$> gets nextPeripheral
		<* modify (\s -> {s & bcs_hardware=s.bcs_hardware ++ [BCLightSensor addr]})
		>>= unmain o def o pure
		}
	light` ti ls
		= ls
		>>= \(LightSensor i) -> tell` [BCMkTask (BCGetLight (fromInt i))]
		>>| setRate True ti

instance AirQualitySensor (StateT BCState (WriterT [BCInstr] Identity))
where
	airqualitysensor addr def = {main
		= AirQualitySensor <$> gets nextPeripheral
		<* modify (\s -> {s & bcs_hardware=s.bcs_hardware ++ [BCAirQualitySensor addr]})
		>>= unmain o def o pure
		}
	setEnvironmentalData aqs humid temp = humid >>| temp >>| aqs >>= \(AirQualitySensor i)->tell` [BCMkTask (BCSetEnvironmentalData (fromInt i))]
	tvoc` i aqs
		= aqs
		>>= \(AirQualitySensor s) -> tell` [BCMkTask (BCTVOC (fromInt s))]
		>>| setRate True i
	co2` i aqs
		= aqs
		>>= \(AirQualitySensor s) -> tell` [BCMkTask (BCCO2 (fromInt s))]
		>>| setRate True i

instance GestureSensor (StateT BCState (WriterT [BCInstr] Identity))
where
	gestureSensor addr def = {main
		=  GestureSensor <$> gets nextPeripheral
		<* modify (\s -> {s & bcs_hardware=s.bcs_hardware ++ [BCGestureSensor addr]})
		>>= unmain o def o pure
		}
	gesture` i ges
		= ges
		>>= \(GestureSensor s) -> tell` [BCMkTask (BCGesture (fromInt s))]
		>>| setRate True i
