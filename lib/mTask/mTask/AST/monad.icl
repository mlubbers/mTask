implementation module mTask.AST.monad

/*
	Pieter Koopman
	Radboud University NIjmegen, The Netherlands
	pieter@cs.ru.nl
*/

import mTask.AST.basic
import mTask.Show.monad
import StdMisc
import Control.Monad, Control.Applicative, Data.Functor

instance Functor AST where
	fmap f (AST s1) = AST \st.let (a, st2) = s1 st in (f a, st2)
instance pure AST where
	pure x = AST \st.(x, st)
instance <*> AST where
	(<*>) (AST f) (AST x) = AST \st0.let (f1, st1) = f st0; (x1, st2) = x st1 in (f1 x1, st2)
instance Monad AST where
	bind (AST x) f = AST \s1.let (a, s2) = x s1; (AST f2) = f a in f2 s2
//	>>= (AST x) f = AST \s1.let (a, s2) = x s1; (AST f2) = f a in f2 s2
//	>>| x f = x >>= \a.f
instance freshId AST where
	freshId :: AST String
	freshId = AST \st=:{ids2=[a:x]}.(a, {st & ids2 = x})

getExpr :: AST Expr
getExpr = AST \st.(st.expr, st)

setExpr :: Expr -> AST x
setExpr e = AST \st.(undef, {st & expr = e})
