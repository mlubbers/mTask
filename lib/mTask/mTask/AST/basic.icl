implementation module mTask.AST.basic

/*
	Pieter Koopman
	Radboud University NIjmegen, The Netherlands
	pieter@cs.ru.nl
*/

import StdInt, StdClass, StdList, StdTuple, StdString, StdEnum
import StdGeneric, Data.GenEq
import Control.Monad, Control.Applicative, Data.Functor
//import Control.Monad
//import iTasks.WF.Combinators.Overloaded
from StdMisc import undef, abort
from StdFunc import o

import mTask.Language.def
import mTask.Language.Long

derive gEq Expr, Type, StepExpr
instance == Expr where (==) x y = gEq{|*|} x y
/*
instance freshId (AST1 r) where
//	freshId :: AST1 String
	freshId = AST1 \st=:{ids2=[a:x]}.(a, undef, {st & ids2 = x})

instance Functor (AST1 r) where
	fmap f (AST1 s1) = AST1 \st.let (a, r, st2) = s1 st in (f a, r, st2)
instance Applicative (AST1 r) where
	(<*>) (AST1 f) (AST1 x) = AST1 \st0.let (f1, r, st1) = f st0; (x1, s, st2) = x st1 in (f1 x1, s, st2)
	pure x = AST1 \st.(x, undef, st)
instance Monad (AST1 r) where
	bind (AST1 x) f = AST1 \s1.let (a, r, s2) = x s1; (AST1 f2) = f a in f2 s2
//	>>= (AST1 x) f = AST1 \s1.let (a, r, s2) = x s1; (AST1 f2) = f a in f2 s2
//	>>| x f = x >>= \a.f
*/
/* --
instance Functor AST where
	fmap f (AST s1) = AST \st.let (a, st2) = s1 st in (f a, st2)
instance Applicative AST where
	(<*>) (AST f) (AST x) = AST \st0.let (f1, st1) = f st0; (x1, st2) = x st1 in (f1 x1, st2)
	pure x = AST \st.(x, st)
instance Monad AST where
	bind (AST x) f = AST \s1.let (a, s2) = x s1; (AST f2) = f a in f2 s2
//	>>= (AST x) f = AST \s1.let (a, s2) = x s1; (AST f2) = f a in f2 s2
//	>>| x f = x >>= \a.f
instance freshId AST where
	freshId :: AST String
	freshId = AST \st=:{ids2=[a:x]}.(a, {st & ids2 = x})

getExpr :: AST Expr
getExpr = AST \st.(st.expr, st)

setExpr :: Expr -> AST x
setExpr e = AST \st.(undef, {st & expr = e})
*/

/*
(>>==) infixl 1 :: (AST1 r a) (r -> AST1 s b) -> AST1 s b
(>>==) (AST1 x) f = AST1 \s1.let (a, r, s2) = x s1; (AST1 f2) = f r in f2 s2

return2 :: r -> AST1 r a
return2 r = AST1 \st.(undef, r, st)
*/
generic genType a :: a -> Type
genType{|Int|} a = IntType

instance value Int  where value = 36
instance value Long where value = Long 42
instance value Bool where value = True
instance value Char where value = 'p'
instance value Real where value = 2.71828
instance value APin where value = A0
instance value DPin where value = D0
instance value ()   where value = ()
instance value (a,b) | value a & value b where value = (value, value)
instance value (a,b,c) | value a & value b & value c where value = (value, value, value)
instance value (a,b,c,d) | value a & value b & value c & value d where value = (value, value, value, value)
instance value (a->b) | value b where value = \x.value
instance value Expr where value = Lit IntType "7"
instance value [a] where value = []
instance value (TaskValue a) | value a where value = Val value True
instance value (AST a) | value a where value = AST \state.(value, state)
instance value String where value = ""

// better with a generic?
instance typeOf Int  where typeOf _ = IntType
instance typeOf Long where typeOf _ = LongType
instance typeOf Bool where typeOf _ = BoolType
instance typeOf Char where typeOf _ = CharType
instance typeOf Real where typeOf _ = RealType
instance typeOf APin where typeOf _ = APinType
instance typeOf DPin where typeOf _ = DPinType
instance typeOf ()   where typeOf _ = VoidType
instance typeOf String where typeOf _ = StringType
instance typeOf (a,b) | typeOf a & typeOf b 
where typeOf t = PairType [typeOf (fst t), typeOf (snd t)]
instance typeOf (a,b,c) | typeOf a & typeOf b & typeOf c 
where typeOf (a,b,c) = PairType [typeOf a, typeOf b, typeOf c]
instance typeOf (a,b,c,d) | typeOf a & typeOf b & typeOf c  & typeOf d
where typeOf (a,b,c,d) = PairType [typeOf a, typeOf b, typeOf c, typeOf d]
//instance typeOf (a->b) | typeOf a & typeOf b
//where typeOf f = ApType (typeOf x) (typeOf (f x)) where x = undef
/*
where typeOf f = typeOfAp f undef
typeOfAp :: (a->b) a -> Type | typeOf a & typeOf b
typeOfAp f x = ApType (typeOf x) (typeOf (f x))
*/

instance typeOf (a->b) | typeOf, value a & typeOf b where
  typeOf f
   = case typeOf (f x) of
      FunType l r = FunType [typeOf x:l] r
      t = FunType [typeOf x] t
  where x = value // abort "undef: x in typeOf (a->b)"
instance typeOf Expr where
  typeOf (Lit     type string)  = type
  typeOf (Var     type string)  = type
  typeOf (SdsExpr type string)  = type
  typeOf (App     type name as) = type
  typeOf (TaskExpr e)           = typeOf e
  typeOf (BindExpr e l)         = typeOf l
instance typeOf [a] | typeOf a where
  typeOf [a:x] = typeOf a
  typeOf [] = VoidType // will run forever //abort "undef: typeOf []"
instance typeOf StepExpr where
	typeOf (ValueExpr    b e) = typeOf e
	typeOf (StableExpr   b e) = typeOf e
	typeOf (UnstableExpr b e) = typeOf e
	typeOf (NoValueExpr    e) = typeOf e
	typeOf (AlwaysExpr     e) = typeOf e
instance typeOf (TaskValue a) | typeOf a where
	typeOf m = K (MTaskType (typeOf a)) (m == Val a True) where a = abort "undef: a in typeOf (TaskValue a)"
instance typeOf (AST a) | typeOf a where
	typeOf (AST f) = typeOf (fst (f (abort "undef: arg in typeOf (typeOf (AST a))")))
instance == (TaskValue a) where == x y = False
K a b = a

instance typeOf a    where typeOf _ = PolyType

instance toString Expr where
	toString (Lit     type name) = name
	toString (Var     type name) = name
	toString (SdsExpr type name) = name
	toString (App type name as)  = "(App " +++ name +++ "( ...)"
	toString (TaskExpr e)        = "(Task " +++ toString e +++ ")"
	toString (Object type name)  = name
	toString (BindExpr e steps)  = "(Bind " +++ toString e +++ " ...)"

instance toString Type where
  toString (FunType args type) = "FunType ... " +++ toString type
  toString (ApType t1 t2) = "ApType " +++ toString t1 +++ " " +++ toString t2
  toString (TaskType type) = "TaskType " +++ toString type
  toString (IntType) = "IntType"
  toString (BoolType) = "BoolType"
  toString (CharType) = "CharType"
  toString (RealType) = "RealType"
  toString (StringType) = "StringType"
  toString (DPinType) = "DPinType"
  toString (VoidType) = "VoidType"
  toString (PairType arg) = "PairType "
  toString (PolyType) = "PolyType"
  toString (MTaskType type) = "MTaskType " +++ toString type
  toString (ObjectType name) = "ObjectType " +++ name

vars :: Expr -> [Expr]
vars (Lit t s)          = []
vars v=:(Var t n)       = [v]
vars (SdsExpr t e)      = []
vars (App t n l)        = varsList l
vars (TaskExpr e)       = vars e
vars (Object t n)       = []
vars (BindExpr e steps) = vars e ++ varsSteps steps
vars _ = abort "undef: vars ???"

varsList l = foldr (\a x.vars a ++ x) [] l

varsSteps l = foldr (\a x.varsStep a ++ x) [] l

varsStep (ValueExpr e1 e2)    = vars e1 ++ vars e2
varsStep (StableExpr e1 e2)   = vars e1 ++ vars e2
varsStep (UnstableExpr e1 e2) = vars e1 ++ vars e2
varsStep (NoValueExpr e)      = vars e
varsStep (AlwaysExpr e)       = vars e

storeDEF :: FunDef -> AST x
storeDEF d = AST \state.(abort "undef: value of storeDEF",{state & defs = [d: state.defs]})

storeSDS :: SDSDef -> AST x
storeSDS s = AST \state.(abort "undef: value of storeSDS",{state & sdss = [s: state.sdss]})

storeObject :: ObjectDef -> AST x 
storeObject obj=:(ObjectDef type name pin dep)
 = AST \state.( abort "undef: value of storeObject"
               , {state & objs = [obj: state.objs]
                        , libs = removeDup (dep ++ state.libs)
                 }
               )
/*
storeDef :: FunDef -> AST1 a FunDef
storeDef d = AST1 (storeDef2 d)
*/

storeDef2 :: FunDef ASTstate -> (FunDef,a,ASTstate)
storeDef2 d state = (d,undef,{state & defs = [d:state.defs]})

//instance makeAST (AST a) where
makeAST :: (AST a) -> ASTstate
makeAST (AST f) = {ast & ids2 = []} where ast = snd (f state0)

mainAST :: (Main (AST a)) -> ASTstate
mainAST {main = main} = makeAST main

state0 :: ASTstate
state0 =
	{ expr	= Lit IntType ""
	, defs  = []
	, sdss  = []
	, libs  = []
	, objs  = []
	, ids2  = map toString [0..]
	}

/*
instance freshId (AST1 a) where
	freshId :: AST1 String a
	freshId = AST1 \st=:{ids2=[a:x]}.(a, {st & ids2 = x})
*/
/*
class basicTypeOf a :: a -> Type
instance basicTypeOf Int  where basicTypeOf _ = IntType
instance basicTypeOf Bool where basicTypeOf _ = BoolType
instance basicTypeOf Char where basicTypeOf _ = CharType
instance basicTypeOf (a,b) | basicTypeOf a & basicTypeOf b 
where basicTypeOf (a,b) = PairType (basicTypeOf a) (basicTypeOf b)
instance basicTypeOf a    where basicTypeOf _ = PolyType
*/
test =
	(typeOf 3
	,typeOf True
	,typeOf (7,'a')
	,typeOf [1]
	,typeOf idInt
	,typeOf (1,1,1) // works fine
//	,typeOf und // does not work
//	,typeOf id // does not work
	)

und :: x | typeOf x
und = undef

idInt :: Int -> Int
idInt x = x

id :: x -> x | typeOf x
id x = x

