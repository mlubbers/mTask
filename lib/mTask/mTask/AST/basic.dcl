definition module mTask.AST.basic

/*
	Pieter Koopman
	Radboud University NIjmegen, The Netherlands
	pieter@cs.ru.nl
*/

import mTask.Language
from iTasks.WF.Definition import :: TaskValue
//import StdInt, StdClass, StdList, StdTuple, def, StdString, Long
//-- import Control.Monad, Control.Applicative, Data.Functor
//import iTasks.WF.Combinators.Overloaded
//from StdMisc import undef
//from StdFunc import o
//from ShowMonad import class freshId

/*
:: AST1 r a = AST1 (ASTstate -> (a, r, ASTstate))
instance Functor (AST1 r)
instance Applicative (AST1 r)
instance Monad (AST1 r)
instance freshId (AST1 r)
*/

:: AST a = AST (ASTstate -> (a, ASTstate))
/* --
instance Functor AST
instance Applicative AST
instance Monad AST
instance freshId AST
getExpr :: AST Expr
setExpr :: Expr -> AST x
*/

//(>>==) infixl 1 :: (AST1 r a) (r -> AST1 s b) -> AST1 s b
//return2 :: r -> AST1 r a

class typeOf a :: a -> Type
instance typeOf
	Int, Long, Bool, Char, Real, APin, DPin, (), (a,b) | typeOf a & typeOf b, (a->b) | typeOf, value a & typeOf b, 
	(a,b,c) | typeOf a & typeOf b & typeOf c, (a,b,c,d) | typeOf a & typeOf b & typeOf c  & typeOf d,
	Expr, (TaskValue a) | typeOf a, (AST a) | typeOf a , String, a
class value a :: a
instance value
	Int, Long, Bool, Char, Real, APin, DPin, (), (a,b) | value a & value b, (a->b) | value b, 
	(a,b,c) | value a & value b & value c, (a,b,c,d) | value a & value b & value c & value d,
	Expr, (TaskValue a) | value a, (AST a) | value a, String

:: Type
 = FunType [Type] Type
 | ApType Type Type
 | TaskType Type
 | IntType
 | LongType
 | BoolType
 | CharType 
 | RealType 
 | StringType 
 | APinType
 | DPinType
 | VoidType
 | PairType [Type] 
 | PolyType
 | MTaskType Type
 | ObjectType Name

instance toString Type

:: ASTstate =
	{ expr	:: Expr
	, defs  :: [FunDef]
	, sdss  :: [SDSDef]
	, objs  :: [ObjectDef]
	, libs  :: [Name]
	, ids2  :: [String]
	}

:: Expr
	= Lit      Type String
	| Var      Type Name
	| SdsExpr  Type Name
	| App      Type Name [Expr]
	| TaskExpr Expr
	| Object   Type Name
	| BindExpr Expr [StepExpr]

:: StepExpr
	= ValueExpr    Expr Expr
	| StableExpr   Expr Expr
	| UnstableExpr Expr Expr
	| NoValueExpr       Expr
	| AlwaysExpr        Expr

:: Name :== String
:: FunDef = FunDef Type Name [Arg] Expr
:: SDSDef = SDSDef Type Name Expr
:: ObjectDef = ObjectDef Type Name [Name] [Name] // type, id, args, list of imports
:: Arg :== Expr // only Var is expected

vars :: Expr -> [Expr]

//storeDef :: FunDef -> AST1 a FunDef //FunDef ASTstate -> (FunDef,a,ASTstate)

storeDEF :: FunDef -> AST x
storeSDS :: SDSDef -> AST x
storeObject :: ObjectDef -> AST x 

instance toString Expr
instance == Expr

//class makeAST a :: a -> ASTstate
//instance makeAST (AST a)
makeAST :: (AST a) -> ASTstate
mainAST :: (Main (AST a)) -> ASTstate



