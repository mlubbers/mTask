definition module mTask.AST.monad

/*
	Pieter Koopman
	Radboud University NIjmegen, The Netherlands
	pieter@cs.ru.nl
*/

from Control.Monad import class Monad
from Control.Applicative import class Applicative, class pure, class <*>
from Data.Functor import class Functor

import mTask.AST.DSL
from mTask.Show.monad import class freshId

instance Functor AST
instance pure AST
instance <*> AST
instance Monad AST
instance freshId AST
getExpr :: AST Expr
setExpr :: Expr -> AST x
