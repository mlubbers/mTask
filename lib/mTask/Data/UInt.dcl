definition module Data.UInt

from Data.GenHash import generic gHash
from Data.GenDefault import generic gDefault
from iTasks.UI.Editor.Generic import generic gEditor, :: Editor, :: EditorPurpose
from iTasks.Internal.Generic.Visualization import generic gText, :: TextFormat
from Text.GenJSON import generic JSONDecode, generic JSONEncode, :: JSONNode
from GenEq import generic gEq
from StdOverloaded import class zero, class one, class -, class +, class *, class /, class rem, class <, class toInt, class ~, class fromInt, class ^
from GenType import generic gType, :: Box, :: GType
from GenType.CSerialise import generic gCSerialise, generic gCDeserialise, :: Either, :: CDeserialiseError

/**
 * Note that the ints do not overflow but are capped
 */

:: UInt32 =: UInt32 Int
UINT32_MAX :== UInt32 0xffffffff
UINT32_MIN :== UInt32 0x00000000
:: UInt16 =: UInt16 Int
UINT16_MAX :== UInt16 0xffff
UINT16_MIN :== UInt16 0x0000
:: UInt8 =: UInt8 Int
UINT8_MAX :== UInt8 0xff
UINT8_MIN :== UInt8 0x00

:: Int32 =: Int32 Int
INT32_MAX :== Int32  0x7fffffff
INT32_MIN :== Int32 -0x80000000
:: Int16 =: Int16 Int
INT16_MAX :== Int16  0x7fff
INT16_MIN :== Int16 -0x8000
:: Int8 =: Int8 Int
INT8_MAX :== Int8  0x7f
INT8_MIN :== Int8 -0x80

instance ~ UInt8, UInt16, UInt32, Int8, Int16, Int32
instance < UInt8, UInt16, UInt32, Int8, Int16, Int32
instance + UInt8, UInt16, UInt32, Int8, Int16, Int32
instance - UInt8, UInt16, UInt32, Int8, Int16, Int32
instance * UInt8, UInt16, UInt32, Int8, Int16, Int32
instance / UInt8, UInt16, UInt32, Int8, Int16, Int32
instance ^ UInt8, UInt16, UInt32, Int8, Int16, Int32
instance rem UInt8, UInt16, UInt32, Int8, Int16, Int32
instance == UInt8, UInt16, UInt32, Int8, Int16, Int32
instance one UInt8, UInt16, UInt32, Int8, Int16, Int32
instance zero UInt8, UInt16, UInt32, Int8, Int16, Int32
instance toInt UInt8, UInt16, UInt32, Int8, Int16, Int32
instance fromInt UInt8, UInt16, UInt32, Int8, Int16, Int32
instance toString UInt8, UInt16, UInt32, Int8, Int16, Int32

derive gEq UInt8, UInt16, UInt32, Int8, Int16, Int32
derive JSONEncode UInt8, UInt16, UInt32, Int8, Int16, Int32
derive JSONDecode UInt8, UInt16, UInt32, Int8, Int16, Int32

//iTask
derive gText UInt8, UInt16, UInt32, Int8, Int16, Int32
derive gEditor UInt8, UInt16, UInt32, Int8, Int16, Int32
derive gHash UInt8, UInt16, UInt32, Int8, Int16, Int32
derive gCSerialise UInt8, UInt16, UInt32, Int8, Int16, Int32
derive gCDeserialise UInt8, UInt16, UInt32, Int8, Int16, Int32
derive gDefault UInt8, UInt16, UInt32, Int8, Int16, Int32

//gType
derive gType UInt8, UInt16, UInt32, Int8, Int16, Int32
uint8gType :: (String, [String], String -> [String], String -> [String])
uint16gType :: (String, [String], String -> [String], String -> [String])
uint32gType :: (String, [String], String -> [String], String -> [String])
int8gType :: (String, [String], String -> [String], String -> [String])
int16gType :: (String, [String], String -> [String], String -> [String])
int32gType :: (String, [String], String -> [String], String -> [String])
