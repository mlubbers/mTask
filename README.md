# N.B.

The mTask project has been split up and moved to another namespace:
https://gitlab.science.ru.nl/mtask

The example programs can be found here: 
https://gitlab.science.ru.nl/mtask/examples

The client software can be found here:
https://gitlab.science.ru.nl/mtask/client

The server software can be found here:
https://gitlab.science.ru.nl/mtask/server

# The mTask system (mTasks)
## Table of Contents

- [Introduction](#introduction)
- [Software](#software)
	- [Stable™ release](#stable-release)
	- [Use from git](#use-from-git)
- [Project ideas](#project-ideas)
- [Publications](#publications)
	- [Peer reviewed](#peer-reviewed)
	- [Theses](#theses)
- [Contributors](#contributors)

## Introduction

The [mTask system (mTasks)][mtask] is a framework for programming complete IoT systems from a single source.
The framework is powered by a multi-backend tagless embedded DSL embedded in the pure functional programming language [Clean][clean].

With the bytecode generation backend and the iTask integration, mTask tasks can be embedded in [iTask][itask] programs and shared data sources can be synchronized.
The client runtime system runs on embedded devices such as Arduino/ESPxxx/Nucleo boards or on regular devices running linux, windows or mac.

Traditionally IoT architectures are tiered architectures where different languages, protocols and paradigms are used in different layers.
The mTask system generates all tiers from a single source specification:

![Single source architecture overview](doc/mTask/singlesource.png)

### Task Oriented Programming
TODO

### Architecture

In mTask, the tierless aspect concretely means that everything is generated from a single Clean source.
Both the iTask language and the mTask language are embedded in clean.
mTask tasks can be embedded in iTasks and they can exchange information throught their task values and through shared data sources.

![Concrete architecture](doc/mTask/architecture.png)

### Examples

#### Blink
TODO: explanation

```clean
blink :: Main (MTask v Bool) | mtask v
blink
	// Builtin LED is D4 on the D1 Wemos Mini
	= declarePin D4 PMOutput \d4->
	fun \blink=(\x->
		     delay (lit 500)
		>>|. writeD d4 x
		>>=. blink o Not)
	In {main=blink (lit True)}
```

#### More examples: TODO

### Memory layout

The memory is self managed in mTask and can be set with the `MEMSIZE` preprocessor definition.
The memory is divided up into a task heap, a task tree heap and a stack.
The task heap is only modified between executions and thus the stack location changes sometimes.
The `struct MTask` datatype is generated and automatically printed and parsed.

![Memory layout](doc/mTask/memorylayout.png)

## Software

### Stable™ release

Every so often a stable release will appear that can be found here:

ftp://ftp.cs.ru.nl/pub/Clean/mTask/

- `YEAR-MONTH` contains a stable version made in MONTH/YEAR.
- `IOT2020` contains the snapshot used for the IoT 2020 submission.
- `CEFP19` contains the snapshot used for the Central European Functional Programming Summerschool.

For every commit that is younger than 30 days an automated build is available that includes the client, the server:

[win64](https://gitlab.science.ru.nl/mlubbers/mTask/builds/artifacts/master/file/mtask-windows-x64.zip?job=windows-x64)
[linux](https://gitlab.science.ru.nl/mlubbers/mTask/builds/artifacts/master/file/mtask-linux-x64.tgz?job=linux-x64)

The archive file contents with one directory level stripped should be extracted in the `$CLEAN_HOME` directory.
E.g. on linux:

```
tar -xzf mtask-linux-x64.tgz --strip-components=1 -C "$CLEAN_HOME"
```

To test the setup, just open an example, create a project with the mTask
template, compile and run!

### Use from git

It is easier to fetch a prebuilt release.
However, if you want to set things up by hand, to for example use the git version directly, see [HACKING.md](HACKING.md).
To create an client for yourself, see [DEVICES.md](DEVICES.md).

For setting up a crosscompilation environment, checkout `pkg_windows.sh`.

## Project ideas

- [ ] Exceptions
- [x] Rewrite only when there are events (in progress)
- [x] Fixed length updateable data types (e.g. arrays) (in progress)
- [x] Interrupts (in progress)
- [ ] SDS Lenses
- [ ] Multiple task instances for the same task
- [ ] Buttons in step
- [ ] Device to device communication
- [x] An iTask editor for mTask tasks (in progress)
- [ ] Peripherals
    - [x] Generalized interface for peripherals (in progress)
    - [ ] Screen support
         - [ ] Editors
         - [x] Layouting (in progress)
    - [ ] PWM support
    - [ ] ...
- [ ] Formal semantics
- [ ] ...

## Publications

### Peer reviewed

- M. Lubbers, P. Koopman, A. Ramsingh, J. Singer, and P. Trinder, “Tiered versus Tierless IoT Stacks: Comparing Smart Campus Software Architectures,” in Proceedings of the 10th International Conference on the Internet of Things, Malmö, 2020, pp. 9.
- M. Lubbers, P. Koopman, and R. Plasmeijer, “Interpreting Task Oriented Programs on Tiny Computers,” in Proceedings of the 31th Symposium on the Implementation and Application of Functional Programming Languages, Singapore, 2019, in-press.
- M. Lubbers, P. Koopman, and R. Plasmeijer, “Multitasking on Microcontrollers using Task Oriented Programming,” in 2019 42nd International Convention on Information and Communication Technology, Electronics and Microelectronics (MIPRO), Opatija, Croatia, 2019, pp. 1587–1592.
- M. Lubbers, P. Koopman, and R. Plasmeijer, “Task Oriented Programming and the Internet of Things,” in Proceedings of the 30th Symposium on the Implementation and Application of Functional Programming Languages, Lowell, MA, 2018, pp. 83–94.
- P. Koopman, M. Lubbers, and R. Plasmeijer, “A Task-Based DSL for Microcomputers,” 2018, pp. 1–11.
- R. Plasmeijer and P. Koopman, “A Shallow Embedded Type Safe Extendable DSL for the Arduino,” in Trends in Functional Programming, vol. 9547, Cham: Springer International Publishing, 2016.

### Theses

- M. Boer, de, “Secure Communication Channels for the mTask System.”, Bachelor’s Thesis, Radboud University, Nijmegen, 2020.
- M. Amazonas Cabral De Andrade, “Developing Real Life, Task Oriented Applications for the Internet of Things,” Master’s Thesis, Radboud University, Nijmegen, 2018.
- M. Lubbers, “Task Oriented Programming and the Internet of Things,” Master’s Thesis, Radboud University, Nijmegen, 2017.

## Contributors

- Pieter Koopman
- Mart Lubbers
- Matheus Amazonas Cabral de Andrade
- Erin van der Veen
- Michel de Boer
- Sjoerd Crooijmans

[clean]: clean.cs.ru.nl
[itask]: clean.cs.ru.nl/ITasks
[mtask]: clean.cs.ru.nl/MTasks
