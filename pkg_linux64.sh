#!/bin/bash
set -e
shopt -s globstar
TARGET=mtask-linux-x64
PDCURSESURL=https://sourceforge.net/projects/pdcurses/files/pdcurses/3.9/PDCurses-3.9.zip

rm -rf $TARGET lib/{GenType,CleanSerial,MQTTClient}

# Build the pc rts
(
	cd mtask-rts
	cp config.h{.def,}
	make -f Makefile.pc -B
	mv client{,-tty}

	# Build the curses rts
	make CURSES_INTERFACE=yes -f Makefile.pc -B
	mv client{,-curses}

	# Build the xcurses rts
	if [ ! -d PDCurses-3.9 ]
	then
		curl -LSso PDCurses.zip $PDCURSESURL
		unzip -q PDCurses.zip
		rm PDCurses.zip
	fi

	(
		cd PDCurses-3.9/x11
		./configure
		make
		make install || echo "Couldn't install PDCurses, not super user?"
	)
	make CURSES_INTERFACE=pdcurses -f Makefile.pc -B
	mv client{,-xcurses}

	# mqtt version
	(
		cd ../mtask-rts-dependencies/WolfMQTT
		./autogen.sh
		./configure --enable-static --disable-examples --enable-mqtt5 --disable-tls
		make
	)
	make MQTT=yes -f Makefile.pc -B
	mv client{,-mqtt}

	# prepare wolfmqtt for arduino IDE's
	(
		cd ../mtask-rts-dependencies/WolfMQTT/wolfmqtt
		cp options.h.in options.h
		cd ../IDE/ARDUINO
		./wolfmqtt-arduino.sh
	)
)

# GenType
mkdir -p lib/GenType
cp -R dependencies/gentype/src/* lib/GenType/

# Clean Serial
mkdir -p lib/CleanSerial
make -C dependencies/CleanSerial -B
cp -R dependencies/CleanSerial/src{,-posix}/* lib/CleanSerial

# MQTTClient
mkdir -p lib/MQTTClient
(
	cd dependencies/MQTTClient/cdeps/wolfMQTT
	./autogen.sh
	./configure --disable-tls --disable-examples --enable-mqtt5 --enable-static
	make
)
make -C dependencies/MQTTClient -B buildLib
cp -R dependencies/MQTTClient/src/* lib/MQTTClient
#TODO

# Project template
cp etc/mtask{-posix,}.prt

# Copy files
rsync\
	--copy-links \
	--recursive \
	--verbose \
	--prune-empty-dirs\
	--include-from=rsync-include \
	lib examples doc etc mtask-rts mtask-rts-dependencies\
	$TARGET/

# Create archive
tar\
	--create\
	--gzip\
	--file $TARGET.tgz\
	$TARGET
