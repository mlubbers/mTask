module TestSuite

import StdEnv

import Data.Func
import Data.Tuple
import Text

import mTask.Language
import mTask.Interpret
import mTask.Interpret.Device.TCP
import mTask.Interpret.Device.Serial
import mTask.Interpret.Device.MQTT

import MQTT

import iTasks
import iTasks.Extensions.DateTime

import TestCommon => qualified :: MTask

from Gast import genState

/*
 * Parametrically test an mTask
 *
 * @param Name of the test
 * @param arguments
 * @param expected result
 * @param function producing the task
 * @param device
 * @result name and pass/fail
 */
//testTask` :: (b b -> Bool) combinator String a b (a -> Main (BCInterpret (TaskValue b))) MTDevice -> Task (String, Bool) | type b & gText{|*|} a
testTask` p c s args expected_result task dev
	=      traceValue s
	>-| c (liftmTask (task args) dev)
	\r->if (p r expected_result)
		(treturn (s, True))
		(traceValue (concat
			[ "Test ", s, " failed for "
			, toSingleLineText args
			, " expected "
			, toSingleLineText expected_result
			, " but got "
			, toSingleLineText r]) >-| treturn (s, False))

testTask   = testTask` (==) (>>-)
testrTask  = testTask` realeq (>>-)
testuTask  = testTask` (==) (>>~)
testruTask = testTask` realeq (>>~)

tests :== 20

take` num slist = take num [s\\s<|-slist]

Start w = flip doTasks w $ onStartup t
where
//	dev = {TCPSettings|host="192.168.0.77", port=8123}
//	dev = {TCPSettings|host="192.168.12.57", port=8123}
	dev = {TCPSettings|host="localhost", port=8123, pingTimeout= ?None}
//	dev = {TTYSettings|zero&devicePath="/dev/ttyUSB0"}
//  dev = {MQTTSettings| host="localhost", port=1883, mcuId="mcu", serverId="TestSuite", auth=NoAuth}

	t :: Task ()
	t = withDevice dev	\dev->sequence (map (flip ($) dev) $ flatten
			[ []
			//Stable
			, [testTask "ret()" () () \()->{main=rtrn (lit ())}]
			, [testTask "reti" i i \i->{main=rtrn (lit i)}\\i<-take` tests int16s]
			, [testTask "retl" i i \i->{main=rtrn (lit i)}\\i<-tl [Long 0:take` tests (ggen{|*|} genState)]]
			, [testTask "retl" i i \i->{main=rtrn (lit i)}\\i<-tl [Long 0:take` tests (ggen{|*|} genState)]]
			, [testTask "retb" i i \i->{main=rtrn (lit i)}\\i<-[True, False]]
			, [testrTask "retr" i i \i->{main=rtrn (lit i)}\\i<-reals]
			,   [testTask "irett" (1,2) (1,2) \i->{main=rtrn (lit i)}
				,testTask "irettt" (1,(2,3)) (1,(2,3)) \i->{main=rtrn (lit i)}
				,testTask "irettt" ((1,2),3) ((1,2),3) \i->{main=rtrn (lit i)}
				,testTask "iretttt" ((1,2),(3,4)) ((1,2),(3,4)) \i->{main=rtrn (lit i)}
				,testTask "lrett" (Long 1,Long 2) (Long 1,Long 2) \i->{main=rtrn (lit i)}
				,testTask "lrettt" (Long 1,(Long 2,Long 3)) (Long 1,(Long 2,Long 3)) \i->{main=rtrn (lit i)}
				,testTask "lrettt" ((Long 1,Long 2),Long 3) ((Long 1,Long 2),Long 3) \i->{main=rtrn (lit i)}
				,testTask "lretttt" ((Long 1,Long 2),(Long 3,Long 4)) ((Long 1,Long 2),(Long 3,Long 4)) \i->{main=rtrn (lit i)}
				]
			//Unstable
			, [testuTask "unsi" i i \i->{main=unstable (lit i)}\\i<-take` tests int16s]
			, [testuTask "unsl" i i \i->{main=unstable (lit i)}\\i<-tl [Long 0:take` tests (ggen{|*|} genState)]]
			, [testuTask "unsb" i i \i->{main=unstable (lit i)}\\i<-[True, False]]
			, [testruTask "unsr" i i \i->{main=unstable (lit i)}\\i<-reals]
			,   [testuTask "iunst" (1,2) (1,2) \i->{main=unstable (lit i)}
				,testuTask "iunstt" (1,(2,3)) (1,(2,3)) \i->{main=unstable (lit i)}
				,testuTask "iunstt" ((1,2),3) ((1,2),3) \i->{main=unstable (lit i)}
				,testuTask "iunsttt" ((1,2),(3,4)) ((1,2),(3,4)) \i->{main=unstable (lit i)}
				,testuTask "lunst" (Long 1,Long 2) (Long 1,Long 2) \i->{main=unstable (lit i)}
				,testuTask "lunstt" (Long 1,(Long 2,Long 3)) (Long 1,(Long 2,Long 3)) \i->{main=unstable (lit i)}
				,testuTask "lunstt" ((Long 1,Long 2),Long 3) ((Long 1,Long 2),Long 3) \i->{main=unstable (lit i)}
				,testuTask "lunsttt" ((Long 1,Long 2),(Long 3,Long 4)) ((Long 1,Long 2),(Long 3,Long 4)) \i->{main=unstable (lit i)}
				]
			//Boolean arithmetics
			, [testTask "not" i (not i) (\i->{main=rtrn (Not (lit i))})\\i<-take` tests (ggen{|*|} genState)]
			, [testTask "&&" (i,j) (i && j) (\(i,j)->{main=rtrn (lit i &. lit j)})\\(i,j)<-take` tests (ggen{|*|} genState)]
			, [testTask "||" (i,j) (i || j) (\(i,j)->{main=rtrn (lit i |. lit j)})\\(i,j)<-take` tests (ggen{|*|} genState)]
			//Casts
			, [testTask "i2i" i (toInt i)  (\i->{main=rtrn (int (lit i))})\\i<-ints]
			, [testTask "r2i" i (toInt i)  (\i->{main=rtrn (int (lit i))})\\i<-[-1.0, 1.0, -1.1, 1.1, 0.0, -100.0, -100.1, 100.0, 100.1]]
			, [testTask "l2i" i (toInt i)  (\i->{main=rtrn (int (lit i))})\\i<-[1,-1,0,100,105]]
			, [testrTask "i2r" i (toReal i) (\i->{main=rtrn (real (lit i))})\\i<-ints]
			, [testrTask "r2r" i (toReal i) (\i->{main=rtrn (real (lit i))})\\i<-[-1.0, 1.0, -1.1, 1.1, 0.0, -100.0, -100.1, 100.0, 100.1]]
			, [testrTask "l2r" i (toReal i) (\i->{main=rtrn (real (lit i))})\\i<-[1,-1,0,100,105]]
			//Comparison
			, [testTask "i>"  (i,j) (i > j)  (\(i,j)->{main=rtrn (lit i >.  lit j)})\\i<-ints, j<-ints]
			, [testTask "i<"  (i,j) (i < j)  (\(i,j)->{main=rtrn (lit i <.  lit j)})\\i<-ints, j<-ints]
			, [testTask "i>=" (i,j) (i >= j) (\(i,j)->{main=rtrn (lit i >=. lit j)})\\i<-ints, j<-ints]
			, [testTask "i<=" (i,j) (i <= j) (\(i,j)->{main=rtrn (lit i <=. lit j)})\\i<-ints, j<-ints]
			, [testTask "l>"  (i,j) (i > j)  (\(i,j)->{main=rtrn (lit i >.  lit j)})\\i<-longs, j<-longs]
			, [testTask "l<"  (i,j) (i < j)  (\(i,j)->{main=rtrn (lit i <.  lit j)})\\i<-longs, j<-longs]
			, [testTask "l>=" (i,j) (i >= j) (\(i,j)->{main=rtrn (lit i >=. lit j)})\\i<-longs, j<-longs]
			, [testTask "l<=" (i,j) (i <= j) (\(i,j)->{main=rtrn (lit i <=. lit j)})\\i<-longs, j<-longs]
			, [testTask "r>"  (i,j) (i > j)  (\(i,j)->{main=rtrn (lit i >.  lit j)})\\i<-reals, j<-reals]
			, [testTask "r<"  (i,j) (i < j)  (\(i,j)->{main=rtrn (lit i <.  lit j)})\\i<-reals, j<-reals]
			, [testTask "r>=" (i,j) (i >= j) (\(i,j)->{main=rtrn (lit i >=. lit j)})\\i<-reals, j<-reals]
			, [testTask "r<=" (i,j) (i <= j) (\(i,j)->{main=rtrn (lit i <=. lit j)})\\i<-reals, j<-reals]
			//Overflow:(
			, [testTask "i+" (i,j) (i + j) (\(i,j)->{main=rtrn (lit i +. lit j)})\\i<-ints, j<-ints]
			, [testTask "i-" (i,j) (i - j) (\(i,j)->{main=rtrn (lit i -. lit j)})\\i<-ints, j<-ints]
			, [testTask "i*" (i,j) (i * j) (\(i,j)->{main=rtrn (lit i *. lit j)})\\i<-ints, j<-ints]
			, [testTask "i/" (i,j) (i / j) (\(i,j)->{main=rtrn (lit i /. lit j)})\\i<-ints, j<-filter ((<>)0) ints]
			, [testTask "l+" (i,j) (i + j) (\(i,j)->{main=rtrn (lit i +. lit j)})\\i<-longs, j<-longs]
			, [testTask "l-" (i,j) (i - j) (\(i,j)->{main=rtrn (lit i -. lit j)})\\i<-longs, j<-longs]
			, [testTask "l*" (i,j) (i * j) (\(i,j)->{main=rtrn (lit i *. lit j)})\\i<-longs, j<-longs]
			, [testTask "l/" (i,j) (i / j) (\(i,j)->{main=rtrn (lit i /. lit j)})\\i<-longs, j<-filter ((<>)(Long 0)) longs]
			, [testrTask "r+" (i,j) (i + j) (\(i,j)->{main=rtrn (lit i +. lit j)})\\i<-reals, j<-reals]
			, [testrTask "r-" (i,j) (i - j) (\(i,j)->{main=rtrn (lit i -. lit j)})\\i<-reals, j<-reals]
			, [testrTask "r*" (i,j) (i * j) (\(i,j)->{main=rtrn (lit i *. lit j)})\\i<-reals, j<-reals]
			, [testrTask "r/" (i,j) (i / j) (\(i,j)->{main=rtrn (lit i /. lit j)})\\i<-reals, j<-filter ((<>)0.0) reals]
			//Conditional
			, [testTask "if" (i, b) (if b i 0) (\(i,b)->{main=rtrn (If (lit b) (lit i) (lit 0))})\\i<-ints, b<-[True, False]]
			, [testTask "if" (i, b) (if b 0 i) (\(i,b)->{main=rtrn (If (lit b) (lit 0) (lit i))})\\i<-ints, b<-[True, False]]
			//Functions
			, [testTask "const" i i (\i->
					fun \const=(\()->lit i) In {main=rtrn (const ())})\\i<-ints]
			, [testTask "constL" i i (\i->
					fun \const=(\()->lit i) In {main=rtrn (const ())})\\i<-longs]
			, [testrTask "constR" i i (\i->
					fun \const=(\()->lit i) In {main=rtrn (const ())})\\i<-reals]
			, [testTask "increment" i (i+1) (\i->
					fun \inc=(\i->i +. (lit 1)) In {main=rtrn (inc (lit i))})\\i<-ints]
			, [testTask "incrementL" i (i+(Long 1)) (\i->
					fun \inc=(\i->i +. (lit (Long 1))) In {main=rtrn (inc (lit i))})\\i<-longs]
			, [testTask "plus" (i,j) (i+j) (\(i,j)->
					fun \plus=(\(i,j)->i +. j) In {main=rtrn (plus (lit i, lit j))})\\i<-ints, j<-ints]
			, [testTask "plusL" (i,j) (i+j) \(i,j)->
					fun \plus=(\(i,j)->i +. j) In {main=rtrn (plus (lit i, lit j))}\\i<-longs, j<-longs]
			, [testrTask "plusR" (i,j) (i+j) \(i,j)->
					fun \plus=(\(i,j)->i +. j) In {main=rtrn (plus (lit i, lit j))}\\i<-reals, j<-reals]
			, [testTask "countdownTail" 1000 0 \i->
					fun \countdown=(\i->If (i ==. lit 0) (lit 0) (countdown (i -. lit 1)))
					In {main=rtrn (countdown (lit i))}]
			, [testTask "countdownTailL" (Long 1000) (Long 0) \i->
					fun \countdown=(\i->If (i ==. lit zero) (lit zero) (countdown (i -. lit one)))
					In {main=rtrn (countdown (lit i))}]
			, [testTask "factorial" 5 120 \i->
					fun \fac=(\i=If (i ==. lit zero) (lit one) (i *. fac (i -. lit one)))
					In {main=rtrn (fac (lit i))}]
			, [testTask "factorialL" (Long 5) (Long 120) \i->
					fun \fac=(\i=If (i ==. lit zero) (lit one) (i *. fac (i -. lit one)))
					In {main=rtrn (fac (lit i))}]
			, [testTask "factorialR" 5.0 120.0 \i->
					fun \fac=(\i=If (i ==. lit zero) (lit one) (i *. fac (i -. lit one)))
					In {main=rtrn (fac (lit i))}]
			, [testTask "factorialTl" 5 120 \i->
					fun \facacc=(\(n,a)->If (n ==. lit zero) a (facacc (n -. lit one, n *. a))) In
					fun \fac=(\i=facacc (i, lit one))
					In {main=rtrn (fac (lit i))}]
			, [testTask "factorialTlL" (Long 5) (Long 120) \i->
					fun \facacc=(\(n,a)->If (n ==. lit zero) a (facacc (n -. lit one, n *. a))) In
					fun \fac=(\i=facacc (i, lit one))
					In {main=rtrn (fac (lit i))}]
			, [testTask "factorialTl" 5.0 120.0 \i->
					fun \facacc=(\(n,a)->If (n ==. lit zero) a (facacc (n -. lit one, n *. a))) In
					fun \fac=(\i=facacc (i, lit one))
					In {main=rtrn (fac (lit i))}]
			//Tuple functions
			//TODO
			//Parallel
			, let (i, j) = (42, 43) in
				[testTask "i&&" (i,j) (i,j) (\(i,j)->{main=rtrn (lit i)     .&&. rtrn (lit j)})
				,testTask "i||" (i,j)  i    (\(i,j)->{main=rtrn (lit i)     .||. rtrn (lit j)})
				,testTask "i||" (i,j)  i    (\(i,j)->{main=rtrn (lit i)     .||. unstable (lit j)})
				,testTask "i||" (i,j)  j    (\(i,j)->{main=unstable (lit i) .||. rtrn (lit j)})
			]
			, let (i, j) = (Long 42, Long 43) in
				[testTask "l&&" (i,j) (i,j) (\(i,j)->{main=rtrn (lit i)     .&&. rtrn (lit j)})
				,testTask "l||" (i,j)  i    (\(i,j)->{main=rtrn (lit i)     .||. rtrn (lit j)})
				,testTask "l||" (i,j)  i    (\(i,j)->{main=rtrn (lit i)     .||. unstable (lit j)})
				,testTask "l||" (i,j)  j    (\(i,j)->{main=unstable (lit i) .||. rtrn (lit j)})
				]
			//Step
			, let i = 42 in
				[testTask "i>>=" i i (\i->{main=rtrn (lit i) >>=. rtrn})
				,testTask "i>>~" i i (\i->{main=rtrn (lit i) >>~. rtrn})
				,testTask "i>>|" i i (\i->{main=rtrn (lit (i+one)) >>|. rtrn (lit i)})
				,testTask "iu>>." i i (\i->{main=unstable (lit (i+one)) >>.. rtrn (lit i)})
				]
			, let i = Long 42 in
				[testTask "i>>=" i i (\i->{main=rtrn (lit i) >>=. rtrn})
				,testTask "i>>~" i i (\i->{main=rtrn (lit i) >>~. rtrn})
				,testTask "i>>|" i i (\i->{main=rtrn (lit (i+one)) >>|. rtrn (lit i)})
				,testTask "iu>>." i i (\i->{main=unstable (lit (i+one)) >>.. rtrn (lit i)})
				]
			, let (i,j) = (1,2) in
				[testTask "i>>="  (i,j) (i,j) (\(i,j)->{main=rtrn (lit (i,j)) >>=. rtrn})
				,testTask "i>>~"  (i,j) (i,j) (\(i,j)->{main=rtrn (lit (i,j)) >>~. rtrn})
				,testTask "i>>|"  (i,j) (i,j) (\(i,j)->{main=rtrn (lit (i+one,j+one)) >>|. rtrn (lit (i,j))})
				,testTask "iu>>." (i,j) (i,j) (\(i,j)->{main=unstable (lit (i+one,j+one)) >>.. rtrn (lit (i,j))})
				]
			, let (i,j) = (1,2) in
				[testTask "if>>="  (i,j) i (\(i,j)->{main=rtrn (lit (i,j)) >>=. rtrn o first})
				,testTask "if>>~"  (i,j) i (\(i,j)->{main=rtrn (lit (i,j)) >>~. rtrn o first})
				,testTask "if>>|"  (i,j) i (\(i,j)->{main=rtrn (lit (i+one,j+one)) >>|. rtrn (first $ lit (i,j))})
				,testTask "ifu>>." (i,j) i (\(i,j)->{main=unstable (lit (i+one,j+one)) >>.. rtrn (first $ lit (i,j))})
				,testTask "is>>="  (i,j) j (\(i,j)->{main=rtrn (lit (i,j)) >>=. rtrn o second})
				,testTask "is>>~"  (i,j) j (\(i,j)->{main=rtrn (lit (i,j)) >>~. rtrn o second})
				,testTask "is>>|"  (i,j) j (\(i,j)->{main=rtrn (lit (i+one,j+one)) >>|. rtrn (second $ lit (i,j))})
				,testTask "isu>>." (i,j) j (\(i,j)->{main=unstable (lit (i+one,j+one)) >>.. rtrn (second $ lit (i,j))})
				]
			//Repeat
			,
				[testuTask "rpeat" 42 42 \i->{main=rpeat (rtrn (lit i))}
				,testTask "rpeat" 42 42 \i->{main=rpeat (rtrn (lit i)) >>~. rtrn}
				,testTask "rpeat" 42 42 \i->{main=rpeat (rtrn (lit i)) >>*. [IfValue ((==.)(lit i)) rtrn]}
			]
			//Complicated steps
			, let i = 42 in
				[testTask ">>*" i i (\i->{main=rtrn (lit i) >>*. [IfUnstable (const true) (\i->rtrn (i +. lit 1)), IfStable (const true) rtrn]})
				,testTask ">>*" i i (\i->{main=rtrn (lit (i+1)) >>*. [Always (rtrn (lit i))]})
				,testTask ">>*" i i (\i->{main=rtrn (lit i) >>*. [IfValue (\i->i >. lit 1) rtrn, IfValue (\i->i <=. lit 1) (\i->rtrn (i +. lit 1))]})
				,testTask ">>*" i i (\i->{main=rtrn (lit i) >>*. [IfValue (\i->i <=. lit 1) (\i->rtrn (i +. lit 1)), IfValue (\i->i >. lit 1) rtrn]})
				,testTask ">>*" i (i+1) (\i->{main=rtrn (lit i) >>*. [IfValue (\i->i <. lit 1) rtrn, IfValue (\i->i >. lit 1) (\i->rtrn (i +. lit 1))]})
				,testTask ">>*" i (i+1) (\i->{main=rtrn (lit i) >>*. [IfValue (\i->i >=. lit 1) (\i->rtrn (i +. lit 1)), IfValue (\i->i <=. lit 1) rtrn]})
			]
			//Steps with growing contexts
			, let
				suite i =
					[testTask "ctx11>>=" i i (\i->{main=rtrn (lit i) >>=. \x->rtrn x})
					,testTask "ctx21>>=" i i (\i->{main=rtrn (lit i) >>=. \x->rtrn (lit 0) >>=. \_->rtrn x})
					,testTask "ctx22>>=" i i (\i->{main=rtrn (lit 0) >>=. \_->rtrn (lit i) >>=. \x->rtrn x})
					,testTask "ctx22>(=" i i (\i->{main=(rtrn (lit 0) >>=. \_->rtrn (lit i)) >>=. \x->rtrn x})
					,testTask "ctx31>>=" i i (\i->{main=rtrn (lit i) >>=. \x->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn x})
					,testTask "ctx31>(=" i i (\i->{main=rtrn (lit i) >>=. \x->(rtrn (lit 0) >>=. \_->rtrn (lit 0)) >>=. \_->rtrn x})
					,testTask "ctx31>(=" i i (\i->{main=rtrn (lit i) >>=. \x->rtrn (lit 0) >>=. \_->(rtrn (lit 0) >>=. \_->rtrn x)})
					,testTask "ctx31>(=" i i (\i->{main=rtrn (lit i) >>=. \x->(rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn x)})
					,testTask "ctx32>>=" i i (\i->{main=rtrn (lit 0) >>=. \_->rtrn (lit i) >>=. \x->rtrn (lit 0) >>=. \_->rtrn x})
					,testTask "ctx32>>=" i i (\i->{main=rtrn (lit 0) >>=. \_->rtrn (lit i) >>=. \x->(rtrn (lit 0) >>=. \_->rtrn x)})
					,testTask "ctx32>>=" i i (\i->{main=(rtrn (lit 0) >>=. \_->rtrn (lit i)) >>=. \x->rtrn (lit 0) >>=. \_->rtrn x})
					,testTask "ctx33>>=" i i (\i->{main=rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit i) >>=. \x->rtrn x})
					,testTask "ctx41>>=" i i (\i->{main=rtrn (lit i) >>=. \x->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn x})
					,testTask "ctx42>>=" i i (\i->{main=rtrn (lit 0) >>=. \_->rtrn (lit i) >>=. \x->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn x})
					,testTask "ctx43>>=" i i (\i->{main=rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit i) >>=. \x->rtrn (lit 0) >>=. \_->rtrn x})
					,testTask "ctx44>>=" i i (\i->{main=rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit i) >>=. \x->rtrn x})
					,testTask "ctx51>>=" i i (\i->{main=rtrn (lit i) >>=. \x->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn x})
					,testTask "ctx52>>=" i i (\i->{main=rtrn (lit 0) >>=. \_->rtrn (lit i) >>=. \x->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn x})
					,testTask "ctx53>>=" i i (\i->{main=rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit i) >>=. \x->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn x})
					,testTask "ctx54>>=" i i (\i->{main=rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit i) >>=. \x->rtrn (lit 0) >>=. \_->rtrn x})
					,testTask "ctx55>>=" i i (\i->{main=rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit i) >>=. \x->rtrn x})
					,testTask "ctx61>>=" i i (\i->{main=rtrn (lit i) >>=. \x->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn x})
					,testTask "ctx62>>=" i i (\i->{main=rtrn (lit 0) >>=. \_->rtrn (lit i) >>=. \x->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn x})
					,testTask "ctx63>>=" i i (\i->{main=rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit i) >>=. \x->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn x})
					,testTask "ctx64>>=" i i (\i->{main=rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit i) >>=. \x->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn x})
					,testTask "ctx65>>=" i i (\i->{main=rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit i) >>=. \x->rtrn (lit 0) >>=. \_->rtrn x})
					,testTask "ctx66>>=" i i (\i->{main=rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit i) >>=. \x->rtrn x})
					,testTask "ctx71>>=" i i (\i->{main=rtrn (lit i) >>=. \x->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn x})
					,testTask "ctx72>>=" i i (\i->{main=rtrn (lit 0) >>=. \_->rtrn (lit i) >>=. \x->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn x})
					,testTask "ctx73>>=" i i (\i->{main=rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit i) >>=. \x->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn x})
					,testTask "ctx74>>=" i i (\i->{main=rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit i) >>=. \x->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn x})
					,testTask "ctx75>>=" i i (\i->{main=rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit i) >>=. \x->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn x})
					,testTask "ctx76>>=" i i (\i->{main=rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit i) >>=. \x->rtrn (lit 0) >>=. \_->rtrn x})
					,testTask "ctx77>>=" i i (\i->{main=rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit i) >>=. \x->rtrn x})
					]
			  in suite 42 ++ suite (Long 42)
			//mixed steps and binds
			//Functions
			//TODO
			//Shares
			, [testuTask "sds1"  i i (\i->sds \s1=i   In {main=getSds s1})\\i<-take tests int16s]
			, [testuTask "sds21" i i (\i->sds \s1=i  In sds \s2=99 In {main=getSds s1})\\i<-take tests int16s]
			, [testuTask "sds22" i i (\i->sds \s1=99 In sds \s2=i  In {main=getSds s2})\\i<-take tests int16s]
			, [testuTask "sds31" i i (\i->sds \s1=i  In sds \s2=99 In sds \s3=99 In {main=getSds s1})\\i<-take tests int16s]
			, [testuTask "sds32" i i (\i->sds \s1=99 In sds \s2=i  In sds \s3=99 In {main=getSds s2})\\i<-take tests int16s]
			, [testuTask "sds33" i i (\i->sds \s1=99 In sds \s2=99 In sds \s3=i  In {main=getSds s3})\\i<-take tests int16s]
			, [testuTask "lsds1"  i i (\i->liftsds \s1=cShare i In {main=getSds s1})\\i<-take tests int16s]
			, [testuTask "lsds21" i i (\i->liftsds \s1=cShare i  In liftsds \s2=cShare 99 In {main=getSds s1})\\i<-take tests int16s]
			, [testuTask "lsds22" i i (\i->liftsds \s1=cShare 99 In liftsds \s2=cShare i  In {main=getSds s2})\\i<-take tests int16s]
			, [testuTask "lsds31" i i (\i->liftsds \s1=cShare i  In liftsds \s2=cShare 99 In liftsds \s3=cShare 99 In {main=getSds s1})\\i<-take tests int16s]
			, [testuTask "lsds32" i i (\i->liftsds \s1=cShare 99 In liftsds \s2=cShare i  In liftsds \s3=cShare 99 In {main=getSds s2})\\i<-take tests int16s]
			, [testuTask "lsds33" i i (\i->liftsds \s1=cShare 99 In liftsds \s2=cShare 99 In liftsds \s3=cShare i  In {main=getSds s3})\\i<-take tests int16s]
			, [testTask "usds1"  i (i+1) (\i->sds \s1=i In {main=updSds s1 ((+.)(lit 1))})\\i<-take tests ints]
			, [testTask "usds21" i (i+1) (\i->sds \s1=i  In sds \s2=99 In {main=updSds s1 ((+.)(lit 1))})\\i<-take tests ints]
			, [testTask "usds22" i (i+1) (\i->sds \s1=99 In sds \s2=i  In {main=updSds s2 ((+.)(lit 1))})\\i<-take tests ints]
			, [testTask "usds31" i (i+1) (\i->sds \s1=i  In sds \s2=99 In sds \s3=99 In {main=updSds s1 ((+.)(lit 1))})\\i<-take tests ints]
			, [testTask "usds32" i (i+1) (\i->sds \s1=99 In sds \s2=i  In sds \s3=99 In {main=updSds s2 ((+.)(lit 1))})\\i<-take tests ints]
			, [testTask "usds33" i (i+1) (\i->sds \s1=99 In sds \s2=99 In sds \s3=i  In {main=updSds s3 ((+.)(lit 1))})\\i<-take tests ints]
			, [testuTask "usdsatom" i i (\i->sds \s1=i In {main=updSds s1 (flip (+.) (lit 1)) .&&. updSds s1 (flip (-.) (lit 1)) >>|. getSds s1})\\i<-take tests ints]
		//peripherals
			, [testrTask "dht" () 80.0 \()->DHT (DHT_DHT (DigitalPin D4) DHT11) \dht->{main=humidity` r dht >>~. \h->temperature` r dht >>~. \t->rtrn (h +. t)}\\r<-[Default, RangeMs (lit 50) (lit 100)]]
			, [testuTask "writea" i i \i->{main=writeA (lit p) (lit i) >>|. readA` r (lit p)}\\p<-[A0, A1, A2, A3, A4, A5], i<-[0,1,254,255], r<-[Default, RangeMs (lit 50) (lit 100)]]
			, [testuTask "writed" i i \i->{main=writeD (lit p) (lit i) >>|. readD` r (lit p)}\\p<-[A0, A1, A2, A3, A4, A5], i<-[True, False], r<-[Default, RangeMs (lit 50) (lit 100)]]
			, [testuTask "pinmodea" () () \i->{main=pinMode pm (lit p)}\\p<-[A0, A1, A2, A3, A4, A5], pm<-[PMInputPullup, PMInput, PMOutput]]
			, [testuTask "pinmoded" () () \i->{main=pinMode pm (lit p)}\\p<-[D0, D1, D2, D3, D4, D5, D6, D7, D8, D9, D10, D11, D12, D13], pm<-[PMInputPullup, PMInput, PMOutput]]
			, [testuTask "airquality co2" () 42 \()->airqualitysensor (i2c 0x5b) \aqs->{main=co2` r aqs}\\r<-[Default, RangeMs (lit 50) (lit 100)]]
			, [testuTask "airquality tvoc" () 38 \()->airqualitysensor (i2c 0x5b) \aqs->{main=tvoc` r aqs}\\r<-[Default, RangeMs (lit 50) (lit 100)]]
			, [testuTask "airquality both" () 80 \()->airqualitysensor (i2c 0x5b) \aqs->{main=tvoc` r aqs >>~. \v->co2 aqs >>~. \w->rtrn (v +. w)}\\r<-[Default, RangeMs (lit 50) (lit 100)]]
			, [testTask "airquality" () 80 \()->airqualitysensor (i2c 0x5b) \aqs->{main=setEnvironmentalData aqs (lit 4.20) (lit 4.20) >>=. \_->co2 aqs >>~. \c->tvoc aqs >>~. \v->rtrn (v +. c)}]
			, [testTask "airqualityDHT" () 80 \()->DHT (DHT_DHT (DigitalPin D4) DHT11) \dht->airqualitysensor (i2c 0x5b) \aqs->{main=setEnvFromDHT aqs dht >>=. \_->co2 aqs >>~. \c->tvoc aqs >>~. \v->rtrn (v +. c)}]
			, [testuTask "gesture" () GNone \()->gestureSensor (i2c 0x45) \ges->{main=gesture ges}]
		]) >>- \bs->traceValue bs >-| shutDown (if (all snd bs) 0 1)
	where
		cShare = mapWrite (\_ _-> ?None) ?None o constShare
