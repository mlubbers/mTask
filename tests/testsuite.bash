#!/bin/bash
set -e
startdate=$(date)
../mtask-rts/client &
./TestSuite
kill %1
echo "started at $startdate"
echo "ended at $(date)"
