module simulateCEFP

import mTask.Interpret
import mTask.Simulate
import iTasks
import StdDebug

//Start w = doTasks (simulate (sdsLocal 7)) w
//Start w = doTasks (withShared 7 \sds. (Title "SDS" @>> updateSharedInformation [] sds) ||- (Title "Simulator" @>> (simulate (sdsGlobal sds)))) w
//Start w = doTasks iTaskLED w
//Start w = doTasks (simulate {main=onLimit 42}) w
//Start w = doTasks (simulate {main=blink2}) w
//Start w = doTasks (simulate (blinkFun (lit D13) (lit 10))) w
Start w = doTasks (simulate rep) w
//Start w = doTasks (simulate watch) w
//Start w = doTasks (simulate thermometer) w
//Start w = doTasks (simulate repTest) w
//Start w = doTasks (simulate counter) w
//Start w = doTasks iTaskThermometerS w
//Start w = doTasks (simulate wierd) w

switchOn :: Main (MTask v Bool) | mtask v
//switchOn = {main = rtrn true} // ok
//switchOn = fun \f = (\().rtrn true) In {main = f ()} // ok
switchOn = 
	fun \blinkf=( \st->
		writeD (lit D13) st 
		>>|. delay (lit 10)
//		>>|. writeD (lit D12) (Not st) 
//		>>|. blinkf o Not
		>>|. blinkf (Not st)
	)
	In {main = blinkf true}

//read2 :: MTask v (Bool, Int) | mtask v
read2 :: MTask v (Bool, Int) | expr, aio, .&&. v & dio DPin v
read2 = readD (lit D0) .&&. readA (lit A0)

wierd :: Main (MTask v Long) | mtask v
wierd =
  let task =
		readD (lit D0) .&&. readA (lit A0) >>~. \tup.
		delay (If (first tup) (second tup) (lit 10))
  in {main = rpeat task}

rep :: Main (MTask v Long) | mtask v
rep =
	{main = rpeat (
		writeD (lit D13) true >>|. 
		delay (lit 3) >>|.
		writeD (lit D13) false >>|.
		delay (lit 2)
		)
	}

blink :: (v DPin) (v Long) -> MTask v Long | mtask v
blink pin time =
	rpeat (
		writeD pin true >>|. 
		delay time >>|.
		writeD pin false >>|.
		delay time
	)

blinkFun :: (v DPin) (v Int) -> Main (MTask v Bool) | mtask v
blinkFun pin time =
	fun \blink = (\state.delay time >>|. writeD pin state >>=. blink o Not)
	In {main = blink true}

blink2 :: MTask v Long | mtask v
blink2 = blink (lit D13) (lit (Long 100)) .||. blink (lit D0) (lit (Long 123))

copyPin :: DPin DPin -> MTask v Bool | mtask v
copyPin p q = readD (lit p) >>~. writeD (lit q)

onLimit :: Int -> MTask v Bool | mtask v
onLimit lim =
	readA (lit A0) >>*.
	[IfValue (\v.v >. lit lim) \_.writeD (lit D13) true
	]

switch :: Int -> MTask v Bool | mtask v
switch time = 
//	{main =
		writeD (lit D13) true >>|. 
		delay (lit time) >>|.
		writeD (lit D13) false
//	}

condTest =
	readD d0 >>~. \b.
	If b
		(writeD d13 true)
		(writeD a0 false)

blinkFun1 :: Main (MTask v Bool) | mtask v
blinkFun1 =
	fun \blinkf=( \st->
		writeD (lit D13) st >>=. \_-> 
		delay (lit 10) >>|.
		blinkf (Not st))
	In {main = blinkf true}

counter :: Main (MTask v Int) | mtask, lcd v
counter =
	LCD 16 02 [] \lcd.
	fun \count = (\n.
		printAt lcd Zero Zero n >>|.
//		setCursor lcd Zero Zero >>|.
//		print lcd n >>|.
		delay (lit 5) >>|.
		count (n +. One))
	In {main = count Zero}

watch1 :: Main (MTask v ()) | mtask v
watch1 =
	sds \pressed = False In
	fun \watch = (\().
		readD (lit D0) >>*. 
			[IfValue id (\v.setSds pressed true)
			] >>|.
		delay (lit 10) >>|.
		watch ()) In
	fun \flash = (\().
		getSds pressed >>=. \v.
		writeD d13 v >>|.
		setSds pressed false >>|.
		delay (If v (lit 100) (lit 1)) >>|.
		flash ()) In
	{main = watch () .||. flash()}

watch :: Main (MTask v Long) | mtask v
watch =
	sds \pressed = False In
	let watch = rpeat (	
					readD (lit D0) >>*.
						[IfValue id (setSds pressed)] >>|.
					delay (lit 50))
		flash = rpeat (
					getSds pressed >>=. \v.
					writeD d13 v >>|.
					setSds pressed false >>|.
					delay (If v (lit 100) (lit 1))) in
	{main = watch .||. flash}

sdsLocal :: Int -> Main (v (TaskValue Bool)) | mtask, lcd v
sdsLocal n =
	sds \sds1 = n In
	fun \inc = ( \().
		getSds sds1 >>=. \v.
		setSds sds1 (v +. One) >>|.
		delay v >>|.
		inc ()) In
	LCD 16 02 [] \lcd.
	fun \display = ( \().
		getSds sds1 >>=. \v.
		printAt lcd Zero Zero v >>|.
		delay (lit 2) >>|.
		display ()
		) In
	{main = inc () .||. display ()}

iTaskLED :: Task (Bool,TraceTask (TaskValue Long))
iTaskLED =
	withShared True \gsds.
	updateSharedInformation [] gsds -&&-
	simulate (
		liftsds \lsds = gsds In
		{main =
			rpeat (
				getSds lsds >>=.
				writeD (lit D13) >>|.
				delay (lit 5)
			)
		}
	)
	
sdsGlobal :: (Shared sds Int) -> Main (v (TaskValue Bool)) | mtask, lcd, liftsds v & RWShared sds & TC (sds () Int Int)
sdsGlobal n =
	liftsds \sds1 = n In
	fun \inc = ( \().
		getSds sds1 >>=. \v.
		setSds sds1 (v +. One) >>|.
		delay v >>|.
		inc ()) In
	LCD 16 02 [] \lcd.
	fun \display = ( \().
		getSds sds1 >>=. \v.
		printAt lcd Zero Zero  v >>|.
		delay (lit 2) >>|.
		display ()
		) In
	{main = inc () .||. display ()}

thermometer :: Main (MTask v Long) | mtask, lcd, dht v
thermometer =
	LCD 16 02 [] \lcd.
	DHT (DHT_SHT (I2C (UInt8 0x5C))) \dht.
	{main = rpeat (
		temperature dht >>~. \t ->
		printAt lcd Zero Zero t >>|.
		delay (lit 2000)
	)}

ledControl :: Task Bool
ledControl =
	enterDevice >>! \addr ->
	withDevice addr \dev ->
	withShared False \iSds -> 
	iTask iSds -|| liftmTask (mTask iSds) dev
where
	iTask iSds = updateSharedInformation [] iSds <<@ Title "LED"
	mTask iSds =
		liftsds \mSds = iSds In
		{main = rpeat(
			getSds mSds >>=.
			writeD d13 >>|.
			delay (lit 5)
		)}

iTaskThermometer :: Task Real
iTaskThermometer =
	enterDevice >>! \addr ->
	withDevice addr \dev ->
	withShared 0.0 \iSds -> 
	(viewSharedInformation [] iSds <<@ Title "Device temperature") -||
	liftmTask (
		liftsds \mSds = iSds In
		DHT (DHT_SHT (I2C (UInt8 0x5C))) \dht.
		{main = rpeat (
			temperature dht >>~. \t ->
			setSds mSds t >>|.
			delay (lit 2000)
		)}) dev

iTaskThermometerS :: Task Real
iTaskThermometerS =
	withShared 0.0 \iSds -> 
	(viewSharedInformation [] iSds <<@ Title "Device temperature") -||
	simulate (
		liftsds \mSds = iSds In
		DHT (DHT_SHT (I2C (UInt8 0x5C))) \dht.
		{main = rpeat (
//			delay (lit 2000) >>|.
			temperature dht >>~. \t -> //trace "temperature "
			setSds mSds t >>|.
			delay (lit 2000)
		)})

repTest =
	sds \n = 0 In
	{main = rpeat (
		delay (lit 2) >>..
		getSds n >>=. \x.
		setSds n (x +. lit 1)
	) >>*. [IfValue (\a. a ==. lit 3) rtrn]
	}

Zero = lit 0
One = lit 1

instance toString (a,b) | toString a & toString b where
	toString (a,b) = "(" +++ toString a +++ "," +++ toString b +++ ")"
