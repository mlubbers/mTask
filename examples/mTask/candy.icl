module candy

import StdEnv, iTasks
import Text

import mTask.Show
import mTask.Interpret

Start :: !*World -> *World
Start w = doTasks candyTask w

//candyTask :: Task Bool
candyTask
	= enterDevice
	>>? \spec->withDevice spec \dev->
		    updateInformation [] [GLeft,GRight] <<@ Title "Gesture Sequence"
		>&> withSelection (viewInformation [] "Invalid gesture sequence")
		\gseq->liftmTask (candy gseq) dev
			-|| (viewInformation [] (concat (showMain (candy gseq))))
					<<@ Title "mTask code"
where
	candy :: [Gesture] -> Main (MTask v Bool) | mtask, GestureSensor v
	candy gseq =
		gestureSensor (i2c 0x43) \ges->
		fun \rges=(\()->recogGes rges ges GNone gseq)
		In {main=writeD d4 true >>|. rges ()}
	where
		recogGes :: (() -> MTask v Bool) (v GestureSensor) Gesture [Gesture]
			-> MTask v Bool | mtask, GestureSensor v
		recogGes rges ges prev []
			= writeD d4 false >>|. delay (lit 500) >>|. writeD d4 true >>|. rges ()
			// Motor simulation
		recogGes rges ges prev [g:gs] = gesture ges >>*.
			[IfValue ((==.) (lit g)) \_->recogGes rges ges g gs
			,IfValue (\x->x !=. lit GNone &. x !=. lit prev) \_->rges ()
			]
