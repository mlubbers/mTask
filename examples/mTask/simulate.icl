module simulate

import mTask.Interpret
import mTask.Simulate
import iTasks
//import mTask.Language.LCD

//Start w = doTasks (simulate (sdsLocal 7)) w
//Start w = doTasks (withShared 7 \sds. (Title "SDS" @>> updateSharedInformation [] sds) ||- (Title "Simulator" @>> (simulate (sdsGlobal sds)))) w
//Start w = doTasks iTaskLED w
//Start w = doTasks sdsGetTask w
Start w = doTasks sdsSetTask w

sdsGetTask :: Task (Int,TraceTask (TaskValue Int))
sdsGetTask =
	withShared 38 \gsds.
	updateSharedInformation [] gsds -&&-
	simulate (
		liftsds \lsds = gsds In {main=getSds lsds})

sdsSetTask :: Task (Int,TraceTask (TaskValue Int))
sdsSetTask =
	withShared 38 \gsds.
	viewSharedInformation [] gsds -&&-
	simulate (
		liftsds \lsds = gsds In {main=delay (lit 1) >>|. setSds lsds (lit 42)})

switchOn :: Main (MTask v Bool) | mtask v
//switchOn = {main = rtrn true} // ok
//switchOn = fun \f = (\().rtrn true) In {main = f ()} // ok
switchOn = 
	fun \blinkf=( \st->
		writeD (lit D13) st 
		>>|. delay (lit 10)
//		>>|. writeD (lit D12) (Not st) 
//		>>|. blinkf o Not
		>>|. blinkf (Not st)
	)
	In {main = blinkf true}

rep :: Main (MTask v Int) | mtask v
rep =
	{main = rpeat (
		writeD (lit D13) true >>|. 
		delay (lit 3) >>|.
		writeD (lit D13) false >>|.
		delay (lit 2)
		)
	}

switch :: Main (MTask v Bool) | mtask v
switch = 
	{main =
		writeD (lit D13) true >>|. 
		delay (lit 10) >>|.
		writeD (lit D13) false
	}

blink :: Main (MTask v Bool) | mtask v
blink =
	fun \blinkf=( \st->
		writeD (lit D13) st >>=. \_-> 
		delay (lit 10) >>|.
		blinkf (Not st))
	In {main = blinkf true}

counter :: Main (MTask v Int) | mtask, lcd v
counter =
	LCD 16 02 [] \lcd.
	fun \count = (\n.
		setCursor lcd Zero Zero >>|.
		print lcd n >>|.
		delay (lit 5) >>|.
		count (n +. One))
	In {main = count Zero}

sdsLocal :: Int -> Main (v (TaskValue Bool)) | mtask, lcd v
sdsLocal n =
	sds \sds1 = n In
	fun \inc = ( \().
		getSds sds1 >>=. \v.
		setSds sds1 (v +. One) >>|.
		delay v >>|.
		inc ()) In
	LCD 16 02 [] \lcd.
	fun \display = ( \().
		getSds sds1 >>=. \v.
		setCursor lcd Zero Zero >>|.
		print lcd v >>|.
		delay (lit 2) >>|.
		display ()
		) In
	{main = inc () .||. display ()}

iTaskLED :: Task (Bool,TraceTask (TaskValue Int))
iTaskLED =
	withShared True \gsds.
	updateSharedInformation [] gsds -&&-
	simulate (
		liftsds \lsds = gsds In
		{main =
			rpeat (
				getSds lsds >>=.
				writeD (lit D13) >>|.
				delay (lit 5)
			)
		}
	)
	
//sdsGlobal :: (Shared sds Int) -> Main (v (TaskValue Bool)) | mtask, lcd, liftsds v & RWShared sds
sdsGlobal :: (SimpleSDSLens Int) -> Main (v (TaskValue Bool)) | mtask, lcd, liftsds v
sdsGlobal n =
	liftsds \sds1 = n In
	fun \inc = ( \().
		getSds sds1 >>=. \v.
		setSds sds1 (v +. One) >>|.
		delay v >>|.
		inc ()) In
	LCD 16 02 [] \lcd.
	fun \display = ( \().
		getSds sds1 >>=. \v.
		setCursor lcd Zero Zero >>|.
		print lcd v >>|.
		delay (lit 2) >>|.
		display ()
		) In
	{main = inc () .||. display ()}

Zero = lit 0
One = lit 1
