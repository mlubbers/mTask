module debug

import Data.Func, Data.Functor, Text
import StdEnv, iTasks

import mTask.Simulate
import mTask.Show
import mTask.Interpret
import mTask.Interpret.String255
import mTask.Interpret.Instructions
import mTask.Interpret.Compile
import mTask.Interpret.Device.TCP

Start w = doTasks (main task) w

derive class iTask CompileOpts

main :: (A.v: Main (MTask v a) | mtask, dht, LEDMatrix, LightSensor, AirQualitySensor v) -> Task String | type a
main task =
		viewInformation [] (concat $ showMain task)
	-|| (allTasks
		[tune (Title "Executor")  $ executor  @! ()
		,tune (Title "Static")    $ static    @! ()
		,tune (Title "Simulator") $ simulator @! ()
		] <<@ ArrangeWithTabs False)
	<<@ ArrangeWithSideBar 0 TopSide True
where
	simulator = simulate task
	
	static = updateInformation [] zero <<@ Title "Compile options"
		>&> withSelection (viewInformation [] ())
			\opts
				# (taskwidth, shares, hardware, instructions) = compileOpts opts task
				= (viewInformation []
					(formatDebugInstructions $ debugInstructions instructions) <<@ Title "Bytecode")
				-|| (sequence (map snd shares) >>- viewInformation [ViewAs $ map
					\v->{v & bcs_value=fromString (safePrint v.bcs_value)}])
				-|| allTasks [viewSharedInformation [ViewAs $ safePrint] sh\\(?Just sh, _)<-shares]
	
	executor = enterDevice
		>>? \spec->withDevice spec
			\dev->parallel
				[(Embedded, \stl->liftmTaskWithOptions {tailcallopt=True,labelresolve=True,shorthands=True} task dev)
				,(Embedded, \stl->viewSharedInformation
						[ViewAs viewer]
					(sdsFocus {fullTaskListFilter & onlyIndex= ?Just [0]} stl)
					<<@ Title "Current value"
					@? const NoValue)
				] []
			@? fmap snd o tvHd
			>>* [ OnAction (Action "Step") $ ?Just o return
			    , OnValue $ ifStable $ return o flip Value True
    			, OnAllExceptions \e->viewInformation [] e >?| return NoValue
			    ]
			>>- tune (Title "Final value") o viewInformation []

	where
		viewer :: (TaskId, [TaskListItem a]) -> ?(TaskValue a) | iTask a
		viewer (_, x) = fmap (\x->x.TaskListItem.value) $ listToMaybe x
		
//task = fun \f=(\x->x) In {main=rtrn (f (lit 9))}
//task = sds \s=9 In {main = updSds s (\x->x) >>=. setSds s o (+.) (lit 1)}
//task = sds \s=9 In {main = updSds s ((+.) (lit 42))}
//task = sds \s=9 In {main = rtrn (lit 42) >>=. \x->updSds s (\x->x)}
//task = sds \s=9 In {main = rtrn (lit 38) >>=. \_->rtrn (lit 42) >>=. \x->updSds s (\_->x)}
//task = {main = rtrn (lit 1) >>=. \x->rtrn (lit 2) >>=. \y->rtrn x}
