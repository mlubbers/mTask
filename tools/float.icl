module float

import StdEnv
import iTasks
import Data.Maybe
import Data.Either

import mTask.Interpret.ByteCodeEncoding

Start w = startEngine (t1 -&&- t2) w

t1 = enterInformation [] <<@ Title "Float" >&> viewSharedInformation [ViewAs f2bytes]
t2 = enterInformation [] <<@ Title "Bytes" >&> viewSharedInformation [ViewAs bytes2f]

f2bytes :: ((?Real) -> [Int])
f2bytes = maybe [] \f->[toInt c\\c<-:toByteCode{|*|} f]

bytes2f :: ((?(Int, Int, Int, Int)) -> (Either String (?Real), [Char]))
bytes2f = maybe (Left "No input",[]) \(a,b,c,d)->runFBC fromByteCode{|*|} [toChar a,toChar b,toChar c,toChar d]
