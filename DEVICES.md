# Device Notes

## linux/windows/macos client

### Manual compilation

To compile the client for regular operating systems run

    make -f Makefile.pc

To cross compile the windows client from linux install mingw and run

    make DETECTED_OS=Windows CC=x86_64-w64-mingw32-gcc CFLAGS=-m64 -f Makefile.pc

The makefile either creates a `client` or a `client.exe` that can be executed.
Run `client --help` for information on the command line arguments.

#### MQTT

Compile WolfMQTT by running

    (
      cd mtask-rts-dependencies/WolfMQTT
      ./autogen.sh
      ./configure --enable-static --disable-examples --enable-mqtt5 --disable-tls
      make
    )

To compile the client with MQTT run

    make MQTT=yes -f Makefile.pc

On arduino's run:

    (
      cd mtask-rts-dependencies/WolfMQTT
      cp wolfmqtt/options.h.in wolfmqtt/options.h
      cd IDE/ARDUINO
      ./wolfmqtt-arduino.sh
    )

And import the contents of wolfMQTT as a ZIP library in the IDE

## Arduino compatibles

Currently fully supported:

- Arduino UNO
- LOLIN D1 mini
- NodeMCUv2

### IDE

- Install the latest IDE and install all required libraries from `mtask-rts-dependencies`
- Open `mtask-rts/mtask-rts.ino`

### Command line

Make sure to have `Arduino-mk` installed for avr arduinos
On debian this can be done by running

    apt-get install arduino-mk

Make sure to have `makeEspArduino` installed for esp\* arduinos
This is not in the debian repositories but you can install it from
[here](https://github.com/plerup/makeEspArduino/blob/master/makeEspArduino.mk).

#### AVR arduino's (uno)
Edit the makefile to your liking (e.g. change the port for flashing) and
compile the runtimesystem with

    make -f Makefile.arduino-uno

Upload with:

    make -f Makefile.arduino-uno upload

#### ESP8266 arduino's (NodeMCU, LOLIN D1 mini)
Edit the makefile to your liking (e.g. change the port for flashing) and
compile the runtimesystem with

    make -f Makefile.arduino-lolin-d1-mini

Or 

    make -f Makefile.arduino-nodemcu

Upload with:

    make -f Makefile.arduino-{nodemcu,lolin-d1-mini} flash

#### ESP32 arduino's (wemos d32, lilygo T-Wristband)

Edit the makefile to your liking (e.g. change the port for
flashing) and compile the runtimesystem with

    make -f Makefile.arduino-wemos-d32

Upload with:

    make -f Makefile.arduino-wemos-d32 flash

### Wifi

If you want to use wifi, make sure to copy `mtask-rts/config.h.def` to `mtask-rts/config.h` and add the required wifi connection details.

## Customize the firmware

The firmware can be customized using preprocessor macros.

`config.h` contains the global configuration options

`pc.h` contains the linux/windows/macos specific configuration

`arduino.h` contains the Arduino compatible specific configuration

### Customization options

#### General options

- MEMSIZE

  The number of bytes to reserve for the mTask memory
- APINS

  The number of available analog pins
- DPINS

  The number of available digital pins
- LOGLEVEL

  The loglevel (0=silent, 1=info, 2=debug)
- SC(s)

  The method for storing strings (e.g. to store them in progmem)
- REQUIRE\_ALIGNED\_MEMORY\_ACCESS

  Force all pointers dereferences to be aligned (this is automatically set on processors that require this (e.g. ESP8266)).
- PING

  Set a time between pings.
  If set to zero, the device will never send pings

#### Communication

Communication is split up into link and communication.

- The link methods is either

  - LINK\_SERIAL for serial port communication
  - LINK\_TCP for TCP socket communication

- The communication is either

  - COMM\_DIRECT for communication without a protocol
  - COMM\_MQTT for communication using MQTT (this requires a broker)

    For running MQTT, define the server in `config.h` and make sure a server is up and running.
    mTask is tested with mosquitto on debian (testing, mosquitto version 1.6.12) that runs in a suitable-for-mtask configuration out of the box.

#### Peripherals
- `HAVE_DHT`

  Have a digital humidity and temperature sensor.

- `HAVE_LEDMATRIX`

  16x16 LED Matrix is connected
- `HAVE_OLEDSHIELD`

  OLED shield is connected (_TODO_, currently only used for runtime information)
- `HAVE_LIGHTSENSOR`

  Ambient light sensor shield is attached
- `HAVE_I2CBUTTON`

  WEMOS D1 Mini I2Cbutton shield attached
- `HAVE_AIRQUALITYSENSOR`

  CCS811 airquality sensor attached

  WEMOS D1 Mini I2Cbutton shield attached

- `HAVE_GESTURESENSOR`

  PAJ7620 gesture sensor attached

#### PC

- CURSES\_INTERFACE

  If set to `pdcurses` it will build an X11 curses application on linux.
  If set to anything else it will build an ncurses terminal application on
  linux and a `pdcurses` windows application on windows.

  - NOMOUSE

    Disables the mouse support for the curses application

#### Arduino

- BAUDRATE

  Use a different baudrate for the serial connection

### Notes on Arduino compatibles

#### UNO compatibles
It is advised to increase the serial buffer a bit since the loop can take relatively long.
This is done by increasing the `SERIAL_RX_BUFFER_SIZE` in `hardware/arduino/avr/cores/arduino/HardwareSerial.cpp`.
Default baudrate is 9600.

With the new task preparation message this shouldn't be necessary anymore.

#### ESP8266 boards
The ESP8266 requires aligned LOAD/STORE instructions and will crash otherwise.
To achieve this, make sure to define `REQUIRE_ALIGNED_MEMORY_ACCESS`

#### Bluetooth

##### Bluetooth shield v2.2 (Itead studio)
The Bluetooth Shield v2.2 (Itead studio) operates on a baudrate of 38400 so the sketch also requires this.
For operation the tx of the shield needs to be connected to the rx of the arduino and vice versa.

If you want to program the arduino, remove the shield or the tx/rx jumpers.

##### Connect bluetooth on linux
Connect and pair:

```
$ bluetoothctl
[bluetooth]# scan on
...
[bluetooth]# pair 00:00:00:00:00:00
...
[bluetooth]# info 00:00:00:00:00:00
Name: ...
Alias: ...
...
UUID: Serial Port (...)
```

Setup the rfcomm tty

```
$ rfcomm release all # optional
$ rfcomm bind hci0 00:12:12:12:08:30 1
$ ls -l /dev/rfcomm*
crw-rw---- 1 root dialout 216, 0 Oct 31 09:44 /dev/rfcomm0
```

And you are ready to go by selecting /dev/rfcomm0 as a device.
The initial message often takes a little longer than expected.

##### Connect bluetooth on windows

- Click on the bluetooth icon
- Click on add new bluetooth or other device
- Click on bluetooth
- Select the arduino device and enter the pin
- In the bluetooth options click more bluetooth options
- Click on the COM ports tab and there you see an outgoing connection for the device.
  If this is not the case, add it. That outgoing connection is the address you need to enter in the programs.
  e.g. COM4

### Libraries

All library repos are in `dependencies`.

- `Adafruit-GFX-Library`

  Addition graphics routines for the OLED shield
- `Adafruit_SSD1306-esp8266-64x48`

  Patched SDD library for the LOLIN D1 mini
- `DHTlib/libraries/DHTlib`

  Required for DHT
- `WEMOS_Matrix_LED_Shield_Arduino_Library`

  Required for the LED shield
- `WEMOS_SHT3x_Arduino_Library`

  Required for the SHT shield of the LOLIN D1 mini
- `BH1750`

  BH1750 Light sensor
- `LOLIN_OLED_I2C_Button_Library`

  I2C buttons on the LOLIN D1 mini
- `SparkFun_CCS811_Arduino_Library`

  CCS811 air quality sensor
- `WolfMQTT`

  MQTT library. Setup using the instructions in `IDE/ARDUINO` and add the following line: `#define WOLFMQTT_V5` to `mqtt_types.h` after the line containing `//#define ENABLE_MQTT_TLS`
