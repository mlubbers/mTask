# Hacking

## Run mTask directly from the git repo

These instructions are written for linux using bash.
With minor adaptations, they can be used for other shells and operating systems as well.

- Install the latest greatest clean

  ```
  mkdir /opt/clean
  curl ftp://ftp.cs.ru.nl/pub/Clean/builds/linux-x64/clean-bundle-complete-linux-x64-latest.tgz | tar -xz --strip-components=1 -C /opt/clean
  echo 'export CLEAN_HOME=/opt/clean' >> .bashrc
  echo 'export PATH=$PATH:$CLEAN_HOME/bin' >> .bashrc
  ```

- Clone the git repo 

  ```
  git clone --recursive https://gitlab.science.ru.nl/mlubbers/mtask
  ```

- Prepare the dependencies and the dependencies of the dependencies

  ```
  make -C dependencies/CleanSerial
  (
  	cd dependencies/MQTTClient/cdeps/wolfMQTT
  	./autogen.sh
  	./configure --disable-tls --disable-examples --enable-mqtt5 --enable-static
  	make
  )
  make -C dependencies/MQTTClient
  ```

- To regenerate the `bctypes.[ch]` if you changed some types or added instructions

  ```
  cd tools
  cpm project MakeInterpretSymbols create ../mtaskdev-linux.prt
  cpm MakeInterpretSymbols.prj
  ./MakeInterpretSymbols ../mtask-rts
  ```

- Test an example program

  ```
  cd examples/mTask
  cpm project test create ../../mtaskdev-linux.prt
  cpm test.prj
  ```

## Coding style

For all the C code we adhere to the linux kernel coding style:
https://www.kernel.org/doc/html/v4.10/process/coding-style.html
