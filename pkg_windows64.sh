#!/bin/bash
set -e
shopt -s globstar
TARGET=mtask-windows-x64

PDCURSESURL=https://sourceforge.net/projects/pdcurses/files/pdcurses/3.9/PDCurses-3.9.zip

rm -rf $TARGET lib/{GenType,CleanSerial,MQTTClient}

# Build the pc rts
(
	cd mtask-rts
	cp config.h{.def,}
	make DETECTED_OS=Windows CC=x86_64-w64-mingw32-gcc -f Makefile.pc -B
	mv client{,-cmd}.exe

	# Build the pc pdcurses rts
	if [ ! -d PDCurses-3.9 ]
	then
		curl -LSso PDCurses.zip $PDCURSESURL
		unzip -q PDCurses.zip
		rm PDCurses.zip
	fi
	make -C PDCurses-3.9/wincon CC=x86_64-w64-mingw32-gcc
	make DETECTED_OS=Windows CC=x86_64-w64-mingw32-gcc CURSES_INTERFACE=yes -f Makefile.pc -B
	mv client{,-curses}.exe

	# Build the mqtt rts
	(
		cd ../mtask-rts-dependencies/WolfMQTT
		cp ../../dependencies/MQTTClient/cdeps/vs_settings.h wolfmqtt
		./autogen.sh
		./configure --enable-mqtt5 --disable-tls --host=x86_64-pc-mingw32 --disable-examples --enable-static CFLAGS=-Wno-error=attributes LIBS=-lws2_32 CC=x86_64-w64-mingw32-gcc
		make
	)
	make DETECTED_OS=Windows CC=x86_64-w64-mingw32-gcc MQTT=yes -f Makefile.pc -B
	mv client{,-mqtt}.exe

	# prepare wolfmqtt for arduino IDE's
	(
		cd ../mtask-rts-dependencies/WolfMQTT/wolfmqtt
		cp options.h.in options.h
		cd ../IDE/ARDUINO
		./wolfmqtt-arduino.sh
	)
)

# GenType
mkdir -p lib/GenType
cp -R dependencies/gentype/src/* lib/GenType/

# Clean Serial
mkdir -p lib/CleanSerial
make -C dependencies/CleanSerial DETECTED_OS=Windows CC=x86_64-w64-mingw32-gcc-win32 CFLAGS=-m64 PLATFORMDIR=src-windows -B
cp -R dependencies/CleanSerial/src{,-windows}/* lib/CleanSerial

# MQTT
mkdir -p lib/MQTTClient
(
	cd dependencies/MQTTClient
	cp cdeps/vs_settings.h cdeps/wolfMQTT/wolfmqtt
	(
		cd cdeps/wolfMQTT
		./autogen.sh
		./configure --enable-mqtt5 --disable-tls --host=x86_64-pc-mingw32 --disable-examples --enable-static CFLAGS=-Wno-error=attributes LIBS=-lws2_32 CC=x86_64-w64-mingw32-gcc;
		make
	)
	OS=Windows_NT CC=x86_64-w64-mingw32-gcc make buildLib -B
)
cp -R dependencies/MQTTClient/src/* lib/MQTTClient

# Project template
cp etc/mtask{-windows,}.prt

# Copy files
rsync\
	--copy-links \
	--recursive \
	--verbose \
	--prune-empty-dirs\
	--include-from=rsync-include \
	lib examples doc etc mtask-rts mtask-rts-dependencies\
	$TARGET/

# Translate to windows
mv $TARGET/{lib,Libraries}
mv $TARGET/{etc,Config}
mv $TARGET/{examples,Examples}
mv $TARGET/{doc,Help}

# Create archive
zip -r $TARGET.zip $TARGET
