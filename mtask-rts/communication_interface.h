#ifndef COMMUNICATION_INTERFACE_H_
#define COMMUNICATION_INTERFACE_H_
#ifdef __cplusplus
extern "C" {
#endif


#include <stdbool.h>
#include <stdint.h>

#include "interface.h"
#include "bctypes.h"
#include "link_interface.h"

// Communication settings
struct CommSettings {
#if defined(COMM_MQTT)
	bool clean_session;
	uint16_t keep_alive_time;
	char* client_id;
#elif defined(COMM_DIRECT)
	unsigned int ping;
#else
#error Unknown communication method
#endif
};

struct CommSettings get_comm_settings();
bool input_available(void);
struct MTMessageTo receive_message(void);
void send_message(struct MTMessageFro r);
void start_communication(struct CommSettings, struct LinkSettings);
void stop_communication(bool temporary);
void cleanup_communication(); // Called on pc when an exception occurs
void communication_yield(void);

#ifdef __cplusplus
}
#endif
#endif /* COMMUNICATION_INTERFACE_H_ */
