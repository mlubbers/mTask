#if defined(ARDUINO_ARCH_esp8266) || defined(ARDUINO_ARCH_ESP8266) ||\
	defined (ARDUINO_ARCH_ESP32)

#include <Arduino.h>
#include <stdbool.h>
#include <stdint.h>

#include "interface.h"
#include "link_interface.h"
#include "arduino.h"
#if defined(ARDUINO_ARCH_ESP8266)
	#include <ESP8266WiFi.h>
#elif defined(ARDUINO_ARCH_ESP32)
	#include <WiFi.h>
#else
	#error Wifi not supported on current device
#endif

WiFiClient client;

struct LinkSettings link_settings =
	{ .serverMode=true
	, .port=PORT
#ifdef COMM_MQTT
	, .host=HOST
#else
	, .host=NULL
#endif
	};

struct LinkSettings get_link_settings ()
{
	return link_settings;
}

bool link_input_available(void)
{
#ifdef ARDUINO_ARCH_ESP8266
	if (!client.connected()) {
#ifdef HAVE_OLEDSHIELD
		print_to_display("Server disconnected\n");
		delay(1000);
#endif
		reset();
	}
#endif
	return client.available();
}

uint8_t link_read_byte(void)
{
	while (!link_input_available())
		msdelay(5);
	return client.read();
}

void link_write_byte(uint8_t b)
{
	client.write(b);
}

void open_link(struct LinkSettings ls)
{
	const char *ssids[] = SSIDS;
	const char *wpas[] = WPAS;

	WiFi.mode(WIFI_STA);
	WiFi.setSleepMode(WIFI_LIGHT_SLEEP);

	int ssid = -1;
	do {
		Serial.println("Scanning...");
#ifdef HAVE_OLEDSHIELD
		print_to_display("Scanning..\n");
#endif
		int n = WiFi.scanNetworks();
		Serial.print("Found ");
		Serial.print(n);
		Serial.println(" networks");
#ifdef HAVE_OLEDSHIELD
		print_to_display(String(n).c_str());
		print_to_display(" found\n");
		flush_display();
#endif
		if (n == 0) {
#ifdef HAVE_OLEDSHIELD
			print_to_display("none found\n");
			flush_display();
#endif
			Serial.println("No networks found");
			delay(1000);
			continue;
		}
		for (unsigned j = 0; j<sizeof(ssids)/sizeof(char *); j++) {
			for (int i = 0; i < n; i++) {
				if (strcmp(WiFi.SSID(i).c_str(),
						ssids[j]) == 0) {
#ifdef HAVE_OLEDSHIELD
					print_to_display("Try ");
					print_to_display(String(WiFi.SSID(i)).c_str());
					print_to_display("\n");
					flush_display();
#endif
					Serial.print("Connect to: ");
					Serial.println(WiFi.SSID(i));
					ssid = j;
				}
			}
		}
	} while (ssid == -1);
	WiFi.scanDelete();
	WiFi.begin(ssids[ssid], wpas[ssid]);
	while (WiFi.status() != WL_CONNECTED) {
		delay(1000);
		Serial.print(".");
#ifdef HAVE_OLEDSHIELD
		print_to_display(".");
		flush_display();
#endif
	}
	Serial.print("\n");

#ifdef HAVE_OLEDSHIELD
	clear_display();
	print_to_display("Connected to: ");
	print_to_display(ssids[ssid]);
	print_to_display(" ");
	print_to_display(WiFi.localIP().toString().c_str());
	print_to_display(":");
	print_to_display(String(ls.port).c_str());
	print_to_display("\n");
	flush_display();
#endif
	Serial.println("WiFi connected");
	Serial.println("IP address: ");
	Serial.println(WiFi.localIP());

	if (ls.serverMode) {
		WiFiServer server(ls.port);

		server.begin();
		server.setNoDelay(true);
		Serial.print("Server started on port: ");
		Serial.println(ls.port);

		Serial.println("Waiting for a client to connect.");
		while (!(client = server.available())) {
			delay(10);
		}

#ifdef HAVE_OLEDSHIELD
		print_to_display("Client:\n");
		print_to_display(client.remoteIP().toString().c_str());
		flush_display();
#endif
		Serial.println("Client connected\n");

	} else {
		if (!client.connect(ls.host, ls.port)) {
			Serial.println("Connecting to server failed.");
			reset();
		}
	}
}

void close_link(bool temporary)
{
	(void) temporary;
	client.stop();
}

#endif
