#ifndef SCHEDULER_H_
#define SCHEDULER_H_

#include "bctypes.h"
#include "task.h"

#define MAX_SLEEP_TIME 4294967
#define INF_SLEEP_TIME 2147483647
#define TASK_QUEUE_SIZE                                                        \
	min(1 + MEMSIZE / (sizeof(struct TaskTree) + sizeof(struct MTask)), 255)
void calculate_execution_interval(struct MTask *task);

struct MTask *peek_next_task();
struct MTask *next_task(uint32_t now);
void insert_task(struct MTask *t);
void remove_from_queue(uint8_t id);

#endif /* SCHEDULER_H_ */
