#ifndef CLIENT_H
#define CLIENT_H

#ifdef __cplusplus
extern "C" {
#endif

//*Export loop/ main function so that arduino's can call it in loop()/setup()
void real_loop();
void real_main();

#ifdef __cplusplus
}
#endif
#endif
