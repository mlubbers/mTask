#ifndef ARDUINO_CONFIG_H
#define ARDUINO_CONFIG_H
#ifdef ARDUINO

#define SC(s) PSTR(s)

#ifdef ARDUINO_ARCH_esp8266
#undef ARDUINO_ARCH_ESP8266
#define ARDUINO_ARCH_ESP8266
#endif

/*
 * Architectures
 */
#ifndef BAUDRATE
	#define BAUDRATE 115200
#endif

#if defined(ARDUINO_ARCH_AVR)
	#include <Arduino.h>
	#include <WString.h>
	#define MEMSIZE 1024
	#define LINK_SERIAL
	#define COMM_DIRECT

#elif defined(ARDUINO_ARCH_ESP8266) || defined (ARDUINO_ARCH_ESP32)\
		|| defined (ARDUINO_ARCH_esp8266)
	#include <pgmspace.h>

	#define MEMSIZE 16384
	#define REQUIRE_ALIGNED_MEMORY_ACCESS
	#define LINK_TCP
#else
	#error Unknown arduino architecture
#endif

/*
 * Specific Devices
 */
#if defined(ARDUINO_AVR_UNO)

	#define APINS 6
	#define DPINS 14
	#define HAVE_DHT
	#define HAVE_DHT_DHT

#elif defined(ARDUINO_ESP8266_NODEMCU)
	#define APINS 1
	#define DPINS 8

#elif defined(ARDUINO_ESP8266_WEMOS_D1MINI)
	#define APINS 1
	#define DPINS 8

	#define HAVE_LEDMATRIX
	#define HAVE_OLEDSHIELD
	#define HAVE_DHT
	#define HAVE_DHT_SHT
	#define HAVE_I2CBUTTON
	#define HAVE_LIGHTSENSOR
	#define HAVE_AIRQUALITYSENSOR
	#define HAVE_GESTURESENSOR
#elif defined(ARDUINO_LOLIN_D32_PRO)
	#define APINS 1
	#define DPINS 1
#elif defined(ARDUINO_ESP32_DEV) //LILYGO T-Wristband
	#define APINS 1
	#define DPINS 0
	#define HAVE_OLEDSHIELD
#else
	#error Unknown arduino device
#endif

#if LOGLEVEL > 0 && defined (ARDUINO_ARCH_AVR)
#warning Are you sure you want to enable logging? This takes up a lot of space.
#endif

#endif
#endif
