#ifndef REWRITE_H
#define REWRITE_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

//* Rewrites a tasktree, returns the new top of the stack on top of the value
uint16_t *rewrite(struct TaskTree *t, uint8_t *program, uint16_t *stack);

#ifdef __cplusplus
}
#endif
#endif
