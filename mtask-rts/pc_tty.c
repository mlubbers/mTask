#if defined(_WIN32) || defined(__APPLE__) || defined (__linux__)\
	|| defined (__unix__)

#ifndef CURSES_INTERFACE

#include <stdio.h>
#include <stdarg.h>
#include <stdint.h>

#include "bctypes.h"

#include "interface.h"

extern bool dpins[DPINS];
extern uint8_t apins[APINS];
void pc_write_dpin(uint16_t i, bool b)
{
	msg_log("dwrite %d: %d\n", i, b);
	(void)i;
	(void)b;
}

void pc_read_dpin(uint16_t i)
{
	msg_log("dread %d: %s\n", i, dpins[i] ? "true" : "false");
	(void)i;
}

void pc_write_apin(uint16_t i, uint8_t a)
{
	msg_log("awrite %d: %d\n", i, a);
	(void)i;
	(void)a;
}

void pc_read_apin(uint16_t i)
{
	msg_log("aread %d: %u\n", i, apins[i]);
	(void)i;
}

void pc_set_pinmode(uint16_t p, enum PinMode mode)
{
	msg_log("set pinmode of %d to %s\n", p,
		mode == PMInput_c ? "input" :
		mode == PMOutput_c ? "output" :
		"input_pullup");
	(void)p;
	(void)mode;
}

void pc_humidity(struct DHTInfo dht) { (void)dht; }

void pc_temperature(struct DHTInfo dht) { (void)dht; }

void pc_light(void) {}
void pc_tvoc(void) { }
void pc_co2(void) { }
void pc_gesture(void) { }

void pc_lmdot(uint8_t x, uint8_t y, bool s)
{
	msg_log("ledmatrix dot: %u %u %u\n", x, y, s);
	(void)x;
	(void)y;
	(void)s;
}

void pc_lmintensity(uint8_t intensity)
{
	msg_log("ledmatrix intensity: %u\n", intensity);
	(void)intensity;
}

void pc_lmclear() { }
void pc_lmdisplay() { }
void pc_init(int port)
{
	(void)port;
}

void pc_msg_log(const char *fmt, va_list ap)
{
	vfprintf(stderr, fmt, ap);
}

void pc_reset(void)
{
}

void pc_exit(void)
{
}

void pc_yield(void)
{
}

#endif
#endif
