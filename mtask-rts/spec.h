#ifndef SPEC_H
#define SPEC_H

#ifdef __cplusplus
extern "C" {
#endif

//* Sends the specification
void spec_send(void);

#ifdef __cplusplus
}
#endif

#endif
