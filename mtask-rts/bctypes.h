#ifndef BCTYPES_H
#define BCTYPES_H
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
enum MTMessageTo_c {MTTTask_c, MTTTaskPrep_c, MTTTaskDel_c, MTTSpecRequest_c, MTTShutdown_c, MTTSdsUpdate_c} __attribute__ ((__packed__));
enum ButtonStatus {ButtonNone_c, ButtonPress_c, ButtonLong_c, ButtonDouble_c, ButtonHold_c} __attribute__ ((__packed__));
enum MTaskEvalStatus {MTEvaluated_c, MTPurged_c, MTUnevaluated_c} __attribute__ ((__packed__));
enum MTaskValueState {MTNoValue_c, MTUnstable_c, MTStable_c, MTRemoved_c} __attribute__ ((__packed__));
enum MTMessageFro_c {MTFTaskAck_c, MTFTaskPrepAck_c, MTFTaskDelAck_c, MTFTaskReturn_c, MTFSpec_c, MTFSdsUpdate_c, MTFException_c, MTFDebug_c, MTFPing_c} __attribute__ ((__packed__));
enum MTException_c {MTEOutOfMemory_c, MTEHeapUnderflow_c, MTEStackOverflow_c, MTESdsUnknown_c, MTEPeripheralUnknown_c, MTEUnsupportedPeripheral_c, MTEFPException_c, MTESyncException_c, MTERTSError_c, MTEUnexpectedDisconnect_c} __attribute__ ((__packed__));
enum TaskValue_c {NoValue_c, Value_c} __attribute__ ((__packed__));
enum BCPeripheral_c {BCDHT_c, BCLEDMatrix_c, BCI2CButton_c, BCLightSensor_c, BCAirQualitySensor_c, BCGestureSensor_c, BCInitialised_c} __attribute__ ((__packed__));
enum _Unit {_Unit_c} __attribute__ ((__packed__));
enum DHTInfo_c {DHT_DHT_c, DHT_SHT_c} __attribute__ ((__packed__));
enum DHTtype {DHT11_c, DHT21_c, DHT22_c} __attribute__ ((__packed__));
enum Pin_c {AnalogPin_c, DigitalPin_c} __attribute__ ((__packed__));
enum DPin {D0_c, D1_c, D2_c, D3_c, D4_c, D5_c, D6_c, D7_c, D8_c, D9_c, D10_c, D11_c, D12_c, D13_c} __attribute__ ((__packed__));
enum APin {A0_c, A1_c, A2_c, A3_c, A4_c, A5_c} __attribute__ ((__packed__));
enum BCInstr_c {BCReturn1_0_c, BCReturn1_1_c, BCReturn1_2_c, BCReturn1_c, BCReturn2_0_c, BCReturn2_1_c, BCReturn2_2_c, BCReturn2_c, BCReturn3_0_c, BCReturn3_1_c, BCReturn3_2_c, BCReturn3_c, BCReturn_0_c, BCReturn_1_c, BCReturn_2_c, BCReturn_c, BCJumpF_c, BCJump_c, BCLabel_c, BCJumpSR_c, BCTailcall_c, BCArg0_c, BCArg1_c, BCArg2_c, BCArg3_c, BCArg4_c, BCArg_c, BCArg10_c, BCArg21_c, BCArg32_c, BCArg43_c, BCArgs_c, BCStepArg_c, BCMkTask_c, BCTuneRateMs_c, BCTuneRateSec_c, BCIsStable_c, BCIsUnstable_c, BCIsNoValue_c, BCIsValue_c, BCPush1_c, BCPush2_c, BCPush3_c, BCPush4_c, BCPush_c, BCPushNull_c, BCPop1_c, BCPop2_c, BCPop3_c, BCPop4_c, BCPop_c, BCRot_c, BCDup_c, BCPushPtrs_c, BCItoR_c, BCItoL_c, BCRtoI_c, BCRtoL_c, BCLtoI_c, BCLtoR_c, BCAddI_c, BCSubI_c, BCMultI_c, BCDivI_c, BCAddL_c, BCSubL_c, BCMultL_c, BCDivL_c, BCAddR_c, BCSubR_c, BCMultR_c, BCDivR_c, BCAnd_c, BCOr_c, BCNot_c, BCEqI_c, BCNeqI_c, BCEqL_c, BCNeqL_c, BCLeI_c, BCGeI_c, BCLeqI_c, BCGeqI_c, BCLeL_c, BCGeL_c, BCLeqL_c, BCGeqL_c, BCLeR_c, BCGeR_c, BCLeqR_c, BCGeqR_c} __attribute__ ((__packed__));
enum BCTaskType_c {BCStable0_c, BCStable1_c, BCStable2_c, BCStable3_c, BCStable4_c, BCStableNode_c, BCUnstable0_c, BCUnstable1_c, BCUnstable2_c, BCUnstable3_c, BCUnstable4_c, BCUnstableNode_c, BCReadD_c, BCWriteD_c, BCReadA_c, BCWriteA_c, BCPinMode_c, BCRepeat_c, BCDelay_c, BCDelayUntil_c, BCTAnd_c, BCTOr_c, BCStep_c, BCStepStable_c, BCStepUnstable_c, BCSeqStable_c, BCSeqUnstable_c, BCSdsGet_c, BCSdsSet_c, BCSdsUpd_c, BCRateLimit_c, BCDHTTemp_c, BCDHTHumid_c, BCLEDMatrixDisplay_c, BCLEDMatrixIntensity_c, BCLEDMatrixDot_c, BCLEDMatrixClear_c, BCAButton_c, BCBButton_c, BCGetLight_c, BCSetEnvironmentalData_c, BCTVOC_c, BCCO2_c, BCGesture_c} __attribute__ ((__packed__));
enum PinMode {PMInput_c, PMOutput_c, PMInputPullup_c} __attribute__ ((__packed__));
typedef int64_t Int;
typedef uint8_t UInt8;

typedef uint16_t UInt16;

typedef UInt16 JumpLabel;

enum PinMode;

struct BCTaskType {
	enum BCTaskType_c cons;
	struct {
		UInt8 BCStableNode;
		UInt8 BCUnstableNode;
		enum PinMode BCPinMode;
		struct {
			UInt8 f0;
			JumpLabel f1;
		} BCStep;
		struct {
			UInt8 f0;
			JumpLabel f1;
		} BCStepStable;
		struct {
			UInt8 f0;
			JumpLabel f1;
		} BCStepUnstable;
		UInt8 BCSeqStable;
		UInt8 BCSeqUnstable;
		UInt8 BCSdsGet;
		UInt8 BCSdsSet;
		struct {
			UInt8 f0;
			JumpLabel f1;
		} BCSdsUpd;
		UInt8 BCDHTTemp;
		UInt8 BCDHTHumid;
		UInt8 BCLEDMatrixDisplay;
		UInt8 BCLEDMatrixIntensity;
		UInt8 BCLEDMatrixDot;
		UInt8 BCLEDMatrixClear;
		UInt8 BCAButton;
		UInt8 BCBButton;
		UInt8 BCGetLight;
		UInt8 BCSetEnvironmentalData;
		UInt8 BCTVOC;
		UInt8 BCCO2;
		UInt8 BCGesture;
	} data;
};

typedef char Char;
struct Char_HshArray {
	uint32_t size;
	Char *elements;
};

typedef struct String255 { uint8_t size; uint8_t *elements; } String255;
struct BCInstr {
	enum BCInstr_c cons;
	struct {
		UInt8 BCReturn1;
		UInt8 BCReturn2;
		UInt8 BCReturn3;
		UInt8 BCReturn_0;
		UInt8 BCReturn_1;
		UInt8 BCReturn_2;
		struct {
			UInt8 f0;
			UInt8 f1;
		} BCReturn;
		JumpLabel BCJumpF;
		JumpLabel BCJump;
		JumpLabel BCLabel;
		struct {
			UInt8 f0;
			JumpLabel f1;
		} BCJumpSR;
		struct {
			UInt8 f0;
			UInt8 f1;
			JumpLabel f2;
		} BCTailcall;
		UInt8 BCArg;
		struct {
			UInt8 f0;
			UInt8 f1;
		} BCArgs;
		struct {
			UInt16 f0;
			UInt8 f1;
		} BCStepArg;
		struct BCTaskType BCMkTask;
		UInt8 BCPush1;
		struct {
			UInt8 f0;
			UInt8 f1;
		} BCPush2;
		struct {
			UInt8 f0;
			UInt8 f1;
			UInt8 f2;
		} BCPush3;
		struct {
			UInt8 f0;
			UInt8 f1;
			UInt8 f2;
			UInt8 f3;
		} BCPush4;
		String255 BCPush;
		UInt8 BCPop;
		struct {
			UInt8 f0;
			UInt8 f1;
		} BCRot;
	} data;
};

enum APin;

enum DPin;

struct Pin {
	enum Pin_c cons;
	struct {
		enum APin AnalogPin;
		enum DPin DigitalPin;
	} data;
};

enum DHTtype;

typedef UInt8 I2CAddr;

struct DHTInfo {
	enum DHTInfo_c cons;
	struct {
		struct {
			struct Pin f0;
			enum DHTtype f1;
		} DHT_DHT;
		I2CAddr DHT_SHT;
	} data;
};

struct LEDMatrixInfo {
	struct Pin dataPin;
	struct Pin clockPin;
};

enum _Unit;

typedef void *VoidPointer;

struct BCPeripheral {
	enum BCPeripheral_c cons;
	struct {
		struct DHTInfo BCDHT;
		struct LEDMatrixInfo BCLEDMatrix;
		I2CAddr BCI2CButton;
		I2CAddr BCLightSensor;
		I2CAddr BCAirQualitySensor;
		I2CAddr BCGestureSensor;
		VoidPointer BCInitialised;
	} data;
};

typedef bool Bool;
struct MTDeviceSpec {
	UInt16 memory;
	UInt8 aPins;
	UInt8 dPins;
	Bool haveDHT;
	Bool haveLM;
	Bool haveI2B;
	Bool haveLS;
	Bool haveAQS;
	Bool haveGes;
};

struct TaskValue {
	enum TaskValue_c cons;
	struct {
		struct {
			void *f0;
			Bool f1;
		} Value;
	} data;
};

struct MTException {
	enum MTException_c cons;
	struct {
		UInt8 MTEOutOfMemory;
		UInt8 MTEHeapUnderflow;
		UInt8 MTEStackOverflow;
		struct {
			UInt8 f0;
			UInt8 f1;
		} MTESdsUnknown;
		struct {
			UInt8 f0;
			UInt8 f1;
		} MTEPeripheralUnknown;
		struct {
			UInt8 f0;
			UInt8 f1;
		} MTEUnsupportedPeripheral;
		UInt8 MTEFPException;
		String255 MTESyncException;
	} data;
};

struct MTMessageFro {
	enum MTMessageFro_c cons;
	struct {
		UInt8 MTFTaskAck;
		UInt8 MTFTaskPrepAck;
		UInt8 MTFTaskDelAck;
		struct {
			UInt8 f0;
			struct TaskValue f1;
		} MTFTaskReturn;
		struct MTDeviceSpec MTFSpec;
		struct {
			UInt8 f0;
			UInt8 f1;
			String255 f2;
		} MTFSdsUpdate;
		struct MTException MTFException;
		String255 MTFDebug;
	} data;
};

enum MTaskValueState;

struct BCShareSpec {
	UInt8 bcs_ident;
	Bool bcs_itasks;
	String255 bcs_value;
};

struct BCShareSpec_Array {
	uint32_t size;
	struct BCShareSpec *elements;
};

struct BCPeripheral_Array {
	uint32_t size;
	struct BCPeripheral *elements;
};

struct BCInstr_Array {
	uint32_t size;
	struct BCInstr *elements;
};

typedef struct BCInstrs { uint16_t size; uint8_t *elements; } BCInstrs;
enum MTaskEvalStatus;

typedef uint32_t UInt32;

enum ButtonStatus;

struct _Tuple6 {
	void *f0;
	void *f1;
	void *f2;
	void *f3;
	void *f4;
	void *f5;
};

struct MTMessageTo ;
struct MTask ;
struct MTMessageTo {
	enum MTMessageTo_c cons;
	struct {
		struct MTask *MTTTask;
		UInt8 MTTTaskPrep;
		UInt8 MTTTaskDel;
		struct {
			UInt8 f0;
			UInt8 f1;
			String255 f2;
		} MTTSdsUpdate;
	} data;
};

struct MTask {
	UInt8 taskid;
	VoidPointer tree;
	enum MTaskValueState stability;
	String255 value;
	struct BCShareSpec_Array shares;
	struct BCPeripheral_Array peripherals;
	BCInstrs instructions;
	enum MTaskEvalStatus status;
	UInt32 execution_min;
	UInt32 execution_max;
	UInt32 lastrun;
};

void *parse_Int_p(uint8_t (*get)(), void *(*alloc)(size_t));
Int parse_Int(uint8_t (*get)(), void *(*alloc)(size_t));
void *parse_UInt8_p(uint8_t (*get)(), void *(*alloc)(size_t));
UInt8 parse_UInt8(uint8_t (*get)(), void *(*alloc)(size_t));
void *parse_UInt16_p(uint8_t (*get)(), void *(*alloc)(size_t));
UInt16 parse_UInt16(uint8_t (*get)(), void *(*alloc)(size_t));
void *parse_JumpLabel_p(uint8_t (*get)(), void *(*alloc)(size_t));
JumpLabel parse_JumpLabel(uint8_t (*get)(), void *(*alloc)(size_t));
void *parse_PinMode_p(uint8_t (*get)(), void *(*alloc)(size_t));
enum PinMode parse_PinMode(uint8_t (*get)(), void *(*alloc)(size_t));
void *parse_BCTaskType_p(uint8_t (*get)(), void *(*alloc)(size_t));
struct BCTaskType parse_BCTaskType(uint8_t (*get)(), void *(*alloc)(size_t));
void *parse_Char_p(uint8_t (*get)(), void *(*alloc)(size_t));
Char parse_Char(uint8_t (*get)(), void *(*alloc)(size_t));
void *parse_Char_HshArray_p(uint8_t (*get)(), void *(*alloc)(size_t));
struct Char_HshArray parse_Char_HshArray(uint8_t (*get)(), void *(*alloc)(size_t));
void *parse_String255_p(uint8_t (*get)(), void *(*alloc)(size_t));
String255 parse_String255(uint8_t (*get)(), void *(*alloc)(size_t));
void *parse_BCInstr_p(uint8_t (*get)(), void *(*alloc)(size_t));
struct BCInstr parse_BCInstr(uint8_t (*get)(), void *(*alloc)(size_t));
void *parse_APin_p(uint8_t (*get)(), void *(*alloc)(size_t));
enum APin parse_APin(uint8_t (*get)(), void *(*alloc)(size_t));
void *parse_DPin_p(uint8_t (*get)(), void *(*alloc)(size_t));
enum DPin parse_DPin(uint8_t (*get)(), void *(*alloc)(size_t));
void *parse_Pin_p(uint8_t (*get)(), void *(*alloc)(size_t));
struct Pin parse_Pin(uint8_t (*get)(), void *(*alloc)(size_t));
void *parse_DHTtype_p(uint8_t (*get)(), void *(*alloc)(size_t));
enum DHTtype parse_DHTtype(uint8_t (*get)(), void *(*alloc)(size_t));
void *parse_I2CAddr_p(uint8_t (*get)(), void *(*alloc)(size_t));
I2CAddr parse_I2CAddr(uint8_t (*get)(), void *(*alloc)(size_t));
void *parse_DHTInfo_p(uint8_t (*get)(), void *(*alloc)(size_t));
struct DHTInfo parse_DHTInfo(uint8_t (*get)(), void *(*alloc)(size_t));
void *parse_LEDMatrixInfo_p(uint8_t (*get)(), void *(*alloc)(size_t));
struct LEDMatrixInfo parse_LEDMatrixInfo(uint8_t (*get)(), void *(*alloc)(size_t));
void *parse__Unit_p(uint8_t (*get)(), void *(*alloc)(size_t));
enum _Unit parse__Unit(uint8_t (*get)(), void *(*alloc)(size_t));
void *parse_VoidPointer_p(uint8_t (*get)(), void *(*alloc)(size_t));
VoidPointer parse_VoidPointer(uint8_t (*get)(), void *(*alloc)(size_t));
void *parse_BCPeripheral_p(uint8_t (*get)(), void *(*alloc)(size_t));
struct BCPeripheral parse_BCPeripheral(uint8_t (*get)(), void *(*alloc)(size_t));
void *parse_Bool_p(uint8_t (*get)(), void *(*alloc)(size_t));
Bool parse_Bool(uint8_t (*get)(), void *(*alloc)(size_t));
void *parse_MTDeviceSpec_p(uint8_t (*get)(), void *(*alloc)(size_t));
struct MTDeviceSpec parse_MTDeviceSpec(uint8_t (*get)(), void *(*alloc)(size_t));
void *parse_TaskValue_p(uint8_t (*get)(), void *(*alloc)(size_t), void *(*parse_0)(uint8_t (*)(), void *(*)(size_t)));
struct TaskValue parse_TaskValue(uint8_t (*get)(), void *(*alloc)(size_t), void *(*parse_0)(uint8_t (*)(), void *(*)(size_t)));
void *parse_MTException_p(uint8_t (*get)(), void *(*alloc)(size_t));
struct MTException parse_MTException(uint8_t (*get)(), void *(*alloc)(size_t));
void *parse_MTMessageFro_p(uint8_t (*get)(), void *(*alloc)(size_t));
struct MTMessageFro parse_MTMessageFro(uint8_t (*get)(), void *(*alloc)(size_t));
void *parse_MTaskValueState_p(uint8_t (*get)(), void *(*alloc)(size_t));
enum MTaskValueState parse_MTaskValueState(uint8_t (*get)(), void *(*alloc)(size_t));
void *parse_BCShareSpec_p(uint8_t (*get)(), void *(*alloc)(size_t));
struct BCShareSpec parse_BCShareSpec(uint8_t (*get)(), void *(*alloc)(size_t));
void *parse_BCShareSpec_Array_p(uint8_t (*get)(), void *(*alloc)(size_t));
struct BCShareSpec_Array parse_BCShareSpec_Array(uint8_t (*get)(), void *(*alloc)(size_t));
void *parse_BCPeripheral_Array_p(uint8_t (*get)(), void *(*alloc)(size_t));
struct BCPeripheral_Array parse_BCPeripheral_Array(uint8_t (*get)(), void *(*alloc)(size_t));
void *parse_BCInstr_Array_p(uint8_t (*get)(), void *(*alloc)(size_t));
struct BCInstr_Array parse_BCInstr_Array(uint8_t (*get)(), void *(*alloc)(size_t));
void *parse_BCInstrs_p(uint8_t (*get)(), void *(*alloc)(size_t));
BCInstrs parse_BCInstrs(uint8_t (*get)(), void *(*alloc)(size_t));
void *parse_MTaskEvalStatus_p(uint8_t (*get)(), void *(*alloc)(size_t));
enum MTaskEvalStatus parse_MTaskEvalStatus(uint8_t (*get)(), void *(*alloc)(size_t));
void *parse_UInt32_p(uint8_t (*get)(), void *(*alloc)(size_t));
UInt32 parse_UInt32(uint8_t (*get)(), void *(*alloc)(size_t));
void *parse_ButtonStatus_p(uint8_t (*get)(), void *(*alloc)(size_t));
enum ButtonStatus parse_ButtonStatus(uint8_t (*get)(), void *(*alloc)(size_t));
void *parse__Tuple6_p(uint8_t (*get)(), void *(*alloc)(size_t), void *(*parse_0)(uint8_t (*)(), void *(*)(size_t)), void *(*parse_1)(uint8_t (*)(), void *(*)(size_t)), void *(*parse_2)(uint8_t (*)(), void *(*)(size_t)), void *(*parse_3)(uint8_t (*)(), void *(*)(size_t)), void *(*parse_4)(uint8_t (*)(), void *(*)(size_t)), void *(*parse_5)(uint8_t (*)(), void *(*)(size_t)));
struct _Tuple6 parse__Tuple6(uint8_t (*get)(), void *(*alloc)(size_t), void *(*parse_0)(uint8_t (*)(), void *(*)(size_t)), void *(*parse_1)(uint8_t (*)(), void *(*)(size_t)), void *(*parse_2)(uint8_t (*)(), void *(*)(size_t)), void *(*parse_3)(uint8_t (*)(), void *(*)(size_t)), void *(*parse_4)(uint8_t (*)(), void *(*)(size_t)), void *(*parse_5)(uint8_t (*)(), void *(*)(size_t)));
void *parse_MTMessageTo_p(uint8_t (*get)(), void *(*alloc)(size_t));
struct MTMessageTo parse_MTMessageTo(uint8_t (*get)(), void *(*alloc)(size_t));
void *parse_MTask_p(uint8_t (*get)(), void *(*alloc)(size_t));
struct MTask parse_MTask(uint8_t (*get)(), void *(*alloc)(size_t));
void print_Int_p(void (*put)(uint8_t), void *r);
void print_Int(void (*put)(uint8_t), Int r);
void print_UInt8_p(void (*put)(uint8_t), void *r);
void print_UInt8(void (*put)(uint8_t), UInt8 r);
void print_UInt16_p(void (*put)(uint8_t), void *r);
void print_UInt16(void (*put)(uint8_t), UInt16 r);
void print_JumpLabel_p(void (*put)(uint8_t), void *r);
void print_JumpLabel(void (*put)(uint8_t), JumpLabel r);
void print_PinMode_p(void (*put)(uint8_t), void *r);
void print_PinMode(void (*put)(uint8_t), enum PinMode r);
void print_BCTaskType_p(void (*put)(uint8_t), void *r);
void print_BCTaskType(void (*put)(uint8_t), struct BCTaskType r);
void print_Char_p(void (*put)(uint8_t), void *r);
void print_Char(void (*put)(uint8_t), Char r);
void print_Char_HshArray_p(void (*put)(uint8_t), void *r);
void print_Char_HshArray(void (*put)(uint8_t), struct Char_HshArray r);
void print_String255_p(void (*put)(uint8_t), void *r);
void print_String255(void (*put)(uint8_t), String255 r);
void print_BCInstr_p(void (*put)(uint8_t), void *r);
void print_BCInstr(void (*put)(uint8_t), struct BCInstr r);
void print_APin_p(void (*put)(uint8_t), void *r);
void print_APin(void (*put)(uint8_t), enum APin r);
void print_DPin_p(void (*put)(uint8_t), void *r);
void print_DPin(void (*put)(uint8_t), enum DPin r);
void print_Pin_p(void (*put)(uint8_t), void *r);
void print_Pin(void (*put)(uint8_t), struct Pin r);
void print_DHTtype_p(void (*put)(uint8_t), void *r);
void print_DHTtype(void (*put)(uint8_t), enum DHTtype r);
void print_I2CAddr_p(void (*put)(uint8_t), void *r);
void print_I2CAddr(void (*put)(uint8_t), I2CAddr r);
void print_DHTInfo_p(void (*put)(uint8_t), void *r);
void print_DHTInfo(void (*put)(uint8_t), struct DHTInfo r);
void print_LEDMatrixInfo_p(void (*put)(uint8_t), void *r);
void print_LEDMatrixInfo(void (*put)(uint8_t), struct LEDMatrixInfo r);
void print__Unit_p(void (*put)(uint8_t), void *r);
void print__Unit(void (*put)(uint8_t), enum _Unit r);
void print_VoidPointer_p(void (*put)(uint8_t), void *r);
void print_VoidPointer(void (*put)(uint8_t), VoidPointer r);
void print_BCPeripheral_p(void (*put)(uint8_t), void *r);
void print_BCPeripheral(void (*put)(uint8_t), struct BCPeripheral r);
void print_Bool_p(void (*put)(uint8_t), void *r);
void print_Bool(void (*put)(uint8_t), Bool r);
void print_MTDeviceSpec_p(void (*put)(uint8_t), void *r);
void print_MTDeviceSpec(void (*put)(uint8_t), struct MTDeviceSpec r);
void print_TaskValue_p(void (*put)(uint8_t), void *r, void (*print_0)(void (*)(uint8_t), void *));
void print_TaskValue(void (*put)(uint8_t), struct TaskValue r, void (*print_0)(void (*)(uint8_t), void *));
void print_MTException_p(void (*put)(uint8_t), void *r);
void print_MTException(void (*put)(uint8_t), struct MTException r);
void print_MTMessageFro_p(void (*put)(uint8_t), void *r);
void print_MTMessageFro(void (*put)(uint8_t), struct MTMessageFro r);
void print_MTaskValueState_p(void (*put)(uint8_t), void *r);
void print_MTaskValueState(void (*put)(uint8_t), enum MTaskValueState r);
void print_BCShareSpec_p(void (*put)(uint8_t), void *r);
void print_BCShareSpec(void (*put)(uint8_t), struct BCShareSpec r);
void print_BCShareSpec_Array_p(void (*put)(uint8_t), void *r);
void print_BCShareSpec_Array(void (*put)(uint8_t), struct BCShareSpec_Array r);
void print_BCPeripheral_Array_p(void (*put)(uint8_t), void *r);
void print_BCPeripheral_Array(void (*put)(uint8_t), struct BCPeripheral_Array r);
void print_BCInstr_Array_p(void (*put)(uint8_t), void *r);
void print_BCInstr_Array(void (*put)(uint8_t), struct BCInstr_Array r);
void print_BCInstrs_p(void (*put)(uint8_t), void *r);
void print_BCInstrs(void (*put)(uint8_t), BCInstrs r);
void print_MTaskEvalStatus_p(void (*put)(uint8_t), void *r);
void print_MTaskEvalStatus(void (*put)(uint8_t), enum MTaskEvalStatus r);
void print_UInt32_p(void (*put)(uint8_t), void *r);
void print_UInt32(void (*put)(uint8_t), UInt32 r);
void print_ButtonStatus_p(void (*put)(uint8_t), void *r);
void print_ButtonStatus(void (*put)(uint8_t), enum ButtonStatus r);
void print__Tuple6_p(void (*put)(uint8_t), void *r, void (*print_0)(void (*)(uint8_t), void *), void (*print_1)(void (*)(uint8_t), void *), void (*print_2)(void (*)(uint8_t), void *), void (*print_3)(void (*)(uint8_t), void *), void (*print_4)(void (*)(uint8_t), void *), void (*print_5)(void (*)(uint8_t), void *));
void print__Tuple6(void (*put)(uint8_t), struct _Tuple6 r, void (*print_0)(void (*)(uint8_t), void *), void (*print_1)(void (*)(uint8_t), void *), void (*print_2)(void (*)(uint8_t), void *), void (*print_3)(void (*)(uint8_t), void *), void (*print_4)(void (*)(uint8_t), void *), void (*print_5)(void (*)(uint8_t), void *));
void print_MTMessageTo_p(void (*put)(uint8_t), void *r);
void print_MTMessageTo(void (*put)(uint8_t), struct MTMessageTo r);
void print_MTask_p(void (*put)(uint8_t), void *r);
void print_MTask(void (*put)(uint8_t), struct MTask r);
#endif