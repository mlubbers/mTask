#ifndef SDS_H
#define SDS_H
#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include "bctypes.h"

//* Retrieve a share
struct BCShareSpec *sds_get(uint8_t id);
//* Update the value of an SDS
void sds_update(uint8_t taskid, uint8_t sdsid, String255 value);
//* Update a value of an sds from mTask
void sds_set_mtask(struct BCShareSpec *sds, uint16_t *stack);
//* Push the current value of a share on the stack
uint16_t *sds_get_mtask(struct BCShareSpec *sds, uint16_t *stack);

#ifdef __cplusplus
}
#endif
#endif
