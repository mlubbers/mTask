#ifndef LINK_INTERFACE_H_
#define LINK_INTERFACE_H_
#ifdef __cplusplus
extern "C" {
#endif

#include "interface.h"
#include <stdbool.h>
#include <stdint.h>

struct LinkSettings {
#if defined(LINK_SERIAL)
	long int baudrate;
#elif defined(LINK_TCP)
	bool serverMode;
	uint16_t port;
	const char *host;
#endif
};

struct LinkSettings get_link_settings(void);
bool link_input_available(void);
uint8_t link_read_byte(void);
void link_write_byte(uint8_t b);
void open_link(struct LinkSettings);
void close_link(bool temporary);
void cleanup_link();

#ifdef __cplusplus
}
#endif
#endif /* LINK_INTERFACE_H_ */
