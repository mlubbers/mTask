#ifndef PC_CONFIG_H
#define PC_CONFIG_H

#include <setjmp.h>

#define APINS 6
#define DPINS 14
#define MEMSIZE 50024
#define HAVE_DHT
#define HAVE_LEDMATRIX
#define HAVE_LIGHTSENSOR
#define HAVE_I2CBUTTON
#define HAVE_AIRQUALITYSENSOR
#define HAVE_GESTURESENSOR
#define SC(s) s

#define LINK_TCP

extern int gargc;
extern char **gargv;
extern jmp_buf fpe;

void killHandler(int);

#define RECOVERABLE_ERROR_CODE {\
	/*Restore point for SIGFPE exceptions*/\
	if (setjmp(fpe) == 1) {\
		send_message((struct MTMessageFro)\
			{ .cons=MTFException_c\
			, .data={.MTFException=\
				{.cons=MTEFPException_c\
				,.data={current_task->taskid}}\
				}\
			});\
		msg_log(SC("Caught SIGFPE, resetting\n"));\
		mem_reset();\
	}\
}

#endif
