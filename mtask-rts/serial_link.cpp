#ifdef ARDUINO_ARCH_AVR

#include <Arduino.h>
#include <stdbool.h>
#include <stdint.h>

#include "link_interface.h"
#include "interface.h"

struct LinkSettings link_settings = { .baudrate=BAUDRATE };

struct LinkSettings get_link_settings()
{
	return link_settings;
}

bool link_input_available(void)
{
	return Serial.available();
}

uint8_t link_read_byte(void)
{
	while (!link_input_available())
		msdelay(5);
	return Serial.read();
}

void link_write_byte(uint8_t b)
{
	Serial.write(b);
}

void open_link(struct LinkSettings ls)
{
	link_settings = ls;
	Serial.begin(link_settings.baudrate);
}

void close_link(bool temporary)
{
	Serial.end();
}

void cleanup_link() {}

#endif
