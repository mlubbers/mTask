#include "interface.h"
#include "communication_interface.h"
#include "spec.h"
#include "mem.h"

void spec_send(void)
{
	send_message((struct MTMessageFro)
		{ .cons=MTFSpec_c, .data={.MTFSpec=
			{ .memory  = MEMSIZE
			, .aPins   = APINS
			, .dPins   = DPINS
			, .haveDHT =
#ifdef HAVE_DHT
				true
#else
				false
#endif
			, .haveLM=
#ifdef HAVE_LEDMATRIX
				true
#else
				false
#endif
			, .haveI2B=
#ifdef HAVE_I2CBUTTON
				true
#else
				false
#endif
			, .haveLS=
#ifdef HAVE_LIGHTSENSOR
				true
#else
				false
#endif
			, .haveAQS=
#ifdef HAVE_AIRQUALITYSENSOR
				true
#else
				false
#endif
			, .haveGes=
#ifdef HAVE_GESTURESENSOR
				true
#else
				false
#endif
			}
		}});
}
