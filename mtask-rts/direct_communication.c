#include "interface.h"

#ifdef COMM_DIRECT
#include <stdbool.h>
#include <stdint.h>

#include "communication_interface.h"
#include "link_interface.h"
#include "mem.h"

struct CommSettings comm_settings = { .ping=PING };

struct CommSettings get_comm_settings()
{
	return comm_settings;
}

bool input_available(void)
{
	return link_input_available();
}

struct MTMessageTo receive_message(void)
{
	return parse_MTMessageTo(link_read_byte, mem_alloc_task);
}

void send_message(struct MTMessageFro r)
{
	print_MTMessageFro(link_write_byte, r);
}

void start_communication(struct CommSettings cs, struct LinkSettings ls)
{
	comm_settings = cs;
	open_link(ls);
}

void stop_communication(bool temporary)
{
	close_link(temporary);
}

void cleanup_communication()
{
	cleanup_link();
}

unsigned long lastping = 0;
void communication_yield(void)
{
	if (comm_settings.ping > 0 &&\
			getmillis()-lastping > comm_settings.ping) {
		struct MTMessageFro r;
		r.cons = MTFPing_c;
		print_MTMessageFro(link_write_byte, r);
		lastping = getmillis();
	}
}
#endif
