#ifndef PC_INTERFACE_H
#define PC_INTERFACE_H

#include <stdint.h>
#include <stdarg.h>
#include "bctypes.h"

void pc_write_dpin(uint16_t, bool);
void pc_read_dpin(uint16_t);
void pc_write_apin(uint16_t, uint8_t);
void pc_read_apin(uint16_t);
void pc_set_pinmode(uint16_t, enum PinMode);

void pc_humidity(struct DHTInfo);
void pc_temperature(struct DHTInfo);
void pc_light(void);
void pc_tvoc(void);
void pc_co2(void);
void pc_gesture(void);
void pc_lmdot(uint8_t, uint8_t, bool);
void pc_lmintensity(uint8_t);
void pc_lmclear(void);
void pc_lmdisplay(void);

void pc_msg_log(const char *, va_list);

void pc_init(int mtaskport);
void pc_reset(void);
void pc_exit(void);
void pc_yield(void);

#endif
